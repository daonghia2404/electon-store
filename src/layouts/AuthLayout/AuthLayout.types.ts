import { RouteComponentProps } from '@reach/router';

export type TAuthLayoutProps = RouteComponentProps & {
  className?: string;
};
