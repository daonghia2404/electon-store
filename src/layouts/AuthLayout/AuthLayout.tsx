import React from 'react';

import { TAuthLayoutProps } from 'layouts/AuthLayout/AuthLayout.types';

const AuthLayout: React.FC<TAuthLayoutProps> = ({ children }) => {
  return (
    <div className="AuthLayout">
      <div className="AuthLayout-body flex items-center justify-center" style={{ minHeight: '100vh' }}>
        {children}
      </div>
    </div>
  );
};

export default AuthLayout;
