import React, { useEffect } from 'react';

import { TGuestLayoutProps } from 'layouts/GuestLayout/GuestLayout.types';
import Header from 'containers/Header';
import Footer from 'containers/Footer';
import { useDispatch } from 'react-redux';
import { getAllCategoriessAction } from 'redux/actions/category';
import { getAllProductAction } from 'redux/actions';

const GuestLayout: React.FC<TGuestLayoutProps> = ({ children }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllCategoriessAction.request());
    dispatch(getAllProductAction.request());
  }, [dispatch]);
  return (
    <div className="GuestLayout">
      <div className="GuestLayout-header">
        <Header />
      </div>
      <div className="GuestLayout-body">{children}</div>
      <div className="GuestLayout-footer">
        <Footer />
      </div>
    </div>
  );
};

export default GuestLayout;
