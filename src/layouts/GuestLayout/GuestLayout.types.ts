import { RouteComponentProps } from '@reach/router';

export type TGuestLayoutProps = RouteComponentProps & {
  className?: string;
};
