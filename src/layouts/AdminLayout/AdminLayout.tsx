import React from 'react';

import { TAdminLayoutProps } from 'layouts/AdminLayout/AdminLayout.types';
import Sidebar from 'containers/Sidebar';
import AdminHeader from 'containers/AdminHeader';

import './AdminLayout.scss';

const AdminLayout: React.FC<TAdminLayoutProps> = ({ children }) => {
  return (
    <div className="AdminLayout flex items-start">
      <div className="AdminLayout-sidebar">
        <Sidebar />
      </div>
      <div className="AdminLayout-main">
        <div className="AdminLayout-header">
          <AdminHeader />
        </div>
        <div className="AdminLayout-body">{children}</div>
      </div>
    </div>
  );
};

export default AdminLayout;
