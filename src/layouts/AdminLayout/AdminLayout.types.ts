import { RouteComponentProps } from '@reach/router';

export type TAdminLayoutProps = RouteComponentProps & {
  className?: string;
};
