import Breadcrumb from 'components/Breadcrumb/Breadcrumb';
import { TBreadcrumbProps } from './Breadcrumb.types';

export type { TBreadcrumbProps };
export default Breadcrumb;
