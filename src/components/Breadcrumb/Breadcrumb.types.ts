export type TBreadcrumbProps = {
  className?: string;
  data: TBreadcrumbData[];
};

export type TBreadcrumbData = {
  key: string;
  link: string;
  content: string | React.ReactElement;
};
