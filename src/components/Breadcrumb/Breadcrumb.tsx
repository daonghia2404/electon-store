import React from 'react';
import classNames from 'classnames';
import { Breadcrumb as AntdBreadcrumb } from 'antd';
import { Link, useLocation } from '@reach/router';

import { TBreadcrumbProps } from 'components/Breadcrumb/Breadcrumb.types';
import Icon, { EIconColor, EIconName } from 'components/Icon';

import './Breadcrumb.scss';

export const Breadcrumb: React.FC<TBreadcrumbProps> = ({ className, data }) => {
  const path = useLocation().pathname;

  return (
    <div className={classNames('Breadcrumb', className)}>
      <div className="container">
        <div className="Breadcrumb-wrapper">
          <AntdBreadcrumb separator={<Icon name={EIconName.arrowRight} color={EIconColor.doveGray} />}>
            {data.map((item) => (
              <AntdBreadcrumb.Item key={item.key}>
                <Link
                  to={item.link}
                  className={classNames('Breadcrumb-link', { 'active-link': item.link.includes(path) })}
                >
                  {item.content}
                </Link>
              </AntdBreadcrumb.Item>
            ))}
          </AntdBreadcrumb>
        </div>
      </div>
    </div>
  );
};

export default Breadcrumb;
