import Select from 'components/Select/Select';
import { TSelectProps } from './Select.types';

export type { TSelectProps };
export default Select;
