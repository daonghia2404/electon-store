import { TSelectOption } from 'common/types';

export type TSelectProps = {
  className?: string;
  placeholder?: string;
  defaultValue?: TSelectOption;
  value?: TSelectOption;
  options: TSelectOption[];
  dropdownClassName?: string;
  onChange?: (value: TSelectOption) => void;
};
