import React from 'react';
import classNames from 'classnames';
import { TSelectProps } from 'components/Select/Select.types';
import { Select as AntdSelect } from 'antd';

import Icon, { EIconColor, EIconName } from 'components/Icon';

import './Select.scss';

export const Select: React.FC<TSelectProps> = ({
  className,
  value,
  dropdownClassName,
  placeholder,
  defaultValue,
  options,
  onChange,
}) => {
  return (
    <div className={classNames('Select', className)}>
      <AntdSelect
        className="Select-control"
        value={value}
        placeholder={placeholder}
        defaultValue={defaultValue}
        labelInValue
        options={options}
        dropdownClassName={classNames('Select__dropdown', dropdownClassName)}
        suffixIcon={<Icon name={EIconName.caretDown} color={EIconColor.doveGray} />}
        getPopupContainer={(trigger: HTMLElement): HTMLElement => trigger}
        onChange={onChange}
      />
    </div>
  );
};

export default Select;
