import React from 'react';
import classNames from 'classnames';

import { TEmptyStateProps } from 'components/EmptyState';
import ImageEmptyIcon from 'assets/images/image-empty-icon.png';

import './EmptyState.scss';

export const EmptyState: React.FC<TEmptyStateProps> = ({
  className,
  title = 'Không Có Dữ Liệu',
  description = 'Vui lòng thử lại với từ khoá khác hoặc truy vấn bằng các bộ lọc khác.',
}) => {
  return (
    <div className={classNames('EmptyState flex flex-col items-center justify-center', className)}>
      <div className="EmptyState-icon">
        <img src={ImageEmptyIcon} alt="" />
      </div>
      <div className="EmptyState-title">{title}</div>
      <div className="EmptyState-description">{description}</div>
    </div>
  );
};

export default EmptyState;
