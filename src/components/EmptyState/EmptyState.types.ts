export type TEmptyStateProps = {
  className?: string;
  title?: string;
  description?: string;
};
