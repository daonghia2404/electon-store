import EmptyState from 'components/EmptyState/EmptyState';
import { TEmptyStateProps } from './EmptyState.types';

export type { TEmptyStateProps };
export default EmptyState;
