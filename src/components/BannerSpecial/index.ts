import BannerSpecial from 'components/BannerSpecial/BannerSpecial';
import { TBannerSpecialProps } from './BannerSpecial.types';

export type { TBannerSpecialProps };
export default BannerSpecial;
