import React from 'react';

import HoverBoxEffect from 'components/HoverBoxEffect';
import ImageFullBanner from 'assets/images/image-full-banner.png';
import { TBannerSpecialProps } from 'components/BannerSpecial';

import './BannerSpecial.scss';

export const BannerSpecial: React.FC<TBannerSpecialProps> = () => {
  return (
    <div className="BannerSpecial">
      <HoverBoxEffect />
      <img src={ImageFullBanner} alt="" />
    </div>
  );
};

export default BannerSpecial;
