import React from 'react';

import './HoverBoxEffect.scss';

export const HoverBoxEffect: React.FC = () => {
  return (
    <div className="HoverBoxEffect">
      <div className="HoverBoxEffect-box white" />
      <div className="HoverBoxEffect-box gray" />
    </div>
  );
};

export default HoverBoxEffect;
