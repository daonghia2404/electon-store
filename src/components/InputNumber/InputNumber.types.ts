import { SizeType } from 'antd/lib/config-provider/SizeContext';

export type TInputNumberProps = {
  className?: string;
  size?: SizeType;
  min?: number;
  max?: number;
  value?: number;
  defalutValue?: number;
  onChange?: (number: number) => void;
};
