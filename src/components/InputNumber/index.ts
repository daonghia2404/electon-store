import InputNumber from 'components/InputNumber/InputNumber';
import { TInputNumberProps } from './InputNumber.types';

export type { TInputNumberProps };
export default InputNumber;
