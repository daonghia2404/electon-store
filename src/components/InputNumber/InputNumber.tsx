import React from 'react';
import classNames from 'classnames';
import { InputNumber as AntdInputNumber } from 'antd';

import { TInputNumberProps } from 'components/InputNumber';

import './InputNumber.scss';

export const InputNumber: React.FC<TInputNumberProps> = ({ className, min, max, value, defalutValue, onChange }) => {
  return (
    <div className={classNames('InputNumber', className)}>
      <AntdInputNumber
        className="InputNumber-control"
        min={min}
        max={max}
        value={value}
        defaultValue={defalutValue}
        onChange={onChange}
      />
    </div>
  );
};

export default InputNumber;
