import Tabs from 'components/Tabs/Tabs';
import { TTabsProps } from './Tabs.types';

export type { TTabsProps };
export default Tabs;
