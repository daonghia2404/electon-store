import React from 'react';
import classNames from 'classnames';
import { Tabs as AntdTabs } from 'antd';

import { TTabsProps } from 'components/Tabs';

import './Tabs.scss';

const { TabPane } = AntdTabs;

export const Tabs: React.FC<TTabsProps> = ({ className, defaultActiveKey, dataTabs }) => {
  return (
    <div className={classNames('Tabs', className)}>
      <AntdTabs defaultActiveKey={defaultActiveKey}>
        {dataTabs.map((tab) => (
          <TabPane tab={tab.title} key={tab.key}>
            {tab.content}
          </TabPane>
        ))}
      </AntdTabs>
    </div>
  );
};

export default Tabs;
