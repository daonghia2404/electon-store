import React from 'react';

export type TTabsProps = {
  className?: string;
  defaultActiveKey?: string;
  dataTabs: TDataTab[];
};

export type TDataTab = {
  title: string;
  key: string;
  content: React.ReactElement;
};
