import { TSelectOption } from 'common/types';

export type TMultipleSelectProps = {
  className?: string;
  placeholder?: string;
  defaultValue?: TSelectOption;
  value?: TSelectOption;
  options: TSelectOption[];
  dropdownClassName?: string;
  onChange?: () => void;
};
