import MultipleSelect from 'components/MultipleSelect/MultipleSelect';
import { TMultipleSelectProps } from './MultipleSelect.types';

export type { TMultipleSelectProps };
export default MultipleSelect;
