import React from 'react';
import classNames from 'classnames';
import { Select } from 'antd';

import { TMultipleSelectProps } from 'components/MultipleSelect';
import Icon, { EIconColor, EIconName } from 'components/Icon';

import './MultipleSelect.scss';

export const MultipleSelect: React.FC<TMultipleSelectProps> = ({
  className,
  options,
  placeholder,
  value,
  dropdownClassName,
  onChange,
}) => {
  return (
    <div className={classNames('MultipleSelect', className)}>
      <Select
        dropdownClassName={classNames('MultipleSelect__dropdown', dropdownClassName)}
        mode="multiple"
        options={options}
        placeholder={placeholder}
        labelInValue
        value={value}
        suffixIcon={<Icon name={EIconName.caretDown} color={EIconColor.doveGray} />}
        getPopupContainer={(trigger: HTMLElement): HTMLElement => trigger}
        onChange={onChange}
      />
    </div>
  );
};

export default MultipleSelect;
