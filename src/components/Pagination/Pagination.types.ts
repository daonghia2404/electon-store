export type TPaginationProps = {
  className?: string;
  page: number;
  pageSize: number;
  total: number;
};
