import React from 'react';
import { Pagination as AntdPagination } from 'antd';
import classNames from 'classnames';

import { TPaginationProps } from 'components/Pagination/Pagination.types';

import './Pagination.scss';

export const Pagination: React.FC<TPaginationProps> = ({ page, pageSize, total, className }) => {
  return (
    <div className={classNames('Pagination', className)}>
      <AntdPagination current={page} pageSize={pageSize} total={total} hideOnSinglePage />
    </div>
  );
};

export default Pagination;
