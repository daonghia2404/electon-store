import { TBreadcrumbData } from 'components/Breadcrumb/Breadcrumb.types';

export type TPageHeaderProps = {
  className?: string;
  dataBreadcrumb: TBreadcrumbData[];
  title: string;
};
