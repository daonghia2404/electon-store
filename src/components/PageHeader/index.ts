import PageHeader from 'components/PageHeader/PageHeader';
import { TPageHeaderProps } from './PageHeader.types';

export type { TPageHeaderProps };
export default PageHeader;
