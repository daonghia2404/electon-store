import React from 'react';
import classNames from 'classnames';

import { TPageHeaderProps } from 'components/PageHeader';

import './PageHeader.scss';
import Breadcrumb from 'components/Breadcrumb';

export const PageHeader: React.FC<TPageHeaderProps> = ({ title, dataBreadcrumb, className, children }) => {
  return (
    <div className={classNames('PageHeader flex items-end justify-between', className)}>
      <div className="PageHeader-item">
        <div className="PageHeader-title">{title}</div>
        <div className="PageHeader-breadcrumb">
          <Breadcrumb data={dataBreadcrumb} />
        </div>
      </div>
      <div className="PageHeader-item">{children}</div>
    </div>
  );
};

export default PageHeader;
