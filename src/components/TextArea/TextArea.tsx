import React from 'react';
import classNames from 'classnames';
import { Input } from 'antd';

import { TTextAreaProps } from 'components/TextArea';

import './TextArea.scss';

const { TextArea: AntdTextArea } = Input;

export const TextArea: React.FC<TTextAreaProps> = ({ className, placeholder, value, onChange }) => {
  return (
    <div className={classNames('TextArea', className)}>
      <AntdTextArea className="TextArea-control" placeholder={placeholder} value={value} onChange={onChange} />
    </div>
  );
};

export default TextArea;
