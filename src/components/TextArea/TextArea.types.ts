import { ChangeEvent } from 'react';

export type TTextAreaProps = {
  className?: string;
  placeholder?: string;
  value?: string;
  onChange?: (e: ChangeEvent<HTMLTextAreaElement>) => void;
};
