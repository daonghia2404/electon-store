import Tooltip from 'components/Tooltip/Tooltip';
import { TTooltipProps } from './Tooltip.types';

export type { TTooltipProps };
export default Tooltip;
