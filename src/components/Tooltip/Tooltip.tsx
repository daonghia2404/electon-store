import React from 'react';
import { Tooltip as AntdTooltip } from 'antd';
import classNames from 'classnames';

import { TTooltipProps } from 'components/Tooltip/Tooltip.types';

import './Tooltip.scss';

export const Tooltip: React.FC<TTooltipProps> = ({
  title,
  placement = 'bottom',
  className,
  overlayClassName,
  children,
}) => {
  return (
    <div className={classNames('Tooltip', className)}>
      <AntdTooltip
        title={title}
        placement={placement}
        getPopupContainer={(trigger: HTMLElement): HTMLElement => trigger}
        overlayClassName={classNames('Tooltip-overlay', overlayClassName)}
      >
        <div className="Tooltip-wrapper">{children}</div>
      </AntdTooltip>
    </div>
  );
};

export default Tooltip;
