export type TUploadProps = {
  className?: string;
  aspect?: number;
  value?: string;
  onChange?: (url: string) => void;
};

export type TUploadFile = {
  uid: string;
  name: string;
  status: string;
  url: string;
};
