import UploadSingleImage from 'components/UploadSingleImage/UploadSingleImage';
import { TUploadProps } from './UploadSingleImage.types';

export type { TUploadProps };
export default UploadSingleImage;
