import React, { useState, useEffect } from 'react';
import { Upload as AntdUpload } from 'antd';
import classNames from 'classnames';
import ImgCrop from 'antd-img-crop';
import { useDispatch, useSelector } from 'react-redux';

import { TUploadProps } from 'components/UploadSingleImage';
import Icon, { EIconColor, EIconName } from 'components/Icon';
import { uploadAction } from 'redux/actions';
import { getFormDataFileUpload, validateImageTypeFile, showNotification } from 'utils/function';
import { TUploadResponse } from 'services/upload/types';
import env from 'env';
import Loading from 'components/Loading';
import { ETypeNotification } from 'common/enums';
import { TRootState } from 'redux/reducers';
import { EUploadAction } from 'redux/actions/upload/constants';

import './UploadSingleImage.scss';

const { Dragger } = AntdUpload;

export const UploadSingleImage: React.FC<TUploadProps> = ({ className, aspect, value, onChange }) => {
  const [urlImage, setUrlImage] = useState<string>(value || '');
  const dispatch = useDispatch();

  const loading = useSelector((state: TRootState) => state.loading[EUploadAction.UPLOAD_FILE]);

  const handleRequestServer = ({ file, onSuccess }: any): void => {
    if (validateImageTypeFile(file)) {
      const uploadingFile = getFormDataFileUpload(file);
      dispatch(
        uploadAction.request(uploadingFile, (data: TUploadResponse): void => {
          const urlImg = `${env.api.baseUrl.service}${data}`;
          setUrlImage(urlImg);
          onChange?.(urlImg);
          onSuccess();
        }),
      );
    } else {
      showNotification(
        ETypeNotification.error,
        'Ảnh yêu cầu phải có định dạng .png, .jpg hoặc .jpeg và dung lượng ảnh phải < 2MB',
      );
    }
  };

  useEffect(() => {
    if (value) setUrlImage(value);
  }, [value]);

  return (
    <div className={classNames('UploadSingleImage', className)}>
      <div className="UploadSingleImage-upload-main">
        {loading && (
          <div className="UploadSingleImage-upload-main-loading flex items-center justify-center">
            <Loading />
          </div>
        )}

        <ImgCrop aspect={aspect} rotate modalTitle="Cắt Ảnh" modalOk="Xác Nhận" modalCancel="Huỷ Bỏ">
          <Dragger accept=".jpg, .png, .jpeg" customRequest={handleRequestServer} fileList={[]}>
            <p className="ant-upload-drag-icon">
              <Icon name={EIconName.images} color={EIconColor.primary} />
            </p>
            <p className="ant-upload-text">Chọn hoặc kéo ảnh vào đây để tải ảnh lên</p>
            <p className="ant-upload-hint">
              Ảnh yêu cầu phải có định dạng .png, .jpg hoặc .jpeg và dung lượng ảnh phải {`<`} 2MB
            </p>
          </Dragger>
        </ImgCrop>
      </div>

      {urlImage && (
        <div className="UploadSingleImage-upload-image">
          <img src={urlImage} alt="" />
        </div>
      )}
    </div>
  );
};

export default UploadSingleImage;
