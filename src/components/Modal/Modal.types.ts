export type TModalProps = {
  className?: string;
  title?: string;
  visible: boolean;
  width?: number;
  onClose?: () => void;
};
