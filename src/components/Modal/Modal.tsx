import React from 'react';
import { Modal as AntdModal } from 'antd';
import classNames from 'classnames';

import { TModalProps } from 'components/Modal';

import './Modal.scss';
import Icon, { EIconColor, EIconName } from 'components/Icon';

export const Modal: React.FC<TModalProps> = ({ className, width, title, visible, onClose, children }) => {
  const handleCloseModal = (): void => {
    onClose?.();
  };

  return (
    <div className={classNames('Modal', className)}>
      <AntdModal
        closeIcon={<Icon name={EIconName.x} color={EIconColor.doveGray} />}
        visible={visible}
        width={width}
        title={title}
        wrapClassName="Modal-overlay"
        footer={null}
        getContainer={document.body}
        onCancel={handleCloseModal}
      >
        <div className="Modal-body">{children}</div>
      </AntdModal>
    </div>
  );
};

export default Modal;
