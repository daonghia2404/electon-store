import React, { useState } from 'react';
import classNames from 'classnames';
import { Table as AntdTable } from 'antd';

import { TTableProps } from 'components/Table/Table.types';
import Pagination from 'components/Pagination';
import Select from 'components/Select';
import { dataPageSizeOptions } from 'common/static';
import Input from 'components/Input';
import { TSelectOption } from 'common/types';
import EmptyState from 'components/EmptyState';

import './Table.scss';

export const Table: React.FC<TTableProps> = ({
  className,
  page,
  pageSize,
  total,
  columns,
  dataSources,
  loading,
  rowKey,
}) => {
  const [showPageSize] = useState<TSelectOption>({ value: String(pageSize), label: String(pageSize) });
  const [keyword] = useState<string>('');

  return (
    <div className={classNames('Table', className)}>
      <div className="Table-main-header flex items-center justify-between">
        <div className="Table-main-header-item flex items-center">
          Xem:
          <Select value={showPageSize} options={dataPageSizeOptions} />
          Mục
        </div>
        <div className="Table-main-header-item flex items-center">
          Tìm Kiếm:
          <Input value={keyword} placeholder="Nhập Từ Khoá Tìm Kiếm" />
        </div>
      </div>
      <div className="Table-main-body">
        <AntdTable
          locale={{ emptyText: <EmptyState /> }}
          pagination={false}
          columns={columns}
          dataSource={dataSources}
          loading={loading}
          rowKey={rowKey}
        />
      </div>
      <div className="Table-main-footer flex items-center justify-between">
        {/* <div className="Table-main-footer-item">
            Xem {page} đến {pageSize * page} trong {total} mục
          </div> */}
        <div className="Table-main-footer-item">Tổng cộng: {total}</div>
        <div className="Table-main-footer-item">
          <Pagination page={page} pageSize={pageSize} total={total} />
        </div>
      </div>
    </div>
  );
};

export default Table;
