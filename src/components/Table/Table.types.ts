import { TPaginationProps } from 'components/Pagination';

export type TTableProps = TPaginationProps & {
  className?: string;
  columns: TTableColumn[];
  dataSources: Array<any>;
  rowKey: string;
  loading?: boolean;
};

export type TTableColumn = {
  title: string;
  key: string | number;
  dataIndex: string;
  className?: string;
  fixed?: any;
  ellipsis?: boolean;
  width?: string | number;
  sorter?: ((a: any, b: any) => number) | boolean;
  render?: (text: string, record: any, index: number) => React.ReactElement | string;
};
