import Icon from './Icon';
import { TIconProps } from './Icon.types';
import { EIconName, EIconColor } from './Icon.enums';

export type { TIconProps };
export { EIconName, EIconColor };
export default Icon;
