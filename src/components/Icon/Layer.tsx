import React from 'react';

import { EIconColor } from 'components/Icon/Icon.enums';
import { TSvgProps } from 'components/Icon/Icon.types';

const Instagram: React.FC<TSvgProps> = ({ color = EIconColor.black }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke={color}
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
      className="feather feather-layers"
    >
      <polygon points="12 2 2 7 12 12 22 7 12 2" />
      <polyline points="2 17 12 22 22 17" />
      <polyline points="2 12 12 17 22 12" />
    </svg>
  );
};

export default Instagram;
