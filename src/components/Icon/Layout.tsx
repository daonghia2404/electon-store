import React from 'react';

import { EIconColor } from 'components/Icon/Icon.enums';
import { TSvgProps } from 'components/Icon/Icon.types';

const Instagram: React.FC<TSvgProps> = ({ color = EIconColor.black }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke={color}
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
      className="feather feather-layout"
    >
      <rect x="3" y="3" width="18" height="18" rx="2" ry="2" />
      <line x1="3" y1="9" x2="21" y2="9" />
      <line x1="9" y1="21" x2="9" y2="9" />
    </svg>
  );
};

export default Instagram;
