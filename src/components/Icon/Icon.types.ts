export type TIconProps = {
  name: string;
  color?: string;
  className?: string;
  onClick?: () => void;
};

export type TSvgProps = {
  color?: string;
};
