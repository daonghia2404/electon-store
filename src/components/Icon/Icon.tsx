import React from 'react';
import classNames from 'classnames';

import { TIconProps } from 'components/Icon/Icon.types';
import { EIconName } from 'components/Icon/Icon.enums';

import './Icon.scss';
import AngleDown from './AngleDown';
import ShoppingCart from './ShoppingCart';
import Heart from './Heart';
import Bars from './Bars';
import Mail from './Mail';
import MapMarker from './MapMarker';
import Mobile from './Mobile';
import Facebook from './Facebook';
import Twitter from './Twitter';
import Google from './Google';
import Youtube from './Youtube';
import Instagram from './Instagram';
import AngleLeft from './AngleLeft';
import AngleRight from './AngleRight';
import Eye from './Eye';
import Linked from './Linked';
import ArrowRight from './ArrowRight';
import CaretDown from './CaretDown';
import Check from './Check';
import Trash from './Trash';
import Star from './Star';
import StarFill from './StarFill';
import Users from './Users';
import Settings from './Settings';
import Menu from './Menu';
import Products from './Products';
import Layout from './Layout';
import Layer from './Layer';
import Edit from './Edit';
import Home from './Home';
import Moon from './Moon';
import Logout from './Logout';
import Notify from './Notify';
import PlusCircle from './PlusCircle';
import X from './X';
import Images from './Images';

const Icon: React.FC<TIconProps> = ({ name, color, className, onClick }) => {
  const handleClick = (): void => {
    onClick?.();
  };

  const renderIcon = (): React.ReactElement => {
    switch (name) {
      case EIconName.angleDown:
        return <AngleDown color={color} />;
      case EIconName.shoppingCart:
        return <ShoppingCart color={color} />;
      case EIconName.heart:
        return <Heart color={color} />;
      case EIconName.bars:
        return <Bars color={color} />;
      case EIconName.mail:
        return <Mail color={color} />;
      case EIconName.mapMarker:
        return <MapMarker color={color} />;
      case EIconName.mobile:
        return <Mobile color={color} />;
      case EIconName.facebook:
        return <Facebook color={color} />;
      case EIconName.twitter:
        return <Twitter color={color} />;
      case EIconName.google:
        return <Google color={color} />;
      case EIconName.youtube:
        return <Youtube color={color} />;
      case EIconName.instagram:
        return <Instagram color={color} />;
      case EIconName.angleLeft:
        return <AngleLeft color={color} />;
      case EIconName.angleRight:
        return <AngleRight color={color} />;
      case EIconName.eye:
        return <Eye color={color} />;
      case EIconName.linked:
        return <Linked color={color} />;
      case EIconName.arrowRight:
        return <ArrowRight color={color} />;
      case EIconName.caretDown:
        return <CaretDown color={color} />;
      case EIconName.check:
        return <Check color={color} />;
      case EIconName.trash:
        return <Trash color={color} />;
      case EIconName.star:
        return <Star color={color} />;
      case EIconName.starFill:
        return <StarFill color={color} />;
      case EIconName.users:
        return <Users color={color} />;
      case EIconName.settings:
        return <Settings color={color} />;
      case EIconName.menu:
        return <Menu color={color} />;
      case EIconName.products:
        return <Products color={color} />;
      case EIconName.layout:
        return <Layout color={color} />;
      case EIconName.layer:
        return <Layer color={color} />;
      case EIconName.edit:
        return <Edit color={color} />;
      case EIconName.home:
        return <Home color={color} />;
      case EIconName.logout:
        return <Logout color={color} />;
      case EIconName.moon:
        return <Moon color={color} />;
      case EIconName.notify:
        return <Notify color={color} />;
      case EIconName.plusCircle:
        return <PlusCircle color={color} />;
      case EIconName.x:
        return <X color={color} />;
      case EIconName.images:
        return <Images color={color} />;
      default:
        return <div>No Icon Found</div>;
    }
  };

  return (
    <div className={classNames('Icon flex items-center justify-center', className)} onClick={handleClick}>
      {renderIcon()}
    </div>
  );
};

export default Icon;
