import { CSSProperties } from 'react';

export type TBoxProductProps = {
  className?: string;
  link: string;
  style?: CSSProperties;
  _id: string;
  image: string;
  title: string;
  price: number;
  stock: number;
  description: string;
  color_id: Array<{ _id: string; name: string }>;
  config_id: Array<{ _id: string; name: string }>;
  sale: number;
  cat_id: { _id: string; title: string };
  content: string;
  createdAt?: string;
  updatedAt?: string;
  onAddCart?: (data: TBoxProductProps) => void;
};
