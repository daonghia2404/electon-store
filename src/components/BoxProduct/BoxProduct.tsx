import React, { useState } from 'react';
import { Link } from '@reach/router';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';

import { TBoxProductProps } from 'components/BoxProduct/BoxProduct.types';
import { formatNumberWithCommas, getUrlImage, showNotification } from 'utils/function';
import CircleColor from 'components/CircleColor';
import Button from 'components/Button';
import { EIconColor, EIconName } from 'components/Icon';
import Tooltip from 'components/Tooltip';
import { EProductColor, ETypeNotification } from 'common/enums';
import { TRootState } from 'redux/reducers';
import ModalPreviewProduct from 'containers/ModalPreviewProduct';
import { uiAction } from 'redux/actions';

import './BoxProduct.scss';

export const BoxProduct: React.FC<TBoxProductProps> = ({
  _id,
  link,
  image,
  title,
  price,
  sale = 0,
  stock,
  style,
  className,
  color_id,
  config_id,
  description,
  cat_id,
  createdAt,
  content,
  onAddCart,
}) => {
  const dispatch = useDispatch();
  const cartState = useSelector((state: TRootState) => state.ui.cart);
  const isNew = moment().diff(moment(createdAt), 'days') < 30;

  const [visibleQuickView, setVisibleQuickView] = useState<boolean>(false);

  const newPrice = price - price * (sale / 100);
  const isShowLabelSoldout = stock === 0;
  const isShowLabelNew = !isShowLabelSoldout && isNew;
  const isShowLabelSale = Boolean(!isShowLabelSoldout && !isNew && sale);
  const isShowLabelDiscount = !isShowLabelSoldout && Boolean(sale);
  const dataProduct = {
    _id,
    link,
    image,
    title,
    price,
    sale,
    stock,
    isNew,
    color_id,
    cat_id,
    description,
    config_id,
    content,
  };

  const handleAddCartClick = (): void => {
    onAddCart?.(dataProduct);
    showNotification(ETypeNotification.success, 'Thêm Vào Giỏ Hàng Thành Công');

    const [defaultColor] = color_id;
    const [defaultConfig] = config_id;
    const dataCart = {
      ...dataProduct,
      cartProductId: uuidv4(),
      color_id: defaultColor.name,
      config_id: defaultConfig.name,
      quantity: 1,
    };

    dispatch(uiAction.setCart([...cartState, dataCart]));
  };

  const handlePreviewClick = (): void => {
    setVisibleQuickView(true);
  };

  const handleCloseQuickView = (): void => {
    setVisibleQuickView(false);
  };

  return (
    <div className={classNames('BoxProduct', className)} style={style}>
      <ModalPreviewProduct
        title="Xem Nhanh"
        visible={visibleQuickView}
        onClose={handleCloseQuickView}
        data={dataProduct}
      />

      <div className="BoxProduct-image">
        {isShowLabelSoldout && <div className="BoxProduct-label soldout">Hết Hàng</div>}
        {isShowLabelNew && <div className="BoxProduct-label new">Mới</div>}
        {isShowLabelSale && <div className="BoxProduct-label new">Giảm Giá</div>}
        {isShowLabelDiscount && <div className="BoxProduct-label discount">-{sale}%</div>}

        <Link to={link}>
          <img src={getUrlImage(image)} alt="" />
        </Link>

        {!isShowLabelSoldout && (
          <div className="BoxProduct-actions">
            <Tooltip placement="left" title="Thêm Vào Giỏ Hàng">
              <Button
                className="BoxProduct-actions-btn cart"
                iconName={EIconName.shoppingCart}
                iconColor={EIconColor.white}
                type="primary"
                onClick={handleAddCartClick}
              />
            </Tooltip>

            <Tooltip placement="left" title="Xem Nhanh">
              <Button
                className="BoxProduct-actions-btn view"
                iconName={EIconName.eye}
                iconColor={EIconColor.white}
                type="primary"
                onClick={handlePreviewClick}
              />
            </Tooltip>

            <Tooltip placement="left" title="Yêu Thích">
              <Button
                className="BoxProduct-actions-btn favorite"
                iconName={EIconName.heart}
                iconColor={EIconColor.white}
                type="primary"
              />
            </Tooltip>
          </div>
        )}
      </div>
      <div className="BoxProduct-info">
        <div className="BoxProduct-color flex justify-center">
          {color_id.map((item) => (
            <CircleColor color={item.name as EProductColor} />
          ))}
        </div>
        <Link to={link} className="BoxProduct-title">
          {title}
        </Link>
        <div className="BoxProduct-price-wrapper flex items-center justify-center">
          <span className="BoxProduct-price">{formatNumberWithCommas(newPrice)}đ</span>
          {newPrice && <del className="BoxProduct-old-price">{formatNumberWithCommas(price)}đ</del>}
        </div>
      </div>
    </div>
  );
};

export default BoxProduct;
