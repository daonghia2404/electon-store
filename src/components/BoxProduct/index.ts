import BoxProduct from 'components/BoxProduct/BoxProduct';
import { TBoxProductProps } from './BoxProduct.types';

export type { TBoxProductProps };
export default BoxProduct;
