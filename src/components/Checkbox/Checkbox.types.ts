export type TCheckboxProps = {
  className?: string;
  label?: string | React.ReactElement;
  checked?: boolean;
  indeterminate?: boolean;
  onChange?: (checked: boolean) => void;
};
