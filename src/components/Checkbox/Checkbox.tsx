import React from 'react';
import classNames from 'classnames';
import { Checkbox as AntdCheckbox } from 'antd';

import { TCheckboxProps } from 'components/Checkbox';

import './Checkbox.scss';

export const Checkbox: React.FC<TCheckboxProps> = ({ className, label, checked, indeterminate, onChange }) => {
  const handleCheckboxChange = (): void => {
    onChange?.(!checked);
  };

  return (
    <div className={classNames('Checkbox', className)}>
      {typeof checked !== 'undefined' ? (
        <AntdCheckbox indeterminate={indeterminate} checked={checked} onChange={handleCheckboxChange}>
          {label}
        </AntdCheckbox>
      ) : (
        <AntdCheckbox indeterminate={indeterminate}>{label}</AntdCheckbox>
      )}
    </div>
  );
};

export default Checkbox;
