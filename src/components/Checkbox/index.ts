import Checkbox from 'components/Checkbox/Checkbox';
import { TCheckboxProps } from './Checkbox.types';

export type { TCheckboxProps };
export default Checkbox;
