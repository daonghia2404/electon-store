import Section from 'components/Section/Section';
import { TSectionProps } from './Section.types';

export type { TSectionProps };
export default Section;
