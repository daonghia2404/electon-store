export type TSectionProps = {
  className?: string;
  title?: string;
};
