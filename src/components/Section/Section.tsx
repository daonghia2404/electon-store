import React from 'react';
import classNames from 'classnames';

import { TSectionProps } from 'components/Section/Section.types';

import './Section.scss';

export const Section: React.FC<TSectionProps> = ({ className, title, children }) => {
  return (
    <div className={classNames('Section', className)}>
      <div className="container">
        <div className="Section-wrapper">
          {title && (
            <div className="Section-header flex justify-center">
              <h2 className="Section-title">{title}</h2>
            </div>
          )}

          <div className="Section-body">{children}</div>
        </div>
      </div>
    </div>
  );
};

export default Section;
