import React from 'react';
import { Link } from '@reach/router';
import classNames from 'classnames';

import { TBoxCategoryProps } from 'components/BoxCategory/BoxCategory.types';
import HoverBoxEffect from 'components/HoverBoxEffect';

import './BoxCategory.scss';

export const BoxCategory: React.FC<TBoxCategoryProps> = ({ className, link, label, image, style }) => {
  return (
    <div className={classNames('BoxCategory', className)} style={style}>
      <div className="BoxCategory-image">
        <HoverBoxEffect />
        <img src={image} alt="" />
      </div>
      <Link to={link} className="BoxCategory-label">
        {label}
      </Link>
    </div>
  );
};

export default BoxCategory;
