import { CSSProperties } from 'react';

export type TBoxCategoryProps = {
  className?: string;
  label: string;
  image: string;
  link: string;
  style?: CSSProperties;
};
