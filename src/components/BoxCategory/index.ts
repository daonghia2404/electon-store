import BoxCategory from 'components/BoxCategory/BoxCategory';
import { TBoxCategoryProps } from './BoxCategory.types';

export type { TBoxCategoryProps };
export default BoxCategory;
