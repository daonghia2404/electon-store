import BoxShopNow from 'components/BoxShopNow/BoxShopNow';
import { TBoxShopNowProps } from './BoxShopNow.types';

export type { TBoxShopNowProps };
export default BoxShopNow;
