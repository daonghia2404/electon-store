import React from 'react';
import classNames from 'classnames';

import Button from 'components/Button';
import { TBoxShopNowProps } from 'components/BoxShopNow/BoxShopNow.types';
import HoverBoxEffect from 'components/HoverBoxEffect';

import './BoxShopNow.scss';

export const BoxShopNow: React.FC<TBoxShopNowProps> = ({ className, image, title, saleOff }) => {
  return (
    <div className={classNames('BoxShopNow', className)}>
      <HoverBoxEffect />
      <div className="BoxShopNow-image">
        <img src={image} alt="" />
      </div>
      <div className="BoxShopNow-content flex flex-col items-end">
        <h4 className="BoxShopNow-title">{title}</h4>
        <h4 className="BoxShopNow-title">{saleOff}</h4>
        <Button type="primary" title="MUA NGAY" />
      </div>
    </div>
  );
};

export default BoxShopNow;
