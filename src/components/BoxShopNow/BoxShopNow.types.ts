export type TBoxShopNowProps = {
  className?: string;
  image: string;
  title: string;
  saleOff: string;
};
