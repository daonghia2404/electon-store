import { SizeType } from 'antd/lib/config-provider/SizeContext';
import { EColor } from 'common/enums';
import { EIconName } from 'components/Icon';

export type TButtonProps = {
  className?: string;
  htmlType?: 'button' | 'submit' | 'reset';
  title?: string;
  size?: SizeType;
  type?: 'link' | 'text' | 'ghost' | 'default' | 'primary' | 'dashed';
  iconName?: EIconName;
  link?: string;
  iconColor?: EColor;
  reverse?: boolean;
  loading?: boolean;
  disabled?: boolean;
  onClick?: () => void;
};
