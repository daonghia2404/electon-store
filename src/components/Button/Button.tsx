import React from 'react';
import { Button as AntdButton } from 'antd';
import classNames from 'classnames';
import { navigate } from '@reach/router';

import { TButtonProps } from 'components/Button/Button.types';

import './Button.scss';
import Icon from 'components/Icon';

export const Button: React.FC<TButtonProps> = ({
  className,
  link,
  htmlType,
  title,
  size = 'middle',
  type = 'default',
  reverse,
  iconName,
  iconColor,
  disabled,
  loading,
  onClick,
}) => {
  const handleButtonClick = (): void => {
    onClick?.();
    if (link) navigate(link);
  };

  return (
    <div className={classNames('Button', className)}>
      <AntdButton
        className={classNames('Button-control flex items-center justify-center', { 'row-reverse': reverse })}
        type={type}
        htmlType={htmlType}
        size={size}
        onClick={handleButtonClick}
        disabled={disabled}
        loading={loading}
      >
        {title && <div className="Button-control-title">{title}</div>}
        {iconName && (
          <div className="Button-control-icon">
            <Icon name={iconName} color={iconColor} />
          </div>
        )}
      </AntdButton>
    </div>
  );
};

export default Button;
