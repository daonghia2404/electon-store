import Button from 'components/Button/Button';
import { TButtonProps } from './Button.types';

export type { TButtonProps };
export default Button;
