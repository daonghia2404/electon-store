export type TLoadingProps = {
  className?: string;
  minHeight?: number | string;
};
