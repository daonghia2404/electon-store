import Loading from 'components/Loading/Loading';
import { TLoadingProps } from './Loading.types';

export type { TLoadingProps };
export default Loading;
