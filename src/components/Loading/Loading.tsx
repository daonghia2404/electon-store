import React from 'react';
import { Spin } from 'antd';
import classNames from 'classnames';

import { TLoadingProps } from 'components/Loading';

import './Loading.scss';

export const Loading: React.FC<TLoadingProps> = ({ className, minHeight }) => {
  return (
    <div className={classNames('Loading flex items-center justify-center', className)} style={{ minHeight }}>
      <Spin />
    </div>
  );
};

export default Loading;
