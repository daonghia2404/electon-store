import React from 'react';
import { Link } from '@reach/router';
import classNames from 'classnames';

import { TBlogBoxProps } from 'components/BlogBox/BlogBox.types';
import Icon, { EIconName } from 'components/Icon';

import './BlogBox.scss';

export const BlogBox: React.FC<TBlogBoxProps> = ({
  className,
  style,
  image,
  createdDate,
  link,
  title,
  description,
}) => {
  return (
    <div className={classNames('BlogBox', className)} style={style}>
      <div className="BlogBox-image">
        <div className="BlogBox-date">{createdDate}</div>
        <Link to={link} className="BlogBox-overlay flex items-center justify-center">
          <div className="BlogBox-overlay-icon flex items-center justify-center">
            <Icon name={EIconName.linked} />
          </div>
        </Link>
        <Link to={link}>
          <img src={image} alt="" />
        </Link>
      </div>
      <div className="BlogBox-info">
        <Link to={link} className="BlogBox-title">
          {title}
        </Link>
        <div className="BlogBox-des">{description}</div>
        <Link to={link} className="BlogBox-read-more">
          ĐỌC THÊM
        </Link>
      </div>
    </div>
  );
};

export default BlogBox;
