import BlogBox from 'components/BlogBox/BlogBox';
import { TBlogBoxProps } from './BlogBox.types';

export type { TBlogBoxProps };
export default BlogBox;
