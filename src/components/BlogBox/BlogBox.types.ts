import { CSSProperties } from 'react';

export type TBlogBoxProps = {
  className?: string;
  style?: CSSProperties;
  image: string;
  createdDate: string;
  link: string;
  title: string;
  description: string;
};
