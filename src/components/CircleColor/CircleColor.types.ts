import { EProductColor } from 'common/enums';

export type TCircleColorProps = {
  className?: string;
  active?: boolean;
  color: EProductColor;
  onClick?: () => void;
};
