import CircleColor from 'components/CircleColor/CircleColor';
import { TCircleColorProps } from './CircleColor.types';

export type { TCircleColorProps };
export default CircleColor;
