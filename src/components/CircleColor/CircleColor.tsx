import React from 'react';
import classNames from 'classnames';

import { TCircleColorProps } from 'components/CircleColor';
import { dataHexColor } from 'common/static';

import './CircleColor.scss';

export const CircleColor: React.FC<TCircleColorProps> = ({ className, active, color, onClick }) => {
  return (
    <div
      className={classNames('CircleColor', className, { active })}
      style={{ background: dataHexColor[color] }}
      onClick={onClick}
    />
  );
};

export default CircleColor;
