import { SizeType } from 'antd/lib/config-provider/SizeContext';
import { ChangeEvent } from 'react';

export type TInputProps = {
  className?: string;
  placeholder?: string;
  size?: SizeType;
  value?: string;
  type?: 'text' | 'password';
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
};
