import Input from 'components/Input/Input';
import { TInputProps } from './Input.types';

export type { TInputProps };
export default Input;
