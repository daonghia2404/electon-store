import React from 'react';
import classNames from 'classnames';
import { Input as AntdInput } from 'antd';

import { TInputProps } from 'components/Input/Input.types';

import './Input.scss';

export const Input: React.FC<TInputProps> = ({
  className,
  type = 'text',
  value,
  placeholder,
  onChange,
  size = 'middle',
}) => {
  return (
    <div className={classNames('Input', className)}>
      <AntdInput
        type={type}
        className="Input-control"
        value={value}
        placeholder={placeholder}
        size={size}
        onChange={onChange}
      />
    </div>
  );
};

export default Input;
