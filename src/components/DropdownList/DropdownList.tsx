import React from 'react';
import { Dropdown as AntdDropdown } from 'antd';
import { Link } from '@reach/router';

import { TDropdownListProps } from 'components/DropdownList/DropdownList.types';

import './DropdownList.scss';

export const DropdownList: React.FC<TDropdownListProps> = ({ options, children, trigger }) => {
  const renderListOverlay = (): React.ReactElement => {
    return (
      <div className="DropdownList-overlay">
        {options.map((option) => (
          <Link to={option.link || ''} key={option.key} className="DropdownList-option">
            {option.label}
          </Link>
        ))}
      </div>
    );
  };

  return (
    <div className="DropdownList">
      <AntdDropdown
        overlay={renderListOverlay}
        getPopupContainer={(node: HTMLElement): HTMLElement => node}
        trigger={trigger || ['click']}
      >
        <div className="DropdownList-body">{children}</div>
      </AntdDropdown>
    </div>
  );
};

export default DropdownList;
