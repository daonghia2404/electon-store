export type TDropdownListOption = {
  key: string;
  label: string;
  link?: string;
};

export type TDropdownListProps = {
  className?: string;
  trigger?: ('click' | 'hover' | 'contextMenu')[];
  options: TDropdownListOption[];
};
