import DropdownList from 'components/DropdownList/DropdownList';
import { TDropdownListProps } from './DropdownList.types';

export type { TDropdownListProps };
export default DropdownList;
