import React, { useState } from 'react';

import { TAdminHeaderProps } from 'containers/AdminHeader/AdminHeader.types';
import Button from 'components/Button';
import Icon, { EIconColor, EIconName } from 'components/Icon';
import helper from 'services/helpers';
import { navigate } from '@reach/router';
import { LayoutPaths } from 'pages/routers';
import ModalConfirm from 'containers/ModalConfirm';

import './AdminHeader.scss';

export const AdminHeader: React.FC<TAdminHeaderProps> = () => {
  const [visibleModalConfirmLogout, setVisibleModalConfirmLogout] = useState<boolean>(false);

  const handleLogout = (): void => {
    helper.clearTokens();
    navigate(LayoutPaths.Auth);
    handleCloseConfirmLogout();
  };

  const handleOpenConfirmLogout = (): void => {
    setVisibleModalConfirmLogout(true);
  };

  const handleCloseConfirmLogout = (): void => {
    setVisibleModalConfirmLogout(false);
  };

  return (
    <div className="AdminHeader flex items-center justify-end">
      <div className="AdminHeader-icon">
        <div className="AdminHeader-icon-notify" />
        <Icon name={EIconName.notify} color={EIconColor.black} />
      </div>
      <div className="AdminHeader-icon">
        <Icon name={EIconName.moon} color={EIconColor.black} />
      </div>
      <div className="AdminHeader-logout">
        <Button
          iconColor={EIconColor.white}
          iconName={EIconName.logout}
          title="Đăng Xuất"
          reverse
          className="secondary"
          onClick={handleOpenConfirmLogout}
        />
      </div>

      <ModalConfirm
        title="Đăng Xuất"
        description="Bạn có chắc chắn muốn đăng xuất tài khoản khỏi phiên đăng nhập này?"
        visible={visibleModalConfirmLogout}
        onConfirm={handleLogout}
        onClose={handleCloseConfirmLogout}
      />
    </div>
  );
};

export default AdminHeader;
