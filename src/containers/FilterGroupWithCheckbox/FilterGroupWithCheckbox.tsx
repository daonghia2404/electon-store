import React from 'react';
import classNames from 'classnames';
import { Card } from 'antd';

import { TFilterGroupWithCheckboxProps } from 'containers/FilterGroupWithCheckbox/FilterGroupWithCheckbox.types';
import Checkbox from 'components/Checkbox';

import './FilterGroupWithCheckbox.scss';

export const FilterGroupWithCheckbox: React.FC<TFilterGroupWithCheckboxProps> = ({ title, className, data }) => {
  return (
    <div className={classNames('FilterGroupWithCheckbox', className)}>
      <Card title={title} className="no-padding">
        <div className="FilterGroupWithCheckbox-wrapper">
          {data.map((item) => (
            <Checkbox
              key={item.key}
              className="FilterGroupWithCheckbox-checkbox"
              label={`${item.label}${item.total ? `(${item.total})` : ''}`}
            />
          ))}
        </div>
      </Card>
    </div>
  );
};

export default FilterGroupWithCheckbox;
