export type TFilterGroupWithCheckboxProps = {
  className?: string;
  data: TFilterGroupWithCheckboxData[];
  title: string;
};

export type TFilterGroupWithCheckboxData = {
  key: string;
  label: string;
  total?: number;
};
