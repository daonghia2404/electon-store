import ImageBrand1 from 'assets/images/brands/image-brand-1.png';
import ImageBrand2 from 'assets/images/brands/image-brand-2.png';
import ImageBrand3 from 'assets/images/brands/image-brand-3.png';
import ImageBrand4 from 'assets/images/brands/image-brand-4.png';
import ImageBrand5 from 'assets/images/brands/image-brand-5.png';
import ImageBrand6 from 'assets/images/brands/image-brand-6.png';

import Member1 from 'assets/images/brands/member-1.png';
import Member2 from 'assets/images/brands/member-2.png';
import Member3 from 'assets/images/brands/member-3.png';

// export const dataBrands = [
//   {
//     image: ImageBrand1,
//   },
//   {
//     image: ImageBrand2,
//   },
//   {
//     image: ImageBrand3,
//   },
//   {
//     image: ImageBrand4,
//   },
//   {
//     image: ImageBrand5,
//   },
//   {
//     image: ImageBrand6,
//   },
// ];

export const dataBrands = [
  {
    image: Member1,
  },
  {
    image: Member2,
  },
  {
    image: Member3,
  },
];
