import React from 'react';

import Carousels from 'components/Carousels';
import Section from 'components/Section';
import { dataBrands } from 'containers/Brands/Brands.datas';

import './Brands.scss';

export const Brands: React.FC = () => {
  return (
    <div className="Brands">
      <Section title="Thành Viên">
        <div className="Brands-wrapper">
          <Carousels slidesToShow={3} autoplay dots={false} arrows={false}>
            {dataBrands.map((brand) => (
              <div className="Brands-item">
                <img src={brand.image} alt="" />
              </div>
            ))}
          </Carousels>
        </div>
      </Section>
    </div>
  );
};

export default Brands;
