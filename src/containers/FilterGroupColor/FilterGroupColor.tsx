import React from 'react';
import { Card } from 'antd';

import { dataColors, dataHexColor } from 'common/static';
import Icon, { EIconColor, EIconName } from 'components/Icon';

import './FilterGroupColor.scss';

export const FilterGroupColor: React.FC = () => {
  return (
    <div className="FilterGroupColor">
      <Card title="Màu Sắc">
        <div className="FilterGroupColor-wrapper flex flex-wrap">
          {dataColors.map((item) => (
            <div className="FilterGroupColor-item" style={{ background: dataHexColor[item.key] }}>
              <Icon name={EIconName.check} color={EIconColor.white} />
            </div>
          ))}
        </div>
      </Card>
    </div>
  );
};

export default FilterGroupColor;
