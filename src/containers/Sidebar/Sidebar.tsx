import React from 'react';
import { Link, useLocation } from '@reach/router';

import Logo from 'assets/images/logo.png';
import Icon, { EIconColor, EIconName } from 'components/Icon';
import { dataSidebarMenu } from 'containers/Sidebar/Sidebar.datas';
import ImageAvatar from 'assets/images/image-avatar-sample.png';

import './Sidebar.scss';
import classNames from 'classnames';

export const Sidebar: React.FC = () => {
  const path = useLocation().pathname;

  return (
    <div className="Sidebar">
      <div className="Sidebar-top flex items-center justify-between">
        <div className="Sidebar-logo">
          <img src={Logo} alt="" />
        </div>
        <div className="Sidebar-toggle-menu">
          <Icon name={EIconName.menu} color={EIconColor.doveGray} />
        </div>
      </div>

      <div className="Sidebar-body">
        <div className="Sidebar-user">
          <div className="Sidebar-user-avatar">
            <img src={ImageAvatar} alt="" />
          </div>
          <div className="Sidebar-user-name">Admin</div>
          <div className="Sidebar-user-des">Quản Trị Electon Store</div>
        </div>
        <div className="Sidebar-menu">
          {dataSidebarMenu.map((item) => (
            <Link
              key={item.key}
              to={item.link}
              className={classNames('Sidebar-menu-item flex items-center justify-between', {
                active: path.includes(item.dataActive),
              })}
            >
              <div className="Sidebar-menu-item-icon">
                <Icon name={item.icon} color={EIconColor.mineShaft} />
              </div>
              <div className="Sidebar-menu-item-title">{item.label}</div>
              <div className="Sidebar-menu-item-icon arrow">
                <Icon name={EIconName.angleRight} color={EIconColor.mineShaft} />
              </div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
