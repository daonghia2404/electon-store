import { EIconName } from 'components/Icon';
import { LayoutPaths, ModulePaths, Paths } from 'pages/routers';

export const dataSidebarMenu = [
  {
    icon: EIconName.home,
    label: 'Tổng Quan',
    key: 'home',
    link: `${LayoutPaths.Admin}`,
    dataActive: ` `,
  },
  {
    icon: EIconName.layout,
    label: 'Giao Diện',
    key: 'layout',
    link: `${LayoutPaths.Admin}`,
    dataActive: ` `,
  },
  {
    icon: EIconName.products,
    label: 'Sản Phẩm',
    key: 'products',
    link: `${LayoutPaths.Admin}${Paths.ProductsManagement}`,
    dataActive: `${LayoutPaths.Admin}${ModulePaths.ProductsManagement}`,
  },
  {
    icon: EIconName.layer,
    label: 'Đơn Hàng',
    key: 'layer',
    link: `${LayoutPaths.Admin}${Paths.OrdersManagement}`,
    dataActive: `${LayoutPaths.Admin}${Paths.OrdersManagement}`,
  },
  {
    icon: EIconName.edit,
    label: 'Bài Viết',
    key: 'edit',
    link: `${LayoutPaths.Admin}`,
    dataActive: ` `,
  },
  {
    icon: EIconName.users,
    label: 'Người Dùng',
    key: 'users',
    link: `${LayoutPaths.Admin}`,
    dataActive: ` `,
  },
  {
    icon: EIconName.settings,
    label: 'Cài Đặt',
    key: 'settings',
    link: `${LayoutPaths.Admin}`,
    dataActive: ` `,
  },
];
