import ImageService1 from 'assets/images/services/icon-fast-delivery.png';
import ImageService2 from 'assets/images/services/icon-24-7-support.png';
import ImageService3 from 'assets/images/services/icon-best-quality.png';
import ImageService4 from 'assets/images/services/icon-gift-voucher.png';

export const dataServices = [
  { key: 'delivery', icon: ImageService1, title: 'Giao Hàng Miễn Phí', des: 'Theo Dõi Giao Hàng Nhanh' },
  { key: 'support', icon: ImageService2, title: 'Hỗ Trợ 24/7', des: 'Nếu bạn cần trợ giúp, chúng tôi cung cấp' },
  { key: 'quality', icon: ImageService3, title: 'Chất Lượng Tốt', des: 'Đảm Bảo Sản Phẩm Tốt Nhất' },
  { key: 'gift', icon: ImageService4, title: 'Quà Tặng', des: 'Chính Sách Quà Tặng Cho Khách Hàng' },
];
