import React from 'react';

import Section from 'components/Section';
import { dataServices } from 'containers/Services/Services.datas';

import './Services.scss';

export const Services: React.FC = () => {
  return (
    <div className="Services">
      <Section>
        <div className="Services-wrapper flex justify-between">
          {dataServices.map((item) => (
            <div key={item.key} className="Services-item">
              <div className="Services-icon">
                <img src={item.icon} alt="" />
              </div>
              <h4 className="Services-title">{item.title}</h4>
              <p className="Services-des">{item.des}</p>
            </div>
          ))}
        </div>
      </Section>
    </div>
  );
};

export default Services;
