import React from 'react';

import BoxCategory from 'components/BoxCategory';
import Carousels from 'components/Carousels';
import Section from 'components/Section';
import { Paths } from 'pages/routers';
import { useSelector } from 'react-redux';
import { TRootState } from 'redux/reducers';
import { renderImageCategories } from 'utils/function';

export const SliderCategories: React.FC = () => {
  const categoriesState = useSelector((state: TRootState) => state.categoriesState.categories);
  return (
    <div className="SliderCategories">
      <Section title="TOP DANH MỤC">
        <Carousels arrows={false} dots={false} slidesToShow={4}>
          {categoriesState.map((category) => (
            <div key={category._id} className="SliderCategories-item">
              <BoxCategory
                style={{ margin: 10 }}
                link={Paths.Categories(category.title)}
                image={renderImageCategories(category.title)}
                label={category.title}
              />
            </div>
          ))}
        </Carousels>
      </Section>
    </div>
  );
};

export default SliderCategories;
