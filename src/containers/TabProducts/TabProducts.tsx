import React from 'react';
import { useSelector } from 'react-redux';

import Section from 'components/Section';
import Tabs from 'components/Tabs';
import Carousels from 'components/Carousels';
import BoxProduct from 'components/BoxProduct';
import { Paths } from 'pages/routers';
import { TRootState } from 'redux/reducers';

import './TabProducts.scss';

export const TabProducts: React.FC = () => {
  const productsState = useSelector((state: TRootState) => state.productsState.products);

  const renderNewProducts = (): React.ReactElement => {
    return (
      <div className="TabProducts-item-wrapper">
        <Carousels slidesPerRow={2} slidesToShow={4} dots={false} infinite={false}>
          {productsState.map((product) => (
            <div className="TabProducts-item">
              <BoxProduct {...product} link={Paths.Product(product._id)} style={{ margin: 10 }} />
            </div>
          ))}
        </Carousels>
      </div>
    );
  };
  const renderFeaturedProducts = (): React.ReactElement => {
    return (
      <div className="TabProducts-item-wrapper">
        <Carousels slidesPerRow={2} slidesToShow={4} dots={false} infinite={false}>
          {productsState.map((product) => (
            <div className="TabProducts-item">
              <BoxProduct {...product} link={Paths.Product(product._id)} style={{ margin: 10 }} />
            </div>
          ))}
        </Carousels>
      </div>
    );
  };

  const renderBestSellerProducts = (): React.ReactElement => {
    return (
      <div className="TabProducts-item-wrapper">
        <Carousels slidesPerRow={2} slidesToShow={4} dots={false} infinite={false}>
          {productsState
            .filter((item) => item.sale > 0)
            .map((product) => (
              <div className="TabProducts-item">
                <BoxProduct {...product} link={Paths.Product(product._id)} style={{ margin: 10 }} />
              </div>
            ))}
        </Carousels>
      </div>
    );
  };

  const dataTabs = [
    {
      title: 'Sản Phẩm Mới',
      key: 'new-product',
      content: renderNewProducts(),
    },
    {
      title: 'Sản Phẩm Phổ Biến',
      key: 'featured-product',
      content: renderFeaturedProducts(),
    },
    {
      title: 'Sản Phẩm Bán Chạy',
      key: 'best-seller-product',
      content: renderBestSellerProducts(),
    },
  ];

  return (
    <div className="TabProducts">
      <Section title="BỘ SƯU TẬP">
        <Tabs dataTabs={dataTabs} />
      </Section>
    </div>
  );
};

export default TabProducts;
