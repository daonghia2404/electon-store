import React from 'react';
import classNames from 'classnames';

import Button from 'components/Button';
import { TSelectConfigProps } from 'containers/SelectConfig/SelectConfig.types';
import { TConfigOption } from 'services/config/types';

import './SelectConfig.scss';

export const SelectConfig: React.FC<TSelectConfigProps> = ({ className, data, value, onChange }) => {
  const handleClickConfigOption = (currentValue: TConfigOption): void => {
    onChange?.({ label: currentValue.name, value: currentValue._id });
  };

  return (
    <div className={classNames('SelectConfig', className)}>
      <div className="SelectConfig-wrapper flex items-center flex-wrap">
        <div className="SelectConfig-label">Cấu Hình: </div>
        {data.map((item) =>
          item._id === value?.value ? (
            <Button title={item.name} type="primary" onClick={(): void => handleClickConfigOption(item)} />
          ) : (
            <Button className="secondary" title={item.name} onClick={(): void => handleClickConfigOption(item)} />
          ),
        )}
      </div>
    </div>
  );
};

export default SelectConfig;
