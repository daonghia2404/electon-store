import { TSelectOption } from 'common/types';
import { TConfigOption } from 'services/config/types';

export type TSelectConfigProps = {
  className?: string;
  value: TSelectOption | null;
  data: TConfigOption[];
  onChange?: (data: TSelectOption) => void;
};
