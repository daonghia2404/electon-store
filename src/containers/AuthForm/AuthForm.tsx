import React from 'react';
import { Form } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { Link, navigate } from '@reach/router';
import { useDispatch, useSelector } from 'react-redux';

import { TAuthFormProps } from 'containers/AuthForm/AuthForm.types';
import Logo from 'assets/images/logo.png';
import LogoFacebook from 'assets/icons/icon-logo-facebook.svg';
import LogoGoogle from 'assets/icons/icon-logo-google.svg';
import Input from 'components/Input';
import Button from 'components/Button';
import { validationRules } from 'utils/function';
import { loginAction } from 'redux/actions';
import { TLoginBody } from 'services/auth/types';

import './AuthForm.scss';
import { LayoutPaths } from 'pages/routers';
import { TRootState } from 'redux/reducers';
import { EAuthAction } from 'redux/actions/auth/constants';

export const AuthForm: React.FC<TAuthFormProps> = () => {
  const dispatch = useDispatch();
  const [form] = useForm();

  const loading = useSelector((state: TRootState) => state.loading[EAuthAction.LOGIN]);

  const handleLoginSubmit = (values: TLoginBody): void => {
    const { username, password } = values;
    const body = {
      username,
      password,
    };
    dispatch(loginAction.request(body, handleLoginSuccess));
  };

  const handleLoginSuccess = (): void => {
    navigate(`${LayoutPaths.Admin}`);
  };

  return (
    <div className="AuthForm">
      <div className="AuthForm-logo">
        <img src={Logo} alt="" />
      </div>
      <div className="AuthForm-social flex justify-center">
        <div className="AuthForm-social-item facebook">
          <img src={LogoFacebook} alt="" />
        </div>
        <div className="AuthForm-social-item google">
          <img src={LogoGoogle} alt="" />
        </div>
      </div>
      <p className="AuthForm-des">Đăng Nhập Bằng Mạng Xã Hội</p>

      <div className="AuthForm-title">ĐĂNG NHẬP</div>
      <p className="AuthForm-des">Vui lòng đăng nhập chi tiết tài khoản bên dưới</p>

      <Form form={form} className="AuthForm-form" layout="vertical" onFinish={handleLoginSubmit}>
        <Form.Item name="username" label="Tên Đăng Nhập" rules={[validationRules.required()]}>
          <Input size="large" placeholder="Nhập Tên Đăng Nhập" />
        </Form.Item>
        <Form.Item name="password" label="Mật Khẩu" rules={[validationRules.required()]}>
          <Input type="password" size="large" placeholder="Nhập Mật Khẩu" />
        </Form.Item>
        <Form.Item>
          <div className="AuthForm-form-btns flex justify-center">
            <Button loading={loading} htmlType="submit" size="large" className="secondary" title="Đăng Nhập" />
          </div>
        </Form.Item>
      </Form>

      <Link className="AuthForm-link" to="/" style={{ margin: '-10px 0 30px 0' }}>
        Quên Mật Khẩu ?
      </Link>
      <div className="AuthForm-subtitle">Bạn Không Có Tài Khoản ?</div>
      <Link className="AuthForm-link" to="/">
        Tạo Tài Khoản
      </Link>
    </div>
  );
};

export default AuthForm;
