import { dataCategories } from 'common/static';
import { Paths } from 'pages/routers';

export const optionsDropdownMyAccount = [
  { key: 'sign-in', label: 'Đăng Nhập' },
  { key: 'sign-up', label: 'Đăng Ký' },
];

export const optionsDropdownLang = [
  { key: 'vi', label: 'Tiếng Việt' },
  { key: 'en', label: 'Tiếng Anh' },
];

export const dataHeaderMenu = [
  {
    key: 'home',
    link: Paths.Home,
    label: 'Trang Chủ',
  },
  {
    key: 'shop',
    label: 'Mua Sắm',
    child: dataCategories,
  },
  {
    key: 'pages',
    label: 'Trang',
    child: [
      { key: 'about', label: 'Về Chúng Tôi' },
      { key: 'contact', label: 'Liên Hệ' },
      { key: 'faq', label: 'FAQ' },
      { key: 'payment', label: 'Quy Định Thanh Toán' },
      { key: 'shipping', label: 'Quy Định Chuyển Hàng' },
      { key: 'sitemap', label: 'Bản Đồ' },
      { key: 'term-condition', label: 'Điều Kiện Và Thoả Thuận' },
      { key: 'wishlist', label: 'Mục Yêu Thích' },
    ],
  },
  {
    key: 'blogs',
    label: 'Bài Viết',
    link: Paths.Home,
  },
];
