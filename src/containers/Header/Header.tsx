import React from 'react';

import HeaderTop from 'containers/Header/HeaderTop';
import HeaderMain from 'containers/Header/HeaderMain';
import { HeaderMenu } from 'containers/Header/HeaderMenu';

import './Header.scss';

export const Header: React.FC = () => {
  return (
    <div className="Header">
      <HeaderTop />
      <HeaderMain />
      <HeaderMenu />
    </div>
  );
};

export default Header;
