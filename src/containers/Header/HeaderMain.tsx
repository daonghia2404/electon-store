import React, { useState } from 'react';
import { Link, navigate } from '@reach/router';
import { useDispatch, useSelector } from 'react-redux';

import Logo from 'assets/images/logo.png';
import Icon, { EIconColor, EIconName } from 'components/Icon';
import { Form } from 'antd';
import Input from 'components/Input';
import Button from 'components/Button';
import DropdownCustom from 'components/DropdownCustom';
import { Paths } from 'pages/routers';
import { TRootState } from 'redux/reducers';
import { formatNumberWithCommas, getUrlImage, renderTextColor } from 'utils/function';
import { TCartProduct } from 'common/types';
import { uiAction } from 'redux/actions';

export const HeaderMain: React.FC = () => {
  const dispatch = useDispatch();
  const cartState = useSelector((state: TRootState) => state.ui.cart);
  const isEmptyCart = cartState.length === 0;

  const [visibleCart, setVisibleCart] = useState<boolean>(false);

  const handleToggleVisibleCartDropwdown = (currentVisible: boolean): void => {
    setVisibleCart(currentVisible);
  };

  const calculateTotalPrice = (): number => {
    return cartState
      .map((product) => calculateDiscountPrice(product.price, product.sale) * product.quantity)
      .reduce((a, b) => a + b, 0);
  };

  const calculateDiscountPrice = (price: number, sale: number): number => {
    return price - price * (sale / 100);
  };

  const handleRemoveProductCart = (product: TCartProduct & { cartProductId: string }): void => {
    const deletedCart = cartState.filter((item) => item.cartProductId !== product.cartProductId);
    dispatch(uiAction.setCart(deletedCart));
  };

  const handleCheckout = (): void => {
    dispatch(uiAction.setCheckout(cartState));
    navigate(Paths.Checkout);
  };

  const renderCartOverlay = (): React.ReactElement => {
    return (
      <div className="HeaderMain-cart-overlay">
        <div className="HeaderMain-cart-overlay-header">
          {isEmptyCart ? (
            'Không Có Sản Phẩm Trong Giỏ Hàng'
          ) : (
            <>
              Tổng cộng tất cả <span className="primary-color">{cartState.length}</span> sản phẩm
            </>
          )}
        </div>
        {isEmptyCart ? (
          <div className="HeaderMain-cart-empty flex flex-col justify-center items-center">
            <Icon name={EIconName.shoppingCart} color={EIconColor.doveGray} />
            <Button
              className="secondary"
              title="Tiếp Tục Mua Sắm"
              link={`${Paths.Categories('id')}`}
              onClick={(): void => handleToggleVisibleCartDropwdown(false)}
            />
          </div>
        ) : (
          <>
            <div className="HeaderMain-cart-products">
              {cartState.map((product) => (
                <div className="HeaderMain-cart-product-item">
                  <div className="HeaderMain-cart-product-item-image">
                    <Link to={Paths.Product(product._id)}>
                      <img src={getUrlImage(product.image)} alt="" />
                    </Link>
                  </div>
                  <div className="HeaderMain-cart-product-item-info">
                    <Link to={Paths.Product(product._id)} className="HeaderMain-cart-product-item-title">
                      {product.title} ({product.config_id} / {renderTextColor(product.color_id)})
                    </Link>
                    <div className="HeaderMain-cart-product-item-quantity">{product.quantity}x</div>
                    <span className="HeaderMain-cart-product-item-price primary-color">
                      {formatNumberWithCommas(calculateDiscountPrice(product.price, product.sale))}đ
                    </span>
                    {Boolean(product.sale) && (
                      <del className="HeaderMain-cart-product-item-old-price">
                        {formatNumberWithCommas(product.price)}đ
                      </del>
                    )}
                  </div>
                  <div
                    className="HeaderMain-cart-product-item-remove flex items-center justify-center"
                    onClick={(): void => handleRemoveProductCart(product)}
                  >
                    <Icon name={EIconName.trash} color={EIconColor.sangria} />
                  </div>
                </div>
              ))}
            </div>

            <div className="HeaderMain-cart-overlay-total flex justify-between">
              <span>Tổng giá: </span>
              <span className="primary-color">{formatNumberWithCommas(calculateTotalPrice())}đ</span>
            </div>
            <div className="HeaderMain-cart-overlay-footer flex justify-between">
              <Button
                link={Paths.Cart}
                title="Giỏ Hàng"
                className="secondary"
                onClick={(): void => handleToggleVisibleCartDropwdown(false)}
              />
              <Button title="Thanh Toán" type="primary" onClick={handleCheckout} />
            </div>
          </>
        )}
      </div>
    );
  };

  return (
    <div className="HeaderMain">
      <div className="container">
        <div className="HeaderMain-wrapper flex items-center justify-between">
          <Link to={Paths.Home} className="HeaderMain-logo">
            <img src={Logo} alt="" />
          </Link>
          <Form className="HeaderMain-search flex">
            <Form.Item className="HeaderMain-search-input" name="keyword">
              <Input placeholder="Tìm Kiếm Danh Mục" />
            </Form.Item>
            <Form.Item>
              <Button title="TÌM KIẾM" type="primary" />
            </Form.Item>
          </Form>
          <div className="HeaderMain-action flex items-center">
            <div className="HeaderMain-action-item favorite">
              <div className="HeaderMain-action-notify">0</div>
              <Icon name={EIconName.heart} />
            </div>
            <DropdownCustom
              visible={visibleCart}
              onVisibleChange={handleToggleVisibleCartDropwdown}
              overlay={renderCartOverlay()}
            >
              <div className="HeaderMain-action-item cart">
                <div className="HeaderMain-action-notify">{cartState.length}</div>
                <Icon name={EIconName.shoppingCart} />
              </div>
            </DropdownCustom>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderMain;
