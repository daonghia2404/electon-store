import React from 'react';

import DropdownList from 'components/DropdownList';
import Icon, { EIconColor, EIconName } from 'components/Icon';
import { optionsDropdownLang, optionsDropdownMyAccount } from 'containers/Header/Header.datas';

export const HeaderTop: React.FC = () => {
  return (
    <div className="HeaderTop">
      <div className="container">
        <div className="HeaderTop-wrapper flex justify-between">
          <div className="HeaderTop-col">Chào mừng đến với Electon-Store</div>
          <div className="HeaderTop-col flex">
            <div className="HeaderTop-dropdown">
              <DropdownList options={optionsDropdownMyAccount}>
                <div className="HeaderTop-dropdown-item flex items-center">
                  Tài Khoản
                  <Icon name={EIconName.angleDown} color={EIconColor.white} />
                </div>
              </DropdownList>
            </div>
            <div className="HeaderTop-dropdown">
              <DropdownList options={optionsDropdownLang}>
                <div className="HeaderTop-dropdown-item flex items-center">
                  Tiếng Việt
                  <Icon name={EIconName.angleDown} color={EIconColor.white} />
                </div>
              </DropdownList>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderTop;
