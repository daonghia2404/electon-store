import React from 'react';
import { Link } from '@reach/router';

import DropdownList from 'components/DropdownList';
import { dataHeaderMenu } from 'containers/Header/Header.datas';
import Icon, { EIconColor, EIconName } from 'components/Icon';
import DropdownCustom from 'components/DropdownCustom/DropdownCustom';
import { useSelector } from 'react-redux';
import { TRootState } from 'redux/reducers';
import { Paths } from 'pages/routers';
import { renderImageCategories } from 'utils/function';

export const HeaderMenu: React.FC = () => {
  const categoriesState = useSelector((state: TRootState) => state.categoriesState.categories);

  const renderOverlayCategories = (): React.ReactElement => {
    return (
      <div className="HeaderMenu-categories-overlay">
        {categoriesState.map((category) => (
          <Link
            to={Paths.Categories(category.title)}
            key={category._id}
            className="HeaderMenu-categories-dropdown-item flex items-center"
          >
            <div className="HeaderMenu-categories-dropdown-item-image">
              <img src={renderImageCategories(category.title)} alt="" />
            </div>
            <div className="HeaderMenu-categories-dropdown-item-title">{category.title}</div>
          </Link>
        ))}
      </div>
    );
  };

  return (
    <div className="HeaderMenu">
      <div className="container">
        <div className="HeaderMenu-wrapper flex">
          <div className="HeaderMenu-categories">
            <DropdownCustom overlay={renderOverlayCategories()}>
              <div className="HeaderMenu-categories-item flex items-center justify-between">
                DANH MỤC
                <Icon name={EIconName.bars} color={EIconColor.white} />
              </div>
            </DropdownCustom>
          </div>
          <div className="HeaderMenu-menu flex">
            {dataHeaderMenu.map((item) => {
              const isSubMenu = item.child;

              if (isSubMenu)
                return (
                  <DropdownList key={item.key} options={item.child}>
                    <div className="HeaderMenu-menu-item flex items-center">
                      {item.label}
                      <Icon name={EIconName.angleDown} color={EIconColor.white} />
                    </div>
                  </DropdownList>
                );
              return (
                <Link key={item.key} to={item.link} className="HeaderMenu-menu-item flex items-center">
                  {item.label}
                </Link>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};
