import { TSelectOption } from 'common/types';
import { TColorsOption } from 'services/colors/types';

export type TSelectColorProps = {
  className?: string;
  value: TSelectOption | null;
  onChange?: (data: TSelectOption) => void;
  data: TColorsOption[];
};
