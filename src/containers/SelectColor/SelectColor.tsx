import React from 'react';
import classNames from 'classnames';

import { TSelectColorProps } from 'containers/SelectColor/SelectColor.types';
import { TColorsOption } from 'services/colors/types';
import CircleColor from 'components/CircleColor';
import { EProductColor } from 'common/enums';

import './SelectColor.scss';

export const SelectColor: React.FC<TSelectColorProps> = ({ className, data, value, onChange }) => {
  const handleClickColorOption = (currentValue: TColorsOption): void => {
    onChange?.({ label: currentValue.name, value: currentValue._id });
  };

  return (
    <div className={classNames('SelectColor', className)}>
      <div className="SelectColor-wrapper flex items-center flex-wrap">
        <div className="SelectColor-label">Màu Sắc: </div>
        {data.map((item) => (
          <CircleColor
            active={value?.label === item.name}
            color={item.name as EProductColor}
            onClick={(): void => handleClickColorOption(item)}
          />
        ))}
      </div>
    </div>
  );
};

export default SelectColor;
