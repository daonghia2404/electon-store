import { Paths } from 'pages/routers';

export const dataFooterMenuInfomation = [
  { key: 'about', link: Paths.Home, label: 'Về Chúng Tôi' },
  { key: 'contact', link: Paths.Home, label: 'Liên Hệ' },
  { key: 'faq', link: Paths.Home, label: 'FAQ' },
  { key: 'cart', link: Paths.Home, label: 'Giỏ Hàng Của Tôi' },
  { key: 'wishlist', link: Paths.Home, label: 'Mục Yêu Thích' },
];

export const dataFooterMenuPolicy = [
  { key: 'payment', link: Paths.Home, label: 'Chính Sách Thanh Toán' },
  { key: 'privacy', link: Paths.Home, label: 'Chính Sách Bảo Mật' },
  { key: 'return', link: Paths.Home, label: 'Chính Sách Hoàn Trả' },
  { key: 'shipping', link: Paths.Home, label: 'Chính Sách Giao Hàng' },
  { key: 'term-condition', link: Paths.Home, label: 'Điều Kiện Và Thoả Thuận' },
];

export const dataFooterMenuProducts = [
  { key: 'new', link: Paths.Home, label: 'Sản Phẩm Mới' },
  { key: 'feature', link: Paths.Home, label: 'Sản Phẩm Nổi Bật' },
  { key: 'best', link: Paths.Home, label: 'Sản Phẩm Bán Chạy' },
];
