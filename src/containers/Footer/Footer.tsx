/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Link } from '@reach/router';

import Logo from 'assets/images/logo-light.png';
import PaymentAmericanExpress from 'assets/images/card-payment/image-card-american-express.png';
import PaymentDiscover from 'assets/images/card-payment/image-card-discover.png';
import PaymentMasterCard from 'assets/images/card-payment/image-card-master-card.png';
import PaymentPaypal from 'assets/images/card-payment/image-card-paypal.png';
import PaymentVisa from 'assets/images/card-payment/image-card-visa.png';

import { dataFooterMenuInfomation, dataFooterMenuPolicy, dataFooterMenuProducts } from 'containers/Footer/Footer.datas';
import Icon, { EIconColor, EIconName } from 'components/Icon';

import './Footer.scss';

export const Footer: React.FC = () => {
  return (
    <div className="Footer">
      <div className="container">
        <div className="Footer-wrapper">
          <div className="Footer-top flex items-start justify-between">
            <div className="Footer-item">
              <h4 className="Footer-logo">
                <img src={Logo} alt="" />
              </h4>
              <p className="Footer-text">
                Electon Store - Shop bán đồ Công nghệ hàng đầu Việt Nam <br /> Phân phối chính hãng sản phẩm của Apple -
                Giao hàng toàn quốc - Lựa chọn tốt nhất của bạn
              </p>
              <div className="Footer-social flex">
                <a href="#" target="_blank" className="Footer-social-item facebook">
                  <Icon name={EIconName.facebook} color={EIconColor.mineShaft} />
                </a>
                <a href="#" target="_blank" className="Footer-social-item twitter">
                  <Icon name={EIconName.twitter} color={EIconColor.mineShaft} />
                </a>
                <a href="#" target="_blank" className="Footer-social-item google">
                  <Icon name={EIconName.google} color={EIconColor.mineShaft} />
                </a>
                <a href="#" target="_blank" className="Footer-social-item youtube">
                  <Icon name={EIconName.youtube} color={EIconColor.mineShaft} />
                </a>
                <a href="#" target="_blank" className="Footer-social-item instagram">
                  <Icon name={EIconName.instagram} color={EIconColor.mineShaft} />
                </a>
              </div>
            </div>

            <div className="Footer-item">
              <h4 className="Footer-title">THÔNG TIN</h4>
              <ul className="Footer-list">
                {dataFooterMenuInfomation.map((item) => (
                  <li key={item.key} className="Footer-list-item">
                    <Link className="Footer-text" to={item.link}>
                      {item.label}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>

            <div className="Footer-item">
              <h4 className="Footer-title">CHÍNH SÁCH</h4>
              <ul className="Footer-list">
                {dataFooterMenuPolicy.map((item) => (
                  <li key={item.key} className="Footer-list-item">
                    <Link className="Footer-text" to={item.link}>
                      {item.label}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>

            <div className="Footer-item">
              <h4 className="Footer-title">SẢN PHẨM</h4>
              <ul className="Footer-list">
                {dataFooterMenuProducts.map((item) => (
                  <li key={item.key} className="Footer-list-item">
                    <Link className="Footer-text" to={item.link}>
                      {item.label}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>

            <div className="Footer-item">
              <h4 className="Footer-title">LIÊN HỆ</h4>
              <ul className="Footer-list">
                <li className="Footer-list-item flex items-center">
                  <Icon name={EIconName.mapMarker} color={EIconColor.doveGray} />
                  <p className="Footer-text">Hà Nội, Việt Nam</p>
                </li>
                <li className="Footer-list-item flex items-center">
                  <Icon name={EIconName.mobile} color={EIconColor.doveGray} />
                  <p className="Footer-text">(+84) 123 456 789</p>
                </li>
                <li className="Footer-list-item flex items-center">
                  <Icon name={EIconName.mail} color={EIconColor.doveGray} />
                  <p className="Footer-text">electon.store@demo.com</p>
                </li>
              </ul>
            </div>
          </div>

          <div className="Footer-bottom flex items-center justify-between">
            <p className="Footer-text">
              Bản Quyền &copy 2021 thuộc về Electon Store <sup>TM</sup>
            </p>
            <div className="Footer-payment-cards flex">
              <div className="Footer-payment-cards-item">
                <img src={PaymentAmericanExpress} alt="" />
              </div>
              <div className="Footer-payment-cards-item">
                <img src={PaymentDiscover} alt="" />
              </div>

              <div className="Footer-payment-cards-item">
                <img src={PaymentMasterCard} alt="" />
              </div>

              <div className="Footer-payment-cards-item">
                <img src={PaymentPaypal} alt="" />
              </div>

              <div className="Footer-payment-cards-item">
                <img src={PaymentVisa} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
