export type TModalConfirmProps = {
  visible: boolean;
  className?: string;
  title: string;
  description: string;
  titleConfirm?: string;
  titleCancel?: string;
  loading?: boolean;
  onClose?: () => void;
  onCancel?: () => void;
  onConfirm?: () => void;
};
