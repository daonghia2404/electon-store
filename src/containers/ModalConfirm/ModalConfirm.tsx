import React from 'react';
import classNames from 'classnames';

import Button from 'components/Button';
import Modal from 'components/Modal';
import { TModalConfirmProps } from 'containers/ModalConfirm/ModalConfirm.types';

import './ModalConfirm.scss';

export const ModalConfirm: React.FC<TModalConfirmProps> = ({
  className,
  title,
  description,
  visible,
  titleConfirm = 'Xác Nhận',
  titleCancel = 'Huỷ Bỏ',
  loading,
  onClose,
  onCancel,
  onConfirm,
}) => {
  const handleCancel = (): void => {
    onCancel?.();
    onClose?.();
  };

  const handleConfirm = (): void => {
    onConfirm?.();
  };

  return (
    <div className={classNames('ModalConfirm', className)}>
      <Modal width={420} visible={visible} onClose={onClose}>
        <div className="ModalConfirm-title">{title}</div>
        <div className="ModalConfirm-description">{description}</div>
        <div className="ModalConfirm-actions flex justify-center">
          <Button title={titleConfirm} loading={loading} type="primary" onClick={handleConfirm} />
          <Button title={titleCancel} className="secondary" onClick={handleCancel} />
        </div>
      </Modal>
    </div>
  );
};

export default ModalConfirm;
