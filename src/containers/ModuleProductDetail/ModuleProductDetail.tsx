import React, { useEffect } from 'react';
import { Form } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { useDispatch, useSelector } from 'react-redux';

import {
  TModuleProductDetailForm,
  TModuleProductDetailProps,
} from 'containers/ModuleProductDetail/ModuleProductDetail.types';
import Input from 'components/Input';
import { getUrlImage, validationRules } from 'utils/function';
import InputNumber from 'components/InputNumber';
import TextArea from 'components/TextArea';
import Select from 'components/Select';
import MultipleSelect from 'components/MultipleSelect';
import UploadSingleImage from 'components/UploadSingleImage';
import Button from 'components/Button';
import { LayoutPaths, Paths } from 'pages/routers';
import { ETypeModuleProductDetail } from 'containers/ModuleProductDetail/ModuleProductDetail.enums';
import { TRootState } from 'redux/reducers';
import { getAllColorsAction, getAllConfigAction, getOneProductAction } from 'redux/actions';
import { EConfigAction } from 'redux/actions/config/constants';
import Loading from 'components/Loading';
import { getAllCategoriessAction } from 'redux/actions/category';
import { ECategoriesAction } from 'redux/actions/category/constants';
import { dataTextColor } from 'common/static';

import './ModuleProductDetail.scss';

export const ModuleProductDetail: React.FC<TModuleProductDetailProps> = ({ id, type, submitLoading, onSubmit }) => {
  const [form] = useForm();
  const dispatch = useDispatch();

  const isCreateProduct = type === ETypeModuleProductDetail.create;
  const configOptionsState = useSelector((state: TRootState) => state.configState.config);
  const colorOptionsState = useSelector((state: TRootState) => state.colorsState.colors);
  const categoriesOptionsState = useSelector((state: TRootState) => state.categoriesState.categories);
  const productState = useSelector((state: TRootState) => state.productsState.product);

  const configLoading = useSelector((state: TRootState) => state.loading[EConfigAction.GET_ALL_CONFIG]);
  const colorsLoading = useSelector((state: TRootState) => state.loading[EConfigAction.GET_ALL_CONFIG]);
  const categoriesLoading = useSelector((state: TRootState) => state.loading[ECategoriesAction.GET_ALL_CATEGORY]);

  const loading = configLoading || colorsLoading || categoriesLoading;

  const getColorsOptions = (): void => {
    dispatch(getAllColorsAction.request());
  };

  const getConfigOptions = (): void => {
    dispatch(getAllConfigAction.request());
  };

  const getCategoriesOptions = (): void => {
    dispatch(getAllCategoriessAction.request());
  };

  const handleSubmit = (values: TModuleProductDetailForm): void => {
    onSubmit?.(values);
  };

  const getProductDetail = (): void => {
    if (id) dispatch(getOneProductAction.request(id));
  };

  useEffect(() => {
    if (!isCreateProduct) {
      const { color_id, config_id, title, description, price, sale, cat_id, image, content, stock } = productState;

      form.setFieldsValue({
        title,
        description,
        price,
        sale,
        color: color_id?.map((item) => ({ value: item._id, label: dataTextColor[item.name] })),
        config: config_id?.map((item) => ({ value: item._id, label: item.name })),
        category: { value: cat_id?._id, label: cat_id?.title },
        stock,
        content,
        image: getUrlImage(image),
      });
    } else {
      form.setFieldsValue({
        stock: 0,
        sale: 0,
        price: 0,
      });
    }
  }, [form, productState, isCreateProduct]);

  useEffect(() => {
    getConfigOptions();
    getColorsOptions();
    getCategoriesOptions();

    if (!isCreateProduct) {
      getProductDetail();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="ModuleProductDetail">
      {loading ? (
        <div className="ModuleProductDetail-loading flex items-center justify-center">
          <Loading />
        </div>
      ) : (
        <Form form={form} layout="vertical" className="ModuleProductDetail-main flex flex-wrap" onFinish={handleSubmit}>
          <div className="ModuleProductDetail-main-col flex flex-wrap justify-between">
            <Form.Item
              className="ModuleProductDetail-form-item"
              name="title"
              label="Tên Sản Phẩm"
              rules={[validationRules.required()]}
            >
              <Input placeholder="Nhập Tên Sản Phẩm" />
            </Form.Item>
            <Form.Item
              className="ModuleProductDetail-form-item"
              name="description"
              label="Mô Tả Sản Phẩm"
              rules={[validationRules.required()]}
            >
              <TextArea placeholder="Nhập Mô Tả Sản Phẩm" />
            </Form.Item>
            <Form.Item className="ModuleProductDetail-form-item half" name="price" label="Giá Sản Phẩm">
              <InputNumber min={0} />
            </Form.Item>
            <Form.Item className="ModuleProductDetail-form-item half" name="sale" label="Giảm Giá">
              <InputNumber min={0} max={100} />
            </Form.Item>
            <Form.Item
              className="ModuleProductDetail-form-item half"
              name="color"
              label="Màu Sắc Sản Phẩm"
              rules={[validationRules.required()]}
            >
              <MultipleSelect
                placeholder="Chọn Màu Sắc"
                options={colorOptionsState.map((item) => ({ label: dataTextColor[item.name], value: item._id }))}
              />
            </Form.Item>
            <Form.Item
              className="ModuleProductDetail-form-item half"
              name="config"
              label="Cấu Hình Sản Phẩm"
              rules={[validationRules.required()]}
            >
              <MultipleSelect
                options={configOptionsState.map((item) => ({ label: item.name, value: item._id }))}
                placeholder="Chọn Cấu Hình"
              />
            </Form.Item>
            <Form.Item
              className="ModuleProductDetail-form-item"
              name="category"
              label="Danh Mục"
              rules={[validationRules.required()]}
            >
              <Select
                placeholder="Chọn Danh Mục"
                options={categoriesOptionsState.map((item) => ({ label: item.title, value: item._id }))}
              />
            </Form.Item>
            <Form.Item
              className="ModuleProductDetail-form-item half"
              name="stock"
              label="Tồn Kho"
              rules={[validationRules.required()]}
            >
              <InputNumber min={0} />
            </Form.Item>
            <Form.Item
              className="ModuleProductDetail-form-item"
              name="content"
              label="Bài Viết Về Sản Phẩm"
              rules={[validationRules.required()]}
            >
              <TextArea placeholder="Nhập Nội Dung Bài Viết" />
            </Form.Item>
          </div>
          <div className="ModuleProductDetail-main-col">
            <Form.Item
              className="ModuleProductDetail-form-item"
              name="image"
              label="Ảnh Sản Phẩm"
              rules={[validationRules.required()]}
            >
              <UploadSingleImage aspect={300 / 350} />
            </Form.Item>
          </div>

          <div className="ModuleProductDetail-main-col">
            <div className="ModuleProductDetail-submit flex justify-end">
              <Button
                size="large"
                title="Huỷ Bỏ"
                className="secondary"
                link={`${LayoutPaths.Admin}${Paths.ProductsManagement}`}
              />
              <Button size="large" title="Xác Nhận" type="primary" htmlType="submit" loading={submitLoading} />
            </div>
          </div>
        </Form>
      )}
    </div>
  );
};

export default ModuleProductDetail;
