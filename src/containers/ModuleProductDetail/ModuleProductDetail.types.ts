import { TSelectOption } from 'common/types';
import { ETypeModuleProductDetail } from 'containers/ModuleProductDetail/ModuleProductDetail.enums';

export type TModuleProductDetailProps = {
  id?: string;
  className?: string;
  type: ETypeModuleProductDetail;
  submitLoading?: boolean;
  onSubmit?: (data: TModuleProductDetailForm) => void;
};

export type TModuleProductDetailForm = {
  title: string;
  description: string;
  price: number;
  sale: number;
  color: TSelectOption[];
  config: TSelectOption[];
  category: TSelectOption;
  stock: number;
  content: string;
  image: string;
};
