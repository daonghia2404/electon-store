import React from 'react';
import { Form } from 'antd';

import ImageSubscribe from 'assets/images/image-subscribe.jpeg';
import Input from 'components/Input';
import Button from 'components/Button';

import './Subscribe.scss';

export const Subscribe: React.FC = () => {
  return (
    <div className="Subscribe flex">
      <div className="Subscribe-column">
        <h4 className="Subscribe-title">Đăng Ký Để Nhận Bản tin Của Chúng Tôi</h4>
        <p className="Subscribe-des">Được giảm giá 25% cho lần mua đầu tiên!</p>
        <Form className="Subscribe-form flex">
          <Form.Item name="mail">
            <Input placeholder="Nhập mail của bạn..." />
          </Form.Item>
          <Form.Item>
            <Button title="ĐĂNG KÝ" type="primary" />
          </Form.Item>
        </Form>
      </div>
      <div className="Subscribe-column">
        <img src={ImageSubscribe} alt="" />
      </div>
    </div>
  );
};

export default Subscribe;
