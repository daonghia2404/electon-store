import ImageBanner1 from 'assets/images/banner/image-banner-1.png';
import ImageBanner2 from 'assets/images/banner/image-banner-2.png';
import { Paths } from 'pages/routers';

export const dataBanner = [
  {
    image: ImageBanner1,
    subtitle: 'Sản Phẩm Mới',
    title: 'iPhone 13 Pro Max',
    des: 'Hệ thống Camera mạnh mẽ. Màn hình hiển thị sắc nét. Chip điện thoại thông minh nhất thế giới',
    link: Paths.Product('5'),
  },
  {
    image: ImageBanner2,
    subtitle: 'Sản Phẩm Bán Chạy',
    title: 'Macbook Pro 16 Inch',
    des: 'Màn hình Retina đắm chìm. Bộ vi xử lý siêu nhanh. Đồ hoạ tiên tiến. Dung lượng pin lớn nhất.',
    link: Paths.Product('7'),
  },
];
