import React from 'react';
import classNames from 'classnames';
import { Link } from '@reach/router';

import Carousels from 'components/Carousels';
import Button from 'components/Button';
import { dataBanner } from 'containers/Banner/Banner.datas';

import './Banner.scss';

export const Banner: React.FC = () => {
  return (
    <div className="Banner">
      <Carousels dots={false} onHoverShowArrow autoplay>
        {dataBanner.map((item, index) => (
          <div className={classNames('Banner-item', { 'content-right': index % 2 === 0 })}>
            <div className="container">
              <div className="Banner-item-wrapper flex items-center justify-between">
                <div className="Banner-content flex flex-col">
                  <h3 className="Banner-subtitle">{item.subtitle}</h3>
                  <div className="Banner-title">{item.title}</div>
                  <p className="Banner-des">{item.des}</p>
                  <Link to={item.link} className="Banner-btns flex">
                    <Button className="Banner-button" title="MUA NGAY" type="primary" size="large" />
                  </Link>
                </div>

                <div className="Banner-image">
                  <img src={item.image} alt="" />
                </div>
              </div>
            </div>
          </div>
        ))}
      </Carousels>
    </div>
  );
};

export default Banner;
