import React, { useState } from 'react';
import classNames from 'classnames';
import { v4 as uuidv4 } from 'uuid';

import Modal from 'components/Modal';
import { TModalPreviewProductProps } from 'containers/ModalPreviewProduct/ModalPreviewProduct.types';
import Button from 'components/Button';
import InputNumber from 'components/InputNumber';
import { EIconColor, EIconName } from 'components/Icon';
import { formatNumberWithCommas, getUrlImage, showNotification } from 'utils/function';
import SelectConfig from 'containers/SelectConfig';
import SelectColor from 'containers/SelectColor';
import { TSelectOption } from 'common/types';
import { useDispatch, useSelector } from 'react-redux';
import { TRootState } from 'redux/reducers';
import { uiAction } from 'redux/actions';
import { ETypeNotification } from 'common/enums';
import { navigate } from '@reach/router';
import { Paths } from 'pages/routers';

import './ModalPreviewProduct.scss';

export const ModalPreviewProduct: React.FC<TModalPreviewProductProps> = ({
  className,
  title,
  visible,
  data,
  onClose,
}) => {
  const dispatch = useDispatch();
  const cartState = useSelector((state: TRootState) => state.ui.cart);
  const isDiscount = data.sale > 0;
  const newPrice = data.price - data.price * (data.sale / 100);

  const [config, setConfig] = useState<TSelectOption | null>(null);
  const [color, setColor] = useState<TSelectOption | null>(null);
  const [quantity, setQuantity] = useState<number>(1);

  const handleChangeConfig = (value: TSelectOption): void => {
    setConfig(value);
  };

  const handleChangeColor = (value: TSelectOption): void => {
    setColor(value);
  };

  const handleChangeQuantity = (value: number): void => {
    setQuantity(value);
  };

  const resetField = (): void => {
    setConfig(null);
    setColor(null);
    setQuantity(1);
  };

  const handleAddCart = (): void => {
    const isValidatedSuccess = config && color;
    if (isValidatedSuccess) {
      const product = {
        ...data,
        cartProductId: uuidv4(),
        quantity,
        color_id: color?.label,
        config_id: config?.label,
      };

      dispatch(uiAction.setCart([...cartState, product]));
      showNotification(ETypeNotification.success, 'Thêm Vào Giỏ Hàng Thành Công');
      handleCloseModalPreview();
    } else {
      showNotification(ETypeNotification.error, 'Vui Lòng Chọn Cấu Hình Và Màu Của Sản Phẩm');
    }
  };

  const handleCheckout = (): void => {
    const isValidatedSuccess = config && color;
    if (isValidatedSuccess) {
      const product = {
        ...data,
        cartProductId: uuidv4(),
        quantity,
        color_id: color?.label,
        config_id: config?.label,
      };
      dispatch(uiAction.setCheckout([product]));
      navigate(Paths.Checkout);
    } else {
      showNotification(ETypeNotification.error, 'Vui Lòng Chọn Cấu Hình Và Màu Của Sản Phẩm');
    }
  };

  const handleCloseModalPreview = (): void => {
    resetField();
    onClose?.();
  };

  return (
    <div className={classNames('ModalPreviewProduct', className)}>
      <Modal width={650} visible={visible} title={title} onClose={handleCloseModalPreview}>
        <div className="ModalPreviewProduct-main flex items-start">
          <div className="ModalPreviewProduct-image">
            <img src={getUrlImage(data.image)} alt="" />
          </div>
          <div className="ModalPreviewProduct-info">
            <h3 className="ModalPreviewProduct-info-title flex items-center">
              {data.title}
              {isDiscount && <div className="ModalPreviewProduct-info-title-sale style-tag">-{data.sale}%</div>}
            </h3>
            <div className="ModalPreviewProduct-info-price flex items-center">
              {isDiscount ? (
                <>
                  <span className="ModalPreviewProduct-info-price-current">{formatNumberWithCommas(newPrice)}đ</span>
                  <del className="ModalPreviewProduct-info-price-old">{formatNumberWithCommas(data.price)}đ</del>
                </>
              ) : (
                <span className="ModalPreviewProduct-info-price-current">{formatNumberWithCommas(data.price)}đ</span>
              )}
            </div>

            <p className="ModalPreviewProduct-info-description">{data.description}</p>

            <div className="ModalPreviewProduct-info-options">
              <SelectConfig data={data.config_id} value={config} onChange={handleChangeConfig} />

              <SelectColor data={data.color_id} value={color} onChange={handleChangeColor} />

              <div className="ModalPreviewProduct-info-options-config flex items-center flex-wrap">
                <div className="ModalPreviewProduct-info-options-config-label">Số Lượng: </div>
                <InputNumber min={1} max={20} value={quantity} onChange={handleChangeQuantity} />
              </div>

              <div className="ModalPreviewProduct-info-options-btns">
                <Button
                  className="secondary"
                  title="Thêm Vào Giỏ Hàng"
                  iconName={EIconName.shoppingCart}
                  iconColor={EIconColor.white}
                  reverse
                  onClick={handleAddCart}
                />
                <Button
                  className="secondary"
                  title="Thêm Vào Mục Yêu Thích"
                  iconName={EIconName.heart}
                  iconColor={EIconColor.white}
                  reverse
                />
                <Button title="Mua Ngay" type="primary" onClick={handleCheckout} />
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default ModalPreviewProduct;
