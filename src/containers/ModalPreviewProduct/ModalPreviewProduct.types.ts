import { TModalProps } from 'components/Modal';

export type TModalPreviewProductProps = TModalProps & {
  data?: any;
};
