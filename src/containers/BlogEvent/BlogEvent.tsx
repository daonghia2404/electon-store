import React from 'react';

import Section from 'components/Section';
import Carousels from 'components/Carousels';
import BlogBox from 'components/BlogBox';
import { dataBlogs } from 'common/static';

import './BlogEvent.scss';

export const BlogEvent: React.FC = () => {
  return (
    <div className="BlogEvent">
      <Section title="BÀI VIẾT & SỰ KIỆN">
        <Carousels slidesToShow={3} dots={false} infinite={false}>
          {[...dataBlogs, ...dataBlogs].map((blog) => (
            <div className="BlogEvent-item">
              <BlogBox {...blog} style={{ margin: 10 }} />
            </div>
          ))}
        </Carousels>
      </Section>
    </div>
  );
};

export default BlogEvent;
