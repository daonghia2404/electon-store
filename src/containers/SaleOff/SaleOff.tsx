import React from 'react';

import BoxShopNow from 'components/BoxShopNow';
import Section from 'components/Section';
import ImageSaleOff1 from 'assets/images/sale-off/image-sale-off-1.png';
import ImageSaleOff2 from 'assets/images/sale-off/image-sale-off-2.png';

import './SaleOff.scss';

export const SaleOff: React.FC = () => {
  return (
    <div className="SaleOff">
      <Section>
        <div className="SaleOff-wrapper flex justify-between">
          <div className="SaleOff-item">
            <BoxShopNow image={ImageSaleOff1} title="iPhone 12 Series" saleOff="Giảm 20%" />
          </div>
          <div className="SaleOff-item">
            <BoxShopNow image={ImageSaleOff2} title="Airpod Pro True Wireless" saleOff="Giảm 50%" />
          </div>
        </div>
      </Section>
    </div>
  );
};

export default SaleOff;
