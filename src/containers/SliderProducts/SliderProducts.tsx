import React from 'react';

import Section from 'components/Section';
import Carousels from 'components/Carousels';
import BoxProduct from 'components/BoxProduct';
import { TSliderProductsProps } from 'containers/SliderProducts/SliderProducts.types';

import './SliderProducts.scss';
import { useSelector } from 'react-redux';
import { TRootState } from 'redux/reducers';
import { Paths } from 'pages/routers';

export const SliderProducts: React.FC<TSliderProductsProps> = ({ title }) => {
  const productsState = useSelector((state: TRootState) => state.productsState.products);

  return (
    <div className="SliderProducts">
      <Section title={title}>
        <Carousels slidesToShow={4} dots={false} infinite={false}>
          {productsState.map((product) => (
            <div className="SliderProducts-item">
              <BoxProduct {...product} link={Paths.Product(product._id)} style={{ margin: 10 }} />
            </div>
          ))}
        </Carousels>
      </Section>
    </div>
  );
};

export default SliderProducts;
