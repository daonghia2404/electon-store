import { createActionCreator } from 'deox';
import { EUIAction } from './constants';
import { TResetActionStatus, TSetCartAction, TSetCheckoutAction } from './types';

export const uiAction = {
  resetActionStatus: createActionCreator(
    EUIAction.RESET_ACTION_STATUS,
    (resolve) =>
      (actionName: string): TResetActionStatus =>
        resolve({ actionName: actionName.replace('_REQUEST', '') }),
  ),
  setCart: createActionCreator(
    EUIAction.SET_CART,
    (resolve) =>
      (cart: Array<any>): TSetCartAction =>
        resolve({ cart }),
  ),
  setCheckout: createActionCreator(
    EUIAction.SET_CHECKOUT,
    (resolve) =>
      (checkout: Array<any>): TSetCheckoutAction =>
        resolve({ checkout }),
  ),
};

export default uiAction;
