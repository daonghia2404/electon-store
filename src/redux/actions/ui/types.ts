import { EUIAction } from 'redux/actions/ui/constants';

export type TSetDevice = { type: EUIAction.SET_DEVICE; payload: { deviceWidth: number } };
export type TResetActionStatus = { type: EUIAction.RESET_ACTION_STATUS };
export type TZippingStatus = { type: EUIAction.DOWNLOADING_STATUS; payload: { data: { [key: string]: boolean } } };
export type TSetCartAction = {
  type: EUIAction.SET_CART;
  payload: {
    cart: Array<any>;
  };
};
export type TSetCheckoutAction = {
  type: EUIAction.SET_CHECKOUT;
  payload: {
    checkout: Array<any>;
  };
};
