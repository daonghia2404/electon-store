import { loginAction } from './auth';
import { uiAction } from './ui';
import { getAllConfigAction } from './config';
import { getAllColorsAction } from './colors';
import {
  getAllProductAction,
  getOneProductAction,
  createProductAction,
  updateProductAction,
  deleteProductAction,
} from './products';
import {
  getAllOrderAction,
  getOneOrderAction,
  createOrderAction,
  updateOrderAction,
  deleteOrderAction,
} from './orders';
import { uploadAction } from './upload';

export {
  loginAction,
  uiAction,
  getAllConfigAction,
  getAllColorsAction,
  getAllProductAction,
  getOneProductAction,
  createProductAction,
  updateProductAction,
  deleteProductAction,
  getAllOrderAction,
  getOneOrderAction,
  createOrderAction,
  updateOrderAction,
  deleteOrderAction,
  uploadAction,
};
