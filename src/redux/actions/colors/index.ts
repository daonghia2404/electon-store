import { createActionCreator } from 'deox';
import { EColorsAction } from 'redux/actions/colors/constants';
import { TGetAllColorsFailed, TGetAllColorsRequest, TGetAllColorsSuccess } from 'redux/actions/colors/types';
import { TGetAllColorsResponse } from 'services/colors/types';

export const getAllColorsAction = {
  request: createActionCreator(
    EColorsAction.GET_ALL_COLORS_REQUEST,
    (resolve) =>
      (cb?: (data: TGetAllColorsResponse) => void): TGetAllColorsRequest =>
        resolve({ cb }),
  ),
  success: createActionCreator(
    EColorsAction.GET_ALL_COLORS_SUCCESS,
    (resolve) =>
      (data: TGetAllColorsResponse): TGetAllColorsSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EColorsAction.GET_ALL_COLORS_FAILED,
    (resolve) =>
      (error: unknown): TGetAllColorsFailed =>
        resolve({ error }),
  ),
};
