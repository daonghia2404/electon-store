import { TGetAllColorsResponse } from 'services/colors/types';
import { EColorsAction } from './constants';

export type TGetAllColorsRequest = {
  type: EColorsAction.GET_ALL_COLORS_REQUEST;
  payload: {
    cb?: (data: TGetAllColorsResponse) => void;
  };
};

export type TGetAllColorsSuccess = {
  type: EColorsAction.GET_ALL_COLORS_SUCCESS;
  payload: {
    data: TGetAllColorsResponse;
  };
};

export type TGetAllColorsFailed = { type: EColorsAction.GET_ALL_COLORS_FAILED };
