import { createActionCreator } from 'deox';
import { EUploadAction } from 'redux/actions/upload/constants';
import { TUploadFailed, TUploadRequest, TUploadSuccess } from 'redux/actions/upload/types';
import { TUploadResponse } from 'services/upload/types';

export const uploadAction = {
  request: createActionCreator(
    EUploadAction.UPLOAD_FILE_REQUEST,
    (resolve) =>
      (body, cb?: (data: TUploadResponse) => void): TUploadRequest =>
        resolve({ body, cb }),
  ),
  success: createActionCreator(
    EUploadAction.UPLOAD_FILE_SUCCESS,
    (resolve) =>
      (data: TUploadResponse): TUploadSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EUploadAction.UPLOAD_FILE_FAILED,
    (resolve) =>
      (error: unknown): TUploadFailed =>
        resolve({ error }),
  ),
};
