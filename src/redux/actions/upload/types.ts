import { TUploadResponse } from 'services/upload/types';
import { EUploadAction } from './constants';

export type TUploadRequest = {
  type: EUploadAction.UPLOAD_FILE_REQUEST;
  payload: {
    body: FormData;
    cb?: (data: TUploadResponse) => void;
  };
};

export type TUploadSuccess = {
  type: EUploadAction.UPLOAD_FILE_SUCCESS;
  payload: {
    data: TUploadResponse;
  };
};

export type TUploadFailed = { type: EUploadAction.UPLOAD_FILE_FAILED };
