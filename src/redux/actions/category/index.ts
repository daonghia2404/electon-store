import { createActionCreator } from 'deox';
import { ECategoriesAction } from 'redux/actions/category/constants';
import {
  TGetAllCategoriesFailed,
  TGetAllCategoriesRequest,
  TGetAllCategoriesSuccess,
} from 'redux/actions/category/types';
import { TGetAllCategoriesResponse } from 'services/category/types';

export const getAllCategoriessAction = {
  request: createActionCreator(
    ECategoriesAction.GET_ALL_CATEGORY_REQUEST,
    (resolve) =>
      (cb?: (data: TGetAllCategoriesResponse) => void): TGetAllCategoriesRequest =>
        resolve({ cb }),
  ),
  success: createActionCreator(
    ECategoriesAction.GET_ALL_CATEGORY_SUCCESS,
    (resolve) =>
      (data: TGetAllCategoriesResponse): TGetAllCategoriesSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    ECategoriesAction.GET_ALL_CATEGORY_FAILED,
    (resolve) =>
      (error: unknown): TGetAllCategoriesFailed =>
        resolve({ error }),
  ),
};
