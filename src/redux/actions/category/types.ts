import { TGetAllCategoriesResponse } from 'services/category/types';
import { ECategoriesAction } from './constants';

export type TGetAllCategoriesRequest = {
  type: ECategoriesAction.GET_ALL_CATEGORY_REQUEST;
  payload: {
    cb?: (data: TGetAllCategoriesResponse) => void;
  };
};

export type TGetAllCategoriesSuccess = {
  type: ECategoriesAction.GET_ALL_CATEGORY_SUCCESS;
  payload: {
    data: TGetAllCategoriesResponse;
  };
};

export type TGetAllCategoriesFailed = { type: ECategoriesAction.GET_ALL_CATEGORY_FAILED };
