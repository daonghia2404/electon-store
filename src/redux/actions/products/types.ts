import { TCreateProductBody, TGetAllProductsResponse, TGetOneProductsResponse } from 'services/products/types';
import { EProductAction } from './constants';

export type TGetAllProductRequest = {
  type: EProductAction.GET_ALL_PRODUCT_REQUEST;
  payload: {
    cb?: (data: TGetAllProductsResponse) => void;
  };
};

export type TGetAllProductSuccess = {
  type: EProductAction.GET_ALL_PRODUCT_SUCCESS;
  payload: {
    data: TGetAllProductsResponse;
  };
};

export type TGetAllProductFailed = { type: EProductAction.GET_ALL_PRODUCT_FAILED };

export type TGetOneProductRequest = {
  type: EProductAction.GET_ONE_PRODUCT_REQUEST;
  payload: {
    id: string;
    cb?: (data: TGetOneProductsResponse) => void;
  };
};

export type TGetOneProductSuccess = {
  type: EProductAction.GET_ONE_PRODUCT_SUCCESS;
  payload: {
    data: TGetOneProductsResponse;
  };
};

export type TGetOneProductFailed = { type: EProductAction.GET_ONE_PRODUCT_FAILED };

export type TCreateProductRequest = {
  type: EProductAction.CREATE_PRODUCT_REQUEST;
  payload: {
    body: TCreateProductBody;
    cb?: (data: TGetOneProductsResponse) => void;
  };
};

export type TCreateProductSuccess = {
  type: EProductAction.CREATE_PRODUCT_SUCCESS;
  payload: {
    data: TGetOneProductsResponse;
  };
};

export type TCreateProductFailed = { type: EProductAction.CREATE_PRODUCT_FAILED };

export type TUpdateProductRequest = {
  type: EProductAction.UPDATE_PRODUCT_REQUEST;
  payload: {
    id: string;
    body: TCreateProductBody;
    cb?: (data: TGetOneProductsResponse) => void;
  };
};

export type TUpdateProductSuccess = {
  type: EProductAction.UPDATE_PRODUCT_SUCCESS;
  payload: {
    data: TGetOneProductsResponse;
  };
};

export type TUpdateProductFailed = { type: EProductAction.UPDATE_PRODUCT_FAILED };

export type TDeleteProductRequest = {
  type: EProductAction.DELETE_PRODUCT_REQUEST;
  payload: {
    id: string;
    cb?: (data: TGetOneProductsResponse) => void;
  };
};

export type TDeleteProductSuccess = {
  type: EProductAction.DELETE_PRODUCT_SUCCESS;
  payload: {
    data: TGetOneProductsResponse;
  };
};

export type TDeleteProductFailed = { type: EProductAction.DELETE_PRODUCT_FAILED };
