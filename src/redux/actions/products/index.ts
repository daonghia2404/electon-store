import { createActionCreator } from 'deox';
import { EProductAction } from 'redux/actions/products/constants';
import {
  TCreateProductFailed,
  TCreateProductRequest,
  TCreateProductSuccess,
  TDeleteProductFailed,
  TDeleteProductRequest,
  TDeleteProductSuccess,
  TGetAllProductFailed,
  TGetAllProductRequest,
  TGetAllProductSuccess,
  TGetOneProductFailed,
  TGetOneProductRequest,
  TGetOneProductSuccess,
  TUpdateProductFailed,
  TUpdateProductRequest,
  TUpdateProductSuccess,
} from 'redux/actions/products/types';
import { TCreateProductBody, TGetAllProductsResponse, TGetOneProductsResponse } from 'services/products/types';

export const getAllProductAction = {
  request: createActionCreator(
    EProductAction.GET_ALL_PRODUCT_REQUEST,
    (resolve) =>
      (cb?: (data: TGetAllProductsResponse) => void): TGetAllProductRequest =>
        resolve({ cb }),
  ),
  success: createActionCreator(
    EProductAction.GET_ALL_PRODUCT_SUCCESS,
    (resolve) =>
      (data: TGetAllProductsResponse): TGetAllProductSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EProductAction.GET_ALL_PRODUCT_FAILED,
    (resolve) =>
      (error: unknown): TGetAllProductFailed =>
        resolve({ error }),
  ),
};

export const getOneProductAction = {
  request: createActionCreator(
    EProductAction.GET_ONE_PRODUCT_REQUEST,
    (resolve) =>
      (id: string, cb?: (data: TGetOneProductsResponse) => void): TGetOneProductRequest =>
        resolve({ id, cb }),
  ),
  success: createActionCreator(
    EProductAction.GET_ONE_PRODUCT_SUCCESS,
    (resolve) =>
      (data: TGetOneProductsResponse): TGetOneProductSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EProductAction.GET_ONE_PRODUCT_FAILED,
    (resolve) =>
      (error: unknown): TGetOneProductFailed =>
        resolve({ error }),
  ),
};

export const createProductAction = {
  request: createActionCreator(
    EProductAction.CREATE_PRODUCT_REQUEST,
    (resolve) =>
      (body: TCreateProductBody, cb?: (data: TGetOneProductsResponse) => void): TCreateProductRequest =>
        resolve({ body, cb }),
  ),
  success: createActionCreator(
    EProductAction.CREATE_PRODUCT_SUCCESS,
    (resolve) =>
      (data: TGetOneProductsResponse): TCreateProductSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EProductAction.CREATE_PRODUCT_FAILED,
    (resolve) =>
      (error: unknown): TCreateProductFailed =>
        resolve({ error }),
  ),
};

export const updateProductAction = {
  request: createActionCreator(
    EProductAction.UPDATE_PRODUCT_REQUEST,
    (resolve) =>
      (id: string, body: TCreateProductBody, cb?: (data: TGetOneProductsResponse) => void): TUpdateProductRequest =>
        resolve({ id, body, cb }),
  ),
  success: createActionCreator(
    EProductAction.UPDATE_PRODUCT_SUCCESS,
    (resolve) =>
      (data: TGetOneProductsResponse): TUpdateProductSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EProductAction.UPDATE_PRODUCT_FAILED,
    (resolve) =>
      (error: unknown): TUpdateProductFailed =>
        resolve({ error }),
  ),
};

export const deleteProductAction = {
  request: createActionCreator(
    EProductAction.DELETE_PRODUCT_REQUEST,
    (resolve) =>
      (id: string, cb?: (data: TGetOneProductsResponse) => void): TDeleteProductRequest =>
        resolve({ id, cb }),
  ),
  success: createActionCreator(
    EProductAction.DELETE_PRODUCT_SUCCESS,
    (resolve) =>
      (data: TGetOneProductsResponse): TDeleteProductSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EProductAction.DELETE_PRODUCT_FAILED,
    (resolve) =>
      (error: unknown): TDeleteProductFailed =>
        resolve({ error }),
  ),
};
