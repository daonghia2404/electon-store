import { createActionCreator } from 'deox';
import { TLoginBody, TLoginResponse } from 'services/auth/types';

import { EAuthAction } from './constants';
import { TLoginRequest, TLoginSuccess, TLoginFailed, TRemoveAuthUser } from './types';

export const loginAction = {
  request: createActionCreator(
    EAuthAction.LOGIN_REQUEST,
    (resolve) =>
      (body: TLoginBody, cb?: (data: TLoginResponse) => void): TLoginRequest =>
        resolve({ body, cb }),
  ),
  success: createActionCreator(
    EAuthAction.LOGIN_SUCCESS,
    (resolve) =>
      (loginResponse: any): TLoginSuccess =>
        resolve({ loginResponse }),
  ),
  failure: createActionCreator(
    EAuthAction.LOGIN_FAILED,
    (resolve) =>
      (error: unknown): TLoginFailed =>
        resolve({ error }),
  ),
};

export const removeAuthUserAction = createActionCreator(
  EAuthAction.REMOVE_AUTH_USER,
  (resolve) => (): TRemoveAuthUser => resolve({}),
);
