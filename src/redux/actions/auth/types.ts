import { TLoginBody, TLoginResponse } from 'services/auth/types';
import { EAuthAction } from './constants';

// LOGIN
export type TLoginRequest = {
  type: EAuthAction.LOGIN_REQUEST;
  payload: {
    body: TLoginBody;
    cb?: (data: TLoginResponse) => void;
  };
};

export type TLoginSuccess = {
  type: EAuthAction.LOGIN_SUCCESS;
  payload: {
    loginResponse: TLoginResponse;
  };
};

export type TLoginFailed = { type: EAuthAction.LOGIN_FAILED };

export type TRemoveAuthUser = { type: EAuthAction.REMOVE_AUTH_USER };
