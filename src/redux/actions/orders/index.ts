import { createActionCreator } from 'deox';
import { EOrderAction } from 'redux/actions/orders/constants';
import {
  TCreateOrderFailed,
  TCreateOrderRequest,
  TCreateOrderSuccess,
  TDeleteOrderFailed,
  TDeleteOrderRequest,
  TDeleteOrderSuccess,
  TGetAllOrderFailed,
  TGetAllOrderRequest,
  TGetAllOrderSuccess,
  TGetOneOrderFailed,
  TGetOneOrderRequest,
  TGetOneOrderSuccess,
  TUpdateOrderFailed,
  TUpdateOrderRequest,
  TUpdateOrderSuccess,
} from 'redux/actions/orders/types';
import { TAddOrderBody, TGetAllOrdersResponse, TGetOneOrdersResponse, TUpdateOrderBody } from 'services/orders/types';

export const getAllOrderAction = {
  request: createActionCreator(
    EOrderAction.GET_ALL_ORDER_REQUEST,
    (resolve) =>
      (cb?: (data: TGetAllOrdersResponse) => void): TGetAllOrderRequest =>
        resolve({ cb }),
  ),
  success: createActionCreator(
    EOrderAction.GET_ALL_ORDER_SUCCESS,
    (resolve) =>
      (data: TGetAllOrdersResponse): TGetAllOrderSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EOrderAction.GET_ALL_ORDER_FAILED,
    (resolve) =>
      (error: unknown): TGetAllOrderFailed =>
        resolve({ error }),
  ),
};

export const getOneOrderAction = {
  request: createActionCreator(
    EOrderAction.GET_ONE_ORDER_REQUEST,
    (resolve) =>
      (id: string, cb?: (data: TGetOneOrdersResponse) => void): TGetOneOrderRequest =>
        resolve({ id, cb }),
  ),
  success: createActionCreator(
    EOrderAction.GET_ONE_ORDER_SUCCESS,
    (resolve) =>
      (data: TGetOneOrdersResponse): TGetOneOrderSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EOrderAction.GET_ONE_ORDER_FAILED,
    (resolve) =>
      (error: unknown): TGetOneOrderFailed =>
        resolve({ error }),
  ),
};

export const createOrderAction = {
  request: createActionCreator(
    EOrderAction.CREATE_ORDER_REQUEST,
    (resolve) =>
      (body: TAddOrderBody, cb?: (data: TGetOneOrdersResponse) => void): TCreateOrderRequest =>
        resolve({ body, cb }),
  ),
  success: createActionCreator(
    EOrderAction.CREATE_ORDER_SUCCESS,
    (resolve) =>
      (data: TGetOneOrdersResponse): TCreateOrderSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EOrderAction.CREATE_ORDER_FAILED,
    (resolve) =>
      (error: unknown): TCreateOrderFailed =>
        resolve({ error }),
  ),
};

export const updateOrderAction = {
  request: createActionCreator(
    EOrderAction.UPDATE_ORDER_REQUEST,
    (resolve) =>
      (id: string, body: TUpdateOrderBody, cb?: (data: TGetOneOrdersResponse) => void): TUpdateOrderRequest =>
        resolve({ id, body, cb }),
  ),
  success: createActionCreator(
    EOrderAction.UPDATE_ORDER_SUCCESS,
    (resolve) =>
      (data: TGetOneOrdersResponse): TUpdateOrderSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EOrderAction.UPDATE_ORDER_FAILED,
    (resolve) =>
      (error: unknown): TUpdateOrderFailed =>
        resolve({ error }),
  ),
};

export const deleteOrderAction = {
  request: createActionCreator(
    EOrderAction.DELETE_ORDER_REQUEST,
    (resolve) =>
      (id: string, cb?: (data: TGetOneOrdersResponse) => void): TDeleteOrderRequest =>
        resolve({ id, cb }),
  ),
  success: createActionCreator(
    EOrderAction.DELETE_ORDER_SUCCESS,
    (resolve) =>
      (data: TGetOneOrdersResponse): TDeleteOrderSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EOrderAction.DELETE_ORDER_FAILED,
    (resolve) =>
      (error: unknown): TDeleteOrderFailed =>
        resolve({ error }),
  ),
};
