import { TAddOrderBody, TGetAllOrdersResponse, TGetOneOrdersResponse, TUpdateOrderBody } from 'services/orders/types';
import { EOrderAction } from './constants';

export type TGetAllOrderRequest = {
  type: EOrderAction.GET_ALL_ORDER_REQUEST;
  payload: {
    cb?: (data: TGetAllOrdersResponse) => void;
  };
};

export type TGetAllOrderSuccess = {
  type: EOrderAction.GET_ALL_ORDER_SUCCESS;
  payload: {
    data: TGetAllOrdersResponse;
  };
};

export type TGetAllOrderFailed = { type: EOrderAction.GET_ALL_ORDER_FAILED };

export type TGetOneOrderRequest = {
  type: EOrderAction.GET_ONE_ORDER_REQUEST;
  payload: {
    id: string;
    cb?: (data: TGetOneOrdersResponse) => void;
  };
};

export type TGetOneOrderSuccess = {
  type: EOrderAction.GET_ONE_ORDER_SUCCESS;
  payload: {
    data: TGetOneOrdersResponse;
  };
};

export type TGetOneOrderFailed = { type: EOrderAction.GET_ONE_ORDER_FAILED };

export type TCreateOrderRequest = {
  type: EOrderAction.CREATE_ORDER_REQUEST;
  payload: {
    body: TAddOrderBody;
    cb?: (data: TGetOneOrdersResponse) => void;
  };
};

export type TCreateOrderSuccess = {
  type: EOrderAction.CREATE_ORDER_SUCCESS;
  payload: {
    data: TGetOneOrdersResponse;
  };
};

export type TCreateOrderFailed = { type: EOrderAction.CREATE_ORDER_FAILED };

export type TUpdateOrderRequest = {
  type: EOrderAction.UPDATE_ORDER_REQUEST;
  payload: {
    id: string;
    body: TUpdateOrderBody;
    cb?: (data: TGetOneOrdersResponse) => void;
  };
};

export type TUpdateOrderSuccess = {
  type: EOrderAction.UPDATE_ORDER_SUCCESS;
  payload: {
    data: TGetOneOrdersResponse;
  };
};

export type TUpdateOrderFailed = { type: EOrderAction.UPDATE_ORDER_FAILED };

export type TDeleteOrderRequest = {
  type: EOrderAction.DELETE_ORDER_REQUEST;
  payload: {
    id: string;
    cb?: (data: TGetOneOrdersResponse) => void;
  };
};

export type TDeleteOrderSuccess = {
  type: EOrderAction.DELETE_ORDER_SUCCESS;
  payload: {
    data: TGetOneOrdersResponse;
  };
};

export type TDeleteOrderFailed = { type: EOrderAction.DELETE_ORDER_FAILED };
