import { createActionCreator } from 'deox';
import { EConfigAction } from 'redux/actions/config/constants';
import { TGetAllConfigFailed, TGetAllConfigRequest, TGetAllConfigSuccess } from 'redux/actions/config/types';
import { TGetAllConfigResponse } from 'services/config/types';

export const getAllConfigAction = {
  request: createActionCreator(
    EConfigAction.GET_ALL_CONFIG_REQUEST,
    (resolve) =>
      (cb?: (data: TGetAllConfigResponse) => void): TGetAllConfigRequest =>
        resolve({ cb }),
  ),
  success: createActionCreator(
    EConfigAction.GET_ALL_CONFIG_SUCCESS,
    (resolve) =>
      (data: TGetAllConfigResponse): TGetAllConfigSuccess =>
        resolve({ data }),
  ),
  failure: createActionCreator(
    EConfigAction.GET_ALL_CONFIG_FAILED,
    (resolve) =>
      (error: unknown): TGetAllConfigFailed =>
        resolve({ error }),
  ),
};
