import { TGetAllConfigResponse } from 'services/config/types';
import { EConfigAction } from './constants';

export type TGetAllConfigRequest = {
  type: EConfigAction.GET_ALL_CONFIG_REQUEST;
  payload: {
    cb?: (data: TGetAllConfigResponse) => void;
  };
};

export type TGetAllConfigSuccess = {
  type: EConfigAction.GET_ALL_CONFIG_SUCCESS;
  payload: {
    data: TGetAllConfigResponse;
  };
};

export type TGetAllConfigFailed = { type: EConfigAction.GET_ALL_CONFIG_FAILED };
