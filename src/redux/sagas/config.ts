import { all, call, put, takeLatest } from 'redux-saga/effects';
import { ActionType } from 'deox';

import { getAllConfigAction } from 'redux/actions/config';
import ConfigInstance from 'services/config';
import { TGetAllConfigResponse } from 'services/config/types';

export function* getAllConfigSaga(action: ActionType<typeof getAllConfigAction.request>): Generator {
  try {
    const { cb } = action.payload;
    const response = yield call(ConfigInstance.getAll);
    const dataResponse: TGetAllConfigResponse = response as TGetAllConfigResponse;

    yield put(getAllConfigAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(getAllConfigAction.failure(err));
  }
}

export default function* root(): Generator {
  yield all([takeLatest(getAllConfigAction.request.type, getAllConfigSaga)]);
}
