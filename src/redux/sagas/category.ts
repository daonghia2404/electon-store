import { all, call, put, takeLatest } from 'redux-saga/effects';
import { ActionType } from 'deox';
import CategoriesInstance from 'services/category';
import { TGetAllCategoriesResponse } from 'services/category/types';
import { getAllCategoriessAction } from 'redux/actions/category';

export function* getAllCategoriesSaga(action: ActionType<typeof getAllCategoriessAction.request>): Generator {
  try {
    const { cb } = action.payload;
    const response = yield call(CategoriesInstance.getAll);
    const dataResponse: TGetAllCategoriesResponse = response as TGetAllCategoriesResponse;

    yield put(getAllCategoriessAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(getAllCategoriessAction.failure(err));
  }
}

export default function* root(): Generator {
  yield all([takeLatest(getAllCategoriessAction.request.type, getAllCategoriesSaga)]);
}
