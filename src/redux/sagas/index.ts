import { all, fork } from 'redux-saga/effects';

import authSaga from './auth';
import configSaga from './config';
import colorsSaga from './colors';
import productsSaga from './products';
import ordersSaga from './orders';
import categoriesSaga from './category';
import uploadSaga from './upload';

const rootSaga = function* root(): Generator {
  yield all([fork(authSaga)]);
  yield all([fork(configSaga)]);
  yield all([fork(colorsSaga)]);
  yield all([fork(productsSaga)]);
  yield all([fork(ordersSaga)]);
  yield all([fork(categoriesSaga)]);
  yield all([fork(uploadSaga)]);
};

export default rootSaga;
