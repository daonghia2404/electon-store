import { all, call, put, takeLatest } from 'redux-saga/effects';
import { ActionType } from 'deox';
import { uploadAction } from 'redux/actions';
import UploadInstance from 'services/upload';
import { TUploadResponse } from 'services/upload/types';

export function* uploadSingleSaga(action: ActionType<typeof uploadAction.request>): Generator {
  try {
    const { body, cb } = action.payload;
    const response = yield call(UploadInstance.upload, body);
    const dataResponse: TUploadResponse = response as TUploadResponse;

    yield put(uploadAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(uploadAction.failure(err));
  }
}

export default function* root(): Generator {
  yield all([takeLatest(uploadAction.request.type, uploadSingleSaga)]);
}
