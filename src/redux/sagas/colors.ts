import { all, call, put, takeLatest } from 'redux-saga/effects';
import { ActionType } from 'deox';

import { getAllColorsAction } from 'redux/actions/colors';
import ColorsInstance from 'services/colors';
import { TGetAllColorsResponse } from 'services/colors/types';

export function* getAllColorsSaga(action: ActionType<typeof getAllColorsAction.request>): Generator {
  try {
    const { cb } = action.payload;
    const response = yield call(ColorsInstance.getAll);
    const dataResponse: TGetAllColorsResponse = response as TGetAllColorsResponse;

    yield put(getAllColorsAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(getAllColorsAction.failure(err));
  }
}

export default function* root(): Generator {
  yield all([takeLatest(getAllColorsAction.request.type, getAllColorsSaga)]);
}
