import { all, call, put, takeLatest } from 'redux-saga/effects';
import { ActionType } from 'deox';
import ProductInstance from 'services/products';
import { TGetAllProductsResponse, TGetOneProductsResponse } from 'services/products/types';
import {
  createProductAction,
  deleteProductAction,
  getAllProductAction,
  getOneProductAction,
  updateProductAction,
} from 'redux/actions';

export function* getAllProductSaga(action: ActionType<typeof getAllProductAction.request>): Generator {
  try {
    const { cb } = action.payload;
    const response = yield call(ProductInstance.getAll);
    const dataResponse: TGetAllProductsResponse = response as TGetAllProductsResponse;

    yield put(getAllProductAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(getAllProductAction.failure(err));
  }
}

export function* getOneProductSaga(action: ActionType<typeof getOneProductAction.request>): Generator {
  try {
    const { id, cb } = action.payload;
    const response = yield call(ProductInstance.getOne, id);
    const dataResponse: TGetOneProductsResponse = response as TGetOneProductsResponse;

    yield put(getOneProductAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(getOneProductAction.failure(err));
  }
}

export function* createProductSaga(action: ActionType<typeof createProductAction.request>): Generator {
  try {
    const { body, cb } = action.payload;
    const response = yield call(ProductInstance.create, body);
    const dataResponse: TGetOneProductsResponse = response as TGetOneProductsResponse;

    yield put(createProductAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(createProductAction.failure(err));
  }
}

export function* updateProductSaga(action: ActionType<typeof updateProductAction.request>): Generator {
  try {
    const { id, body, cb } = action.payload;
    const response = yield call(ProductInstance.update, id, body);
    const dataResponse: TGetOneProductsResponse = response as TGetOneProductsResponse;

    yield put(updateProductAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(updateProductAction.failure(err));
  }
}

export function* deleteProductSaga(action: ActionType<typeof deleteProductAction.request>): Generator {
  try {
    const { id, cb } = action.payload;
    const response = yield call(ProductInstance.delete, id);
    const dataResponse: TGetOneProductsResponse = response as TGetOneProductsResponse;

    yield put(deleteProductAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(deleteProductAction.failure(err));
  }
}

export default function* root(): Generator {
  yield all([takeLatest(getAllProductAction.request.type, getAllProductSaga)]);
  yield all([takeLatest(getOneProductAction.request.type, getOneProductSaga)]);
  yield all([takeLatest(createProductAction.request.type, createProductSaga)]);
  yield all([takeLatest(updateProductAction.request.type, updateProductSaga)]);
  yield all([takeLatest(deleteProductAction.request.type, deleteProductSaga)]);
}
