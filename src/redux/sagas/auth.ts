import { all, call, put, takeLatest } from 'redux-saga/effects';
import { ActionType } from 'deox';

import { loginAction } from 'redux/actions';
import AuthInstance from 'services/auth';
import helper from 'services/helpers';
import { TLoginResponse } from 'services/auth/types';

export function* loginSaga(action: ActionType<typeof loginAction.request>): Generator {
  try {
    const { body, cb } = action.payload;
    const response = yield call(AuthInstance.login, body);
    const loginResponse: TLoginResponse = response as TLoginResponse;

    helper.storeAccessToken(loginResponse.accessToken);
    helper.storeRefreshToken(loginResponse.refreshToken);

    yield put(loginAction.success(loginResponse));
    cb?.(loginResponse);
  } catch (err) {
    yield put(loginAction.failure(err));
  }
}

export default function* root(): Generator {
  yield all([takeLatest(loginAction.request.type, loginSaga)]);
}
