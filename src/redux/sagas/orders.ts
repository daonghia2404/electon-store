import { all, call, put, takeLatest } from 'redux-saga/effects';
import { ActionType } from 'deox';
import OrderInstance from 'services/orders';
import { TGetAllOrdersResponse, TGetOneOrdersResponse } from 'services/orders/types';
import {
  createOrderAction,
  deleteOrderAction,
  getAllOrderAction,
  getOneOrderAction,
  updateOrderAction,
} from 'redux/actions';

export function* getAllOrderSaga(action: ActionType<typeof getAllOrderAction.request>): Generator {
  try {
    const { cb } = action.payload;
    const response = yield call(OrderInstance.getAll);
    const dataResponse: TGetAllOrdersResponse = response as TGetAllOrdersResponse;

    yield put(getAllOrderAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(getAllOrderAction.failure(err));
  }
}

export function* getOneOrderSaga(action: ActionType<typeof getOneOrderAction.request>): Generator {
  try {
    const { id, cb } = action.payload;
    const response = yield call(OrderInstance.getOne, id);
    const dataResponse: TGetOneOrdersResponse = response as TGetOneOrdersResponse;

    yield put(getOneOrderAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(getOneOrderAction.failure(err));
  }
}

export function* createOrderSaga(action: ActionType<typeof createOrderAction.request>): Generator {
  try {
    const { body, cb } = action.payload;
    const response = yield call(OrderInstance.create, body);
    const dataResponse: TGetOneOrdersResponse = response as TGetOneOrdersResponse;

    yield put(createOrderAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(createOrderAction.failure(err));
  }
}

export function* updateOrderSaga(action: ActionType<typeof updateOrderAction.request>): Generator {
  try {
    const { id, body, cb } = action.payload;
    const response = yield call(OrderInstance.update, id, body);
    const dataResponse: TGetOneOrdersResponse = response as TGetOneOrdersResponse;

    yield put(updateOrderAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(updateOrderAction.failure(err));
  }
}

export function* deleteOrderSaga(action: ActionType<typeof deleteOrderAction.request>): Generator {
  try {
    const { id, cb } = action.payload;
    const response = yield call(OrderInstance.delete, id);
    const dataResponse: TGetOneOrdersResponse = response as TGetOneOrdersResponse;

    yield put(deleteOrderAction.success(dataResponse));
    cb?.(dataResponse);
  } catch (err) {
    yield put(deleteOrderAction.failure(err));
  }
}

export default function* root(): Generator {
  yield all([takeLatest(getAllOrderAction.request.type, getAllOrderSaga)]);
  yield all([takeLatest(getOneOrderAction.request.type, getOneOrderSaga)]);
  yield all([takeLatest(createOrderAction.request.type, createOrderSaga)]);
  yield all([takeLatest(updateOrderAction.request.type, updateOrderSaga)]);
  yield all([takeLatest(deleteOrderAction.request.type, deleteOrderSaga)]);
}
