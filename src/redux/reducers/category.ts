import { createReducer } from 'deox';
import { getAllCategoriessAction } from 'redux/actions/category';

import { TCategoriesOption } from 'services/category/types';

export interface IColorsState {
  categories: TCategoriesOption[];
}

const initialState: IColorsState = {
  categories: [],
};

const categoryReducer = createReducer(initialState, (handleAction) => [
  handleAction(getAllCategoriessAction.success, (state, action) => {
    const { data } = action.payload;
    return { ...state, categories: data.message };
  }),
]);

export default categoryReducer;
