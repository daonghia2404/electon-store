import { createReducer } from 'deox';
import { getAllProductAction, getOneProductAction } from 'redux/actions';
import { TProductResponse } from 'services/products/types';

export interface IProductState {
  products: TProductResponse[];
  product: TProductResponse;
  total: number;
}

const initialState: IProductState = {
  products: [],
  product: {
    _id: '',
    color_id: [],
    config_id: [],
    title: '',
    description: '',
    price: 0,
    sale: 0,
    cat_id: { _id: '', title: '' },
    content: '',
    stock: 0,
    createdAt: '',
    updatedAt: '',
    image: '',
  },
  total: 0,
};

const productsReducer = createReducer(initialState, (handleAction) => [
  handleAction(getAllProductAction.success, (state, action) => {
    const { data } = action.payload;
    return { ...state, products: data.message, total: data.total };
  }),
  handleAction(getOneProductAction.success, (state, action) => {
    const { data } = action.payload;
    return { ...state, product: data.message };
  }),
]);

export default productsReducer;
