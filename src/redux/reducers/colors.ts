import { createReducer } from 'deox';

import { getAllColorsAction } from 'redux/actions/colors';
import { TColorsOption } from 'services/colors/types';

export interface IColorsState {
  colors: TColorsOption[];
}

const initialState: IColorsState = {
  colors: [],
};

const colorsReducer = createReducer(initialState, (handleAction) => [
  handleAction(getAllColorsAction.success, (state, action) => {
    const { data } = action.payload;
    return { ...state, colors: data.message };
  }),
]);

export default colorsReducer;
