import { createReducer } from 'deox';
import { uiAction } from 'redux/actions';

import { removeDuplicateInArray } from 'utils/function';

export interface IUIState {
  cart: Array<any>;
  checkout: Array<any>;
}

const initialState: IUIState = {
  cart: [],
  checkout: [],
};

const uiReducer = createReducer(initialState, (handleAction) => [
  handleAction(uiAction.setCart, (state, action) => {
    const { cart } = action.payload;

    const newCart = cart.map((product, productIndex) => {
      let { quantity } = product;
      cart.forEach((item, index) => {
        const isSameProduct =
          productIndex !== index &&
          product.cartProductId === item.cartProductId &&
          product._id === item._id &&
          product.config_id === item.config_id &&
          product.color_id === item.color_id;

        if (isSameProduct) quantity += item.quantity;
      });

      return { ...product, quantity };
    });

    return { ...state, cart: newCart };
  }),
  handleAction(uiAction.setCheckout, (state, action) => {
    const { checkout } = action.payload;

    return { ...state, checkout };
  }),
]);

export default uiReducer;
