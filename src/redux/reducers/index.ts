import { combineReducers } from 'redux';

import loading from 'redux/reducers/status/loading';
import success from 'redux/reducers/status/success';
import error from 'redux/reducers/status/error';
import ui from 'redux/reducers/ui';
import configState from 'redux/reducers/config';
import colorsState from 'redux/reducers/colors';
import productsState from 'redux/reducers/products';
import ordersState from 'redux/reducers/orders';
import categoriesState from 'redux/reducers/category';

const rootReducer = combineReducers({
  loading,
  success,
  error,
  ui,
  configState,
  colorsState,
  productsState,
  categoriesState,
  ordersState,
});

export default rootReducer;

export type TRootState = ReturnType<typeof rootReducer>;
