import { createReducer } from 'deox';
import { getAllOrderAction, getOneOrderAction } from 'redux/actions';
import { EOrderStatus } from 'services/orders/enums';
import { TOrderResponse } from 'services/orders/types';

export interface IOrderState {
  orders: TOrderResponse[];
  order: TOrderResponse;
  total: number;
}

const initialState: IOrderState = {
  orders: [],
  order: {
    _id: '',
    status: EOrderStatus.PENDING,
    products: [],
    firstName: '',
    lastName: '',
    address: '',
    phoneNumber: '',
    message: '',
    city: '',
    country: '',
    unitShipping: '',
    createdAt: '',
    updatedAt: '',
  },
  total: 0,
};

const ordersReducer = createReducer(initialState, (handleAction) => [
  handleAction(getAllOrderAction.success, (state, action) => {
    const { data } = action.payload;
    return { ...state, orders: data.message, total: data.total };
  }),
  handleAction(getOneOrderAction.success, (state, action) => {
    const { data } = action.payload;
    return { ...state, order: data.message };
  }),
]);

export default ordersReducer;
