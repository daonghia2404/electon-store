import { createReducer } from 'deox';

import { getAllConfigAction } from 'redux/actions/config';
import { TConfigOption } from 'services/config/types';

export interface IConfigState {
  config: TConfigOption[];
}

const initialState: IConfigState = {
  config: [],
};

const configReducer = createReducer(initialState, (handleAction) => [
  handleAction(getAllConfigAction.success, (state, action) => {
    const { data } = action.payload;
    return { ...state, config: data.message };
  }),
]);

export default configReducer;
