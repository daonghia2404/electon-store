import env from 'env';
import AuthorizedInstance from './authorized';

const Service = AuthorizedInstance(env.api.baseUrl.service);

export default Service;
