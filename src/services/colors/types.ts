import { EProductColor } from 'common/enums';

export type TColorsOption = {
  _id: string;
  name: EProductColor;
};

export type TGetAllColorsResponse = {
  status: string;
  message: TColorsOption[];
};
