import Services from 'services';
import { TGetAllColorsResponse } from 'services/colors/types';

class Colors {
  getAll = async (): Promise<TGetAllColorsResponse> => {
    const response = await Services.get(`/colors`);
    return response.data;
  };
}

const ColorsInstance = new Colors();
export default ColorsInstance;
