export enum EOrderStatus {
  PENDING = 'PENDING',
  INPROCESS = 'INPROCESS',
  SUCCESS = 'SUCCESS',
  REJECT = 'REJECT',
}
