import { EOrderStatus } from 'services/orders/enums';

export type TOrderResponse = {
  _id: string;
  status: EOrderStatus;
  products: Array<{
    _id: string;
    title: string;
    price: number;
    color_id: string;
    config_id: string;
    quatity: number;
    sale: number;
    quantity: number;
    image: string;
  }>;
  firstName: string;
  lastName: string;
  address: string;
  phoneNumber: string;
  message: string;
  city: string;
  country: string;
  unitShipping: string;
  createdAt: string;
  updatedAt: string;
};

export type TGetAllOrdersResponse = {
  status: string;
  message: TOrderResponse[];
  total: number;
};

export type TGetOneOrdersResponse = {
  status: string;
  message: TOrderResponse;
};

export type TAddOrderBody = {
  firstName: string;
  lastName: string;
  address: string;
  phoneNumber: string;
  message: string;
  city: string;
  country: string;
  unitShipping: string;
  products: Array<{
    _id: string;
    title: string;
    price: number;
    color_id: string;
    config_id: string;
    quatity: number;
    sale: number;
    image: string;
  }>;
};

export type TUpdateOrderBody = {
  status: EOrderStatus;
};
