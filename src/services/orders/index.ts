import Services from 'services';
import { TAddOrderBody, TGetAllOrdersResponse, TGetOneOrdersResponse, TUpdateOrderBody } from 'services/orders/types';

class Order {
  getAll = async (): Promise<TGetAllOrdersResponse> => {
    const response = await Services.get(`/orders`);
    return response.data;
  };

  getOne = async (id: string): Promise<TGetOneOrdersResponse> => {
    const response = await Services.get(`/orders/${id}`);
    return response.data;
  };

  create = async (params: TAddOrderBody): Promise<TGetOneOrdersResponse> => {
    const response = await Services.post(`/add-order`, params);
    return response.data;
  };

  update = async (id: string, params: TUpdateOrderBody): Promise<TGetOneOrdersResponse> => {
    const response = await Services.put(`/orders/${id}`, params);
    return response.data;
  };

  delete = async (id: string): Promise<TGetOneOrdersResponse> => {
    const response = await Services.delete(`/orders/${id}`);
    return response.data;
  };
}

const OrderInstance = new Order();
export default OrderInstance;
