export type TConfigOption = {
  _id: string;
  name: string;
};

export type TGetAllConfigResponse = {
  status: string;
  message: TConfigOption[];
};
