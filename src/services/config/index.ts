import Services from 'services';
import { TGetAllConfigResponse } from 'services/config/types';

class Config {
  getAll = async (): Promise<TGetAllConfigResponse> => {
    const response = await Services.get(`/configs`);
    return response.data;
  };
}

const ConfigInstance = new Config();
export default ConfigInstance;
