import axios, { AxiosRequestConfig, AxiosResponse, AxiosError, AxiosInstance } from 'axios';
import { navigate } from '@reach/router';

import { EResponseCode } from 'common/enums';
import { LayoutPaths, Paths } from 'pages/routers';
import authHelpers from './helpers';
import AuthInstance from './auth';

const keyToken = 'x-token';

const AuthorizedInstance = (baseURL: string): AxiosInstance => {
  const instance = axios.create({
    baseURL,
  });

  const refreshTokens = async (): Promise<string> => {
    const existingRefreshToken: string = authHelpers.getRefreshToken();

    if (!existingRefreshToken) {
      navigate(`${LayoutPaths.Auth}${Paths.AdminLogin}`);
    }

    const { jwtAccessToken, refreshToken } = await AuthInstance.refreshToken({
      refreshToken: existingRefreshToken ?? '',
    });

    authHelpers.storeAccessToken(jwtAccessToken);
    authHelpers.storeRefreshToken(refreshToken);

    return authHelpers.getAccessToken();
  };

  const onRequest = (request: AxiosRequestConfig): AxiosRequestConfig => {
    const authBearer: string | unknown = authHelpers.getAccessToken();
    if (authBearer) {
      request.headers[keyToken] = `Bearer ${authBearer}`;
    }

    return request;
  };

  const onResponseSuccess = (response: AxiosResponse): AxiosResponse => response;

  const onResponseError = async (axiosError: AxiosError): Promise<void | AxiosResponse<any>> => {
    const { response } = axiosError;
    const responseStatus = response?.status;
    const originalRequest = axiosError.config;

    if (responseStatus === EResponseCode.UNAUTHORIZED && originalRequest) {
      return refreshTokens()
        .then((newAccessToken) => {
          originalRequest.headers[keyToken] = `Bearer ${newAccessToken}`;
          return axios.request(originalRequest);
        })
        .catch(() => {
          authHelpers.clearTokens();
          navigate(`${LayoutPaths.Auth}${Paths.AdminLogin}`);
        });
    }

    if (responseStatus === EResponseCode.FORBIDDEN && originalRequest) {
      authHelpers.clearTokens();
      navigate(`${LayoutPaths.Auth}${Paths.AdminLogin}`);
    }

    return Promise.reject(axiosError);
  };

  instance.interceptors.request.use(onRequest);
  instance.interceptors.response.use(onResponseSuccess, onResponseError);

  return instance;
};

export default AuthorizedInstance;
