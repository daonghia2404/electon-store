import Services from 'services';
import { TCreateProductBody, TGetAllProductsResponse, TGetOneProductsResponse } from 'services/products/types';

class Product {
  getAll = async (): Promise<TGetAllProductsResponse> => {
    const response = await Services.get(`/products`);
    return response.data;
  };

  getOne = async (id: string): Promise<TGetOneProductsResponse> => {
    const response = await Services.get(`/products/${id}`);
    return response.data;
  };

  create = async (params: TCreateProductBody): Promise<TGetOneProductsResponse> => {
    const response = await Services.post(`/add-product`, params);
    return response.data;
  };

  update = async (id: string, params: TCreateProductBody): Promise<TGetOneProductsResponse> => {
    const response = await Services.put(`/products/${id}`, params);
    return response.data;
  };

  delete = async (id: string): Promise<TGetOneProductsResponse> => {
    const response = await Services.delete(`/products/${id}`);
    return response.data;
  };
}

const ProductInstance = new Product();
export default ProductInstance;
