import { EProductColor } from 'common/enums';

export type TProductResponse = {
  _id: string;
  color_id: Array<{ _id: string; name: EProductColor }>;
  config_id: Array<{ _id: string; name: string }>;
  title: string;
  description: string;
  price: number;
  sale: number;
  cat_id: { _id: string; title: string };
  image: string;
  content: string;
  stock: number;
  createdAt: string;
  updatedAt: string;
};

export type TGetAllProductsResponse = {
  status: string;
  message: TProductResponse[];
  total: number;
};

export type TGetOneProductsResponse = {
  status: string;
  message: TProductResponse;
};

export type TCreateProductBody = {
  title: string;
  description: string;
  price: number;
  sale: number;
  color: string[];
  config: string[];
  category: string;
  stock: number;
  content: string;
  image: string;
};
