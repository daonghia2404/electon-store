import Services from 'services';
import { TUploadResponse } from 'services/upload/types';

class Upload {
  upload = async (body: FormData): Promise<TUploadResponse> => {
    const respone = await Services.post(`/file/upload`, body);
    return respone.data;
  };
}

const UploadInstance = new Upload();
export default UploadInstance;
