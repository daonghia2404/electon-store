export type TCategoriesOption = {
  _id: string;
  title: string;
};

export type TGetAllCategoriesResponse = {
  status: string;
  message: TCategoriesOption[];
};
