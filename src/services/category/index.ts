import Services from 'services';
import { TGetAllCategoriesResponse } from 'services/category/types';

class Categories {
  getAll = async (): Promise<TGetAllCategoriesResponse> => {
    const response = await Services.get(`/categories`);
    return response.data;
  };
}

const CategoriesInstance = new Categories();
export default CategoriesInstance;
