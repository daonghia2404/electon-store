import Services from 'services';
import { TLoginBody, TLoginResponse, TRefreshTokenBody, TRefreshTokenResponse } from './types';

class Auth {
  login = async (body: TLoginBody): Promise<TLoginResponse> => {
    const response = await Services.post(`/login`, body);
    return response.data;
  };

  refreshToken = async (body: TRefreshTokenBody): Promise<TRefreshTokenResponse> => {
    const response = await Services.post(`/renew-token`, body);
    return response.data;
  };
}

const AuthInstance = new Auth();
export default AuthInstance;
