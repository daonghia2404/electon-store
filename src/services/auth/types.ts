export type TLoginBody = {
  username: string;
  password: string;
};

export type TLoginResponse = {
  status: string;
  accessToken: string;
  refreshToken: string;
};

export type TRefreshTokenBody = any;
export type TRefreshTokenResponse = any;
