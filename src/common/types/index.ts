import { EProductColor } from 'common/enums';

export type TCartProduct = {
  image: string;
  title: string;
  quantity: number;
  price: number;
  sale: number;
  _id: string;
  cartProductId: string;
  config_id: string;
  color_id: EProductColor;
};

export type TSelectOption = {
  key?: string;
  label: string;
  value: string;
};

export type TPaginateParams = {
  page: number;
  pageSize: number;
};
