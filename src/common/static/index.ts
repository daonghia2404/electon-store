import ImageCategoryMacbook from 'assets/images/categories/image-categories-macbook.png';
import ImageCategoryMacPro from 'assets/images/categories/image-categories-mac-pro.png';
import ImageCategoryMacMini from 'assets/images/categories/image-categories-mac-mini.png';
import ImageCategoryIMac from 'assets/images/categories/image-categories-imac.png';
import ImageCategoryIPad from 'assets/images/categories/image-categories-ipad.png';
import ImageCategoryIPhone from 'assets/images/categories/image-categories-iphone.png';
import ImageCategoryWatch from 'assets/images/categories/image-categories-watch.png';
import ImageCategoryTV from 'assets/images/categories/image-categories-tv.png';
import ImageCategoryMusic from 'assets/images/categories/image-categories-music.png';

import ImageProductAirpodPro from 'assets/images/products/image-product-airpod-pro.png';
import ImageProductAppleWatchSeries5 from 'assets/images/products/image-product-apple-watch-series-5.png';
import ImageProductIMac2020 from 'assets/images/products/image-product-imac-2020.png';
import ImageProductIPadPro from 'assets/images/products/image-product-ipad-pro.png';
import ImageProductIPhoneProMax from 'assets/images/products/image-product-iphone-13-pro-max.png';
import ImageProductIPhoneXsMax from 'assets/images/products/image-product-iphone-xs-max.png';
import ImageProductMacbookAir from 'assets/images/products/image-product-macbook-air.png';
import ImageProductMacbookPro from 'assets/images/products/image-product-macbook-pro.png';

import ImageBlog1 from 'assets/images/blogs/image-blog-1.jpg';
import ImageBlog2 from 'assets/images/blogs/image-blog-2.jpg';
import ImageBlog3 from 'assets/images/blogs/image-blog-3.jpg';

import { ECategory, EProductColor } from 'common/enums';
import { Paths } from 'pages/routers';
import { EOrderStatus } from 'services/orders/enums';

export const dataCategories = [
  { link: Paths.Categories('id'), image: ImageCategoryMacbook, key: ECategory.macbook, label: 'Macbook' },
  { link: Paths.Categories('id'), image: ImageCategoryMacPro, key: ECategory.macPro, label: 'Mac Pro' },
  { link: Paths.Categories('id'), image: ImageCategoryMacMini, key: ECategory.macMini, label: 'Mac Mini' },
  { link: Paths.Categories('id'), image: ImageCategoryIMac, key: ECategory.imac, label: 'iMac' },
  { link: Paths.Categories('id'), image: ImageCategoryIPad, key: ECategory.iPad, label: 'iPad' },
  { link: Paths.Categories('id'), image: ImageCategoryIPhone, key: ECategory.iPhone, label: 'iPhone' },
  { link: Paths.Categories('id'), image: ImageCategoryWatch, key: ECategory.watch, label: 'Watch' },
  { link: Paths.Categories('id'), image: ImageCategoryTV, key: ECategory.tv, label: 'TV' },
  { link: Paths.Categories('id'), image: ImageCategoryMusic, key: ECategory.music, label: 'Music' },
];

export const dataCategoriesFilter = [
  { key: ECategory.featured, label: 'Sản Phẩm Nổi Bật', total: 12 },
  { key: ECategory.bestSeller, label: 'Sản Phẩm Bán Chạy', total: 12 },
  { key: ECategory.macbook, label: 'Macbook' },
  { key: ECategory.macbook, label: 'Macbook' },
  { key: ECategory.macPro, label: 'Mac Pro' },
  { key: ECategory.macMini, label: 'Mac Mini' },
  { key: ECategory.imac, label: 'iMac' },
  { key: ECategory.iPad, label: 'iPad' },
  { key: ECategory.iPhone, label: 'iPhone' },
  { key: ECategory.watch, label: 'Watch' },
  { key: ECategory.tv, label: 'TV' },
  { key: ECategory.music, label: 'Music' },
];

export const dataStoragesFilter = [
  { key: '32gb', label: '32GB' },
  { key: '64gb', label: '64GB' },
  { key: '128gb', label: '128GB' },
  { key: '256gb', label: '256GB' },
  { key: '512gb', label: '512GB' },
];

export const dataTagsFilter = [
  { key: 'phone', label: 'Điện Thoại' },
  { key: 'tablet', label: 'Máy Tính Bảng' },
  { key: 'laptop', label: 'Máy Tính Xách Tay' },
  { key: 'headphone', label: 'Tai Nghe' },
  { key: 'watch', label: 'Đồng Hồ' },
];

export const dataPricesFilter = [
  { key: '0-3000000', label: '0 - 3.000.000đ' },
  { key: '3000000-6000000', label: '3.000.000đ - 6.000.000đ' },
  { key: '6000000-9000000', label: '6.000.000đ - 9.000.000đ' },
  { key: '9000000-12000000', label: '9.000.000đ - 12.000.000đ' },
  { key: '12000000-15000000', label: '12.000.000đ - 15.000.000đ' },
  { key: '15000000-18000000', label: '15.000.000đ - 18.000.000đ' },
  { key: '18000000-21000000', label: '18.000.000đ - 21.000.000đ' },
  { key: '21000000-24000000', label: '21.000.000đ - 24.000.000đ' },
  { key: '24000000-27000000', label: '24.000.000đ - 27.000.000đ' },
  { key: '>27000000', label: '> 27.000.000đ' },
];

export const dataProducts = [
  {
    id: '1',
    link: Paths.Product('1'),
    image: ImageProductAppleWatchSeries5,
    title: 'Apple Watch Series 5',
    price: 10599000,
    discount: 5,
    stock: 12,
    isNew: true,
    color: EProductColor.pink,
    description:
      'Apple Watch Series 5 bao gồm 2 phiên bản mặt đồng hồ 40mm và 44mm, hỗ trợ chỉ Wi-Fi hoặc mạng di động, dung lượng bộ nhớ lưu trữ 32GB (trước đó chỉ 16GB)',
  },
  {
    id: '2',
    link: Paths.Product('2'),
    image: ImageProductIMac2020,
    title: 'iMac 2020 5K',
    price: 30999000,
    discount: 10,
    stock: 0,
    isNew: true,
    color: EProductColor.white,
    description:
      'Điều này giúp cho màn hình Retina 5K trên iMac 2020 đẹp hơn bao giờ hết. Công nghệ nano-texture lần đầu tiên được giới thiệu trên dòng màn hình cao cấp nhất',
  },
  {
    id: '3',
    link: Paths.Product('3'),
    image: ImageProductAirpodPro,
    title: 'Airpod Pro',
    price: 5399000,
    discount: 0,
    stock: 22,
    isNew: false,
    color: EProductColor.white,
    description:
      'AirPods Pro là mẫu tai nghe True Wireless (không dây) của Apple được đánh giá cao bởi tính năng chống ồn chủ động (ANC) cùng một vài sự cải tiến trong thiết bị',
  },
  {
    id: '4',
    link: Paths.Product('4'),
    image: ImageProductIPadPro,
    title: 'iPad Pro',
    price: 22599000,
    discount: 10,
    stock: 53,
    isNew: true,
    color: EProductColor.gray,
    description:
      'iPad Pro sử dụng hệ điều hành iOS, với 2 kích cỡ màn hình, 9.7-inch và 12.9-inch, mỗi màn hình với 3 tùy chọn cho dung lượng bộ nhớ: 32, 128 hoặc 256 GB',
  },
  {
    id: '5',
    link: Paths.Product('5'),
    image: ImageProductIPhoneProMax,
    title: 'iPhone 13 Pro Max',
    price: 49999000,
    discount: 5,
    stock: 12,
    isNew: true,
    color: EProductColor.gray,
    description:
      'iPhone 13 Series năm nay cũng có bốn phiên bản tương tự như iPhone 12 Series năm ngoái. Đầu tiên là iPhone 13 Mini, chiếc iPhone nhỏ xinh với màn hình 5.4 inch. iPhone 13 và iPhone 13 Pro đều sở hữu màn hình kích thước 6.1 inch. Còn iPhone 13 Pro, phiên bản cao cấp nhất thì sở hữu màn hình lên tới 6.7 inch.',
  },
  {
    id: '6',
    link: Paths.Product('6'),
    image: ImageProductIPhoneXsMax,
    title: 'iPhone Xs Max',
    price: 18599000,
    discount: 15,
    stock: 32,
    isNew: false,
    color: EProductColor.yellow,
    description:
      'Apple giới thiệu iPhone XS và iPhone XS Max: Hỗ trợ 2 SIM, chip A12 Bionic, chống nước IP68, bộ nhớ 512GB, màu vàng mới',
  },
  {
    id: '7',
    link: Paths.Product('7'),
    image: ImageProductMacbookAir,
    title: 'Macbook Air',
    price: 23999000,
    discount: 5,
    stock: 2,
    isNew: true,
    color: EProductColor.yellow,
    description:
      'MacBook Air 2020 là phiên bản có nhiều nâng cấp vượt trội về cấu hình và thiết kế bàn phím, hứa hẹn đem tới trải nghiệm mượt mà, thoải mái hơn tới người dùng.',
  },
  {
    id: '8',
    link: Paths.Product('8'),
    image: ImageProductMacbookPro,
    title: 'Macbook Pro',
    price: 25999000,
    discount: 5,
    stock: 33,
    isNew: true,
    color: EProductColor.gray,
    description:
      'MacBook Pro 13 2020 gần như chỉ là một bản cập nhật bắt buộc, bởi vì đây là chiếc MacBook cuối cùng có bàn phím bướm cũ sau khi cả MacBook Pro 16 và MacBook Air đều nhận được cập nhật. Việc chuyển sang bộ xử lý Ice Lake không thực sự làm thay đổi hay nâng cấp hiệu suất đang có.',
  },
];

export const dataBlogs = [
  {
    image: ImageBlog1,
    createdDate: '14 Tháng 9, 2020',
    link: Paths.Product('id'),
    title: 'Apple ra mắt iPhone 13 Pro và iPhone 13 Pro Max - chuyên nghiệp hơn bao giờ hết',
    description:
      'Hệ thống camera chuyên nghiệp tiên tiến nhất từng có trên iPhone; Màn hình Super Retina XDR với ProMotion; một bước nhảy vọt về tuổi thọ pin; A15 Bionic, chip nhanh nhất trong điện thoại thông minh; trải nghiệm 5G tiên tiến; và nhiều hơn nữa',
  },
  {
    image: ImageBlog2,
    createdDate: '13 Tháng 9, 2020',
    link: Paths.Product('id'),
    title: 'Apple giới thiệu iPhone 13 và iPhone 13 mini',
    description:
      'Có thiết kế đẹp và bền, hệ thống camera kép mới tiên tiến để cải thiện ảnh và video trong điều kiện ánh sáng yếu, đồng thời giới thiệu chế độ Cinematic',
  },
  {
    image: ImageBlog3,
    createdDate: '12 Tháng 9, 2020',
    link: Paths.Product('id'),
    title: 'Apple tiết lộ Apple Watch Series 7, có màn hình lớn hơn, tiên tiến hơn',
    description: 'Thiết kế tinh tế với độ bền nâng cao, sạc nhanh hơn, màu vỏ nhôm mới và watchOS 8',
  },
];

export const dataColors = [
  { key: EProductColor.white, label: 'Màu Trắng' },
  { key: EProductColor.pink, label: 'Màu Hồng' },
  { key: EProductColor.gray, label: 'Màu Xám' },
  { key: EProductColor.yellow, label: 'Màu Vàng' },
];

export const dataHexColor = {
  [EProductColor.gray]: '#7f8c8d',
  [EProductColor.pink]: '#fd79a8',
  [EProductColor.white]: '#fff',
  [EProductColor.yellow]: '#f1c40f',
  [EProductColor.black]: '#000',
};

export const dataTextColor = {
  [EProductColor.white]: 'Trắng',
  [EProductColor.pink]: 'Hồng',
  [EProductColor.gray]: 'Xám',
  [EProductColor.yellow]: 'Vàng',
  [EProductColor.black]: 'Đen',
};

export const dataPageSizeOptions = [
  { value: '10', label: '10' },
  { value: '25', label: '25' },
  { value: '50', label: '50' },
  { value: '75', label: '75' },
  { value: '100', label: '100' },
];

export const emptyTableData = '-';

export const DEFAULT_PAGE = 1;
export const DEFAULT_PAGE_SIZE = 20;

export const dataOrderStatus = {
  [EOrderStatus.PENDING]: 'Chờ Duyệt',
  [EOrderStatus.INPROCESS]: 'Đang Vận Chuyển',
  [EOrderStatus.SUCCESS]: 'Đã Giao Hàng',
  [EOrderStatus.REJECT]: 'Bị Loại Bỏ',
};

export const dataColorOrderStatus = {
  [EOrderStatus.PENDING]: '#f1c40f',
  [EOrderStatus.INPROCESS]: '#105caa',
  [EOrderStatus.SUCCESS]: '#28a745',
  [EOrderStatus.REJECT]: '#FF4D4F',
};

export const dataCountryOptions = [{ label: 'Việt Nam', value: 'viet-nam' }];

export const dataCityOptions = [
  { label: 'Hà Nội', value: 'ha-noi' },
  { label: 'Hải Phòng', value: 'hai-phong' },
  { label: 'Đà Nẵng', value: 'da-nang' },
  { label: 'Thành Phố Hồ Chí Minh', value: 'thanh-pho-ho-chi-minh' },
  { label: 'Cần Thơ', value: 'can-tho' },
];

export const dataUnitShippingOptions = [
  { label: 'Viettel Post', value: 'viettel-post' },
  { label: 'Vietnam Post', value: 'vietnam-post' },
  { label: 'Giao Hàng Nhanh', value: 'giao-hang-nhanh' },
  { label: 'Giao Hàng Tiết Kiệm', value: 'giao-hang-tiet-kiem' },
  { label: 'Kerry Express', value: 'kerry-express' },
  { label: 'SShip', value: 'sship' },
  { label: 'Shipchung', value: 'shipchung' },
];
