enum ValueType {
  string = 'string',
  number = 'number',
  boolean = 'boolean',
}

const correctTypeValue = (value?: string, type: ValueType = ValueType.string): any => {
  if (value) {
    switch (type) {
      case ValueType.boolean:
      case ValueType.number:
        return JSON.parse(value);
      default:
        return value;
    }
  }
  return '';
};

const env = {
  siteName: correctTypeValue(process.env.REACT_APP_SITE_NAME),
  domain: correctTypeValue(process.env.REACT_APP_DOMAIN_NAME),
  rootUrl: correctTypeValue(process.env.REACT_APP_ROOT_URL),
  api: {
    baseUrl: {
      service: correctTypeValue(process.env.REACT_APP_SERVICE_BASE_URL),
    },
    lookUpIpAddress: correctTypeValue(process.env.REACT_APP_LOOKUP_IP_ADDRESS_URL),
  },
  cookie: {
    domain: correctTypeValue(process.env.REACT_APP_COOKIE_DOMAIN),
  },
  dev: {},
};

export default env;
