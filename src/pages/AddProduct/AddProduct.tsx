import React from 'react';

import PageHeader from 'components/PageHeader';
import { dataAddProductBreadcrumb } from 'pages/AddProduct/AddProduct.datas';
import ModuleProductDetail from 'containers/ModuleProductDetail/ModuleProductDetail';
import { ETypeModuleProductDetail } from 'containers/ModuleProductDetail/ModuleProductDetail.enums';
import { getNameFile, showNotification } from 'utils/function';
import { ETypeNotification } from 'common/enums';
import { navigate } from '@reach/router';
import { LayoutPaths, Paths } from 'pages/routers';
import { useDispatch, useSelector } from 'react-redux';
import { createProductAction } from 'redux/actions';
import { TRootState } from 'redux/reducers';
import { EProductAction } from 'redux/actions/products/constants';
import { TModuleProductDetailForm } from 'containers/ModuleProductDetail/ModuleProductDetail.types';

import './AddProduct.scss';

export const AddProduct: React.FC = () => {
  const dispatch = useDispatch();
  const loading = useSelector((state: TRootState) => state.loading[EProductAction.CREATE_PRODUCT]);

  const handleCreateProduct = (data: TModuleProductDetailForm): void => {
    const body = {
      ...data,
      category: data.category.value,
      color: data.color.map((item) => item.value),
      config: data.config.map((item) => item.value),
      image: getNameFile(data.image),
    };
    dispatch(createProductAction.request(body, handleCreateProductSuccess));
  };

  const handleCreateProductSuccess = (): void => {
    showNotification(ETypeNotification.success, 'Tạo Mới Sản Phẩm Thành Công');
    navigate(`${LayoutPaths.Admin}${Paths.ProductsManagement}`);
  };
  return (
    <div className="AddProduct">
      <PageHeader title="Tạo Mới Sản Phẩm" dataBreadcrumb={dataAddProductBreadcrumb} />
      <ModuleProductDetail
        type={ETypeModuleProductDetail.create}
        onSubmit={handleCreateProduct}
        submitLoading={loading}
      />
    </div>
  );
};

export default AddProduct;
