import { LayoutPaths, Paths } from 'pages/routers';

export const dataAddProductBreadcrumb = [
  { key: 'dashboard', link: `${LayoutPaths.Admin}${Paths.Home}`, content: 'Tổng Quan' },
  { key: 'products', link: `${LayoutPaths.Admin}${Paths.ProductsManagement}`, content: 'Sản Phẩm' },
  { key: 'add-product', link: `${LayoutPaths.Admin}${Paths.AddProduct}`, content: 'Tạo Mới' },
];
