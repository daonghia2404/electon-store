import React, { useEffect } from 'react';
import { Form, Card } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { useSelector } from 'react-redux';

import Breadcrumb from 'components/Breadcrumb';
import { dataBreadcrumbCategories, dataOptionsCategoriesSort } from 'pages/Categories/Categories.datas';
import ImageCategoryMacbook from 'assets/images/categories/image-categories-macbook.png';
import Section from 'components/Section';

import Select from 'components/Select';
import { dataPricesFilter, dataStoragesFilter, dataTagsFilter } from 'common/static';
import BoxProduct from 'components/BoxProduct';
import Pagination from 'components/Pagination';
import FilterGroupWithCheckbox from 'containers/FilterGroupWithCheckbox';
import FilterGroupColor from 'containers/FilterGroupColor';
import { TRootState } from 'redux/reducers';
import './Categories.scss';
import { Paths } from 'pages/routers';

export const Categories: React.FC = () => {
  const [form] = useForm();
  const categoriesState = useSelector((state: TRootState) => state.categoriesState.categories);
  const productsState = useSelector((state: TRootState) => state.productsState.products);

  useEffect(() => {
    form.setFieldsValue({
      sort: { label: 'Sản Phẩm Bán Chạy', value: 'best-selling' },
    });
  }, [form]);

  return (
    <div className="Categories">
      <Breadcrumb data={dataBreadcrumbCategories} />

      <Section>
        <div className="Categories-wrapper flex">
          <div className="Categories-filter">
            <FilterGroupWithCheckbox
              title="Danh Mục"
              data={categoriesState.map((item) => ({ key: item._id, label: item.title }))}
            />
            <FilterGroupColor />
            <FilterGroupWithCheckbox title="Lọc Theo Giá" data={dataPricesFilter} />
            <FilterGroupWithCheckbox title="Lọc Theo Dung Lượng" data={dataStoragesFilter} />
            <FilterGroupWithCheckbox title="Lọc Theo Nhãn" data={dataTagsFilter} />
          </div>
          <div className="Categories-view">
            <Card className="Categories-overview" title="MACBOOK">
              <div className="Categories-overview-body flex">
                <div className="Categories-overview-image">
                  <img src={ImageCategoryMacbook} alt="" />
                </div>
                <div className="Categories-overview-info">
                  MacBook thực chất là dòng sản phẩm máy tính xách tay của thương hiệu nổi tiếng Apple. Với thiết kế vô
                  cùng đẹp mắt cùng hệ điều hành thân thiện, hiệu năng tốt đã giúp MacBook giành được thị phần của hệ
                  điều hành MacOS.
                </div>
              </div>
            </Card>

            <Form form={form} className="Categories-sort flex items-center justify-end">
              <div className="Categories-sort-label">Sắp Xếp</div>

              <Form.Item name="sort">
                <Select
                  options={dataOptionsCategoriesSort}
                  defaultValue={{ label: 'Sản Phẩm Bán Chạy', value: 'best-selling' }}
                />
              </Form.Item>
            </Form>

            <div className="Categories-products-view flex flex-wrap">
              {productsState.map((product) => (
                <div className="Categories-products-view-item">
                  <BoxProduct {...product} link={Paths.Product(product._id)} style={{ margin: 10 }} />
                </div>
              ))}
            </div>

            <div className="Categories-pagination flex items-center justify-between">
              Hiển thị 1 - 12 của 12 kết quả
              <Pagination page={1} pageSize={10} total={15} />
            </div>
          </div>
        </div>
      </Section>
    </div>
  );
};

export default Categories;
