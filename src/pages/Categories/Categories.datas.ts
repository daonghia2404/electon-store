import { Paths } from 'pages/routers';

export const dataBreadcrumbCategories = [
  { key: 'home', link: Paths.Home, content: 'Trang Chủ' },
  { key: 'categories', link: Paths.Categories(), content: 'Danh Mục' },
];

export const dataOptionsCategoriesSort = [
  { label: 'Sản Phẩm Bán Chạy', value: 'best-selling' },
  { label: 'Sản Phẩm Nổi Bật', value: 'featured' },
  { label: 'Sắp Xếp Từ A-Z', value: 'a-z' },
  { label: 'Sắp Xếp Từ Z-A', value: 'z-a' },
  { label: 'Sắp Xếp Giá Tăng Dần', value: 'low-to-high' },
  { label: 'Sắp Xếp Giá Giảm Dần', value: 'high-to-low' },
  { label: 'Mới Nhất', value: 'old-to-new' },
  { label: 'Cũ Nhất', value: 'new-to-old' },
];
