import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import PageHeader from 'components/PageHeader';
import ModuleProductDetail from 'containers/ModuleProductDetail/ModuleProductDetail';
import { ETypeModuleProductDetail } from 'containers/ModuleProductDetail/ModuleProductDetail.enums';
import { getNameFile, showNotification } from 'utils/function';
import { ETypeNotification } from 'common/enums';
import { navigate, useParams } from '@reach/router';
import { LayoutPaths, Paths } from 'pages/routers';
import { updateProductAction } from 'redux/actions';
import { TRootState } from 'redux/reducers';
import { EProductAction } from 'redux/actions/products/constants';
import { TModuleProductDetailForm } from 'containers/ModuleProductDetail/ModuleProductDetail.types';

import './UpdateProduct.scss';

export const UpdateProduct: React.FC = () => {
  const dispatch = useDispatch();
  const loading = useSelector((state: TRootState) => state.loading[EProductAction.UPDATE_PRODUCT]);
  const { productId } = useParams();

  const dataUpdateProductBreadcrumb = [
    { key: 'dashboard', link: `${LayoutPaths.Admin}${Paths.Home}`, content: 'Tổng Quan' },
    { key: 'products', link: `${LayoutPaths.Admin}${Paths.ProductsManagement}`, content: 'Sản Phẩm' },
    { key: 'update-product', link: `${LayoutPaths.Admin}${Paths.UpdateProduct(productId)}`, content: 'Cập Nhật' },
  ];

  const handleUpdateProduct = (data: TModuleProductDetailForm): void => {
    const body = {
      ...data,
      category: data.category.value,
      color: data.color.map((item) => item.value),
      config: data.config.map((item) => item.value),
      image: getNameFile(data.image),
    };
    dispatch(updateProductAction.request(productId, body, handleUpdateProductSuccess));
  };

  const handleUpdateProductSuccess = (): void => {
    showNotification(ETypeNotification.success, 'Cập Nhật Sản Phẩm Thành Công');
    navigate(`${LayoutPaths.Admin}${Paths.ProductsManagement}`);
  };
  return (
    <div className="UpdateProduct">
      <PageHeader title="Cập Nhật Sản Phẩm" dataBreadcrumb={dataUpdateProductBreadcrumb} />
      <ModuleProductDetail
        id={productId}
        type={ETypeModuleProductDetail.update}
        onSubmit={handleUpdateProduct}
        submitLoading={loading}
      />
    </div>
  );
};

export default UpdateProduct;
