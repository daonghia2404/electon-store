import React from 'react';
import AuthForm from 'containers/AuthForm';

export const AdminLogin: React.FC = () => {
  return (
    <div className="AdminLogin">
      <AuthForm />
    </div>
  );
};

export default AdminLogin;
