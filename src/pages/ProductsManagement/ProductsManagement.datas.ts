import { LayoutPaths, Paths } from 'pages/routers';

export const dataProductsManagementBreadcrumb = [
  { key: 'dashboard', link: `${LayoutPaths.Admin}${Paths.Home}`, content: 'Tổng Quan' },
  { key: 'products', link: `${LayoutPaths.Admin}${Paths.ProductsManagement}`, content: 'Sản Phẩm' },
];
