import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import PageHeader from 'components/PageHeader';
import Table from 'components/Table';
import { dataProductsManagementBreadcrumb } from 'pages/ProductsManagement/ProductsManagement.datas';
import { DEFAULT_PAGE, DEFAULT_PAGE_SIZE, emptyTableData } from 'common/static';
import { formatISODateToDateTime, formatNumberWithCommas, getUrlImage, showNotification } from 'utils/function';
import CircleColor from 'components/CircleColor';
import { EProductColor, ETypeNotification } from 'common/enums';
import Button from 'components/Button';
import { EIconColor, EIconName } from 'components/Icon';
import { LayoutPaths, Paths } from 'pages/routers';
import { deleteProductAction, getAllProductAction } from 'redux/actions';
import { TRootState } from 'redux/reducers';
import { TPaginateParams } from 'common/types';
import { TProductResponse } from 'services/products/types';
import ImageProductDefault from 'assets/images/image-product-default.png';
import { Link } from '@reach/router';
import ModalConfirm from 'containers/ModalConfirm';
import { EProductAction } from 'redux/actions/products/constants';

import './ProductsManagement.scss';

export const ProductsManagement: React.FC = () => {
  const dispatch = useDispatch();

  const productsState = useSelector((state: TRootState) => state.productsState.products);
  const productsTotalState = useSelector((state: TRootState) => state.productsState.total);
  const deleteLoading = useSelector((state: TRootState) => state.loading[EProductAction.DELETE_PRODUCT]);
  const productLoading = useSelector((state: TRootState) => state.loading[EProductAction.GET_ALL_PRODUCT]);

  const [paramsRequest] = useState<TPaginateParams>({
    page: DEFAULT_PAGE,
    pageSize: DEFAULT_PAGE_SIZE,
  });
  const [stateDeleteProductModal, setStateDeleteProductModal] = useState<{ visible: boolean; id: string }>({
    visible: false,
    id: '',
  });

  const dataSources = productsState;

  const columns = [
    {
      key: 'image',
      dataIndex: 'image',
      title: 'Hình Ảnh',
      render: (_: string, record: TProductResponse): React.ReactElement => (
        <div className="Table-image">
          {record.image ? <img src={getUrlImage(record.image)} alt="" /> : <img src={ImageProductDefault} alt="" />}
        </div>
      ),
    },
    {
      key: 'title',
      dataIndex: 'title',
      title: 'Tên Sản Phẩm',
      className: 'nowrap',
      render: (value: string, record: TProductResponse): React.ReactElement => (
        <Link className="Table-link" to={`${LayoutPaths.Admin}${Paths.UpdateProduct(record._id)}`}>
          {value}
        </Link>
      ),
    },
    {
      key: 'cat_id',
      dataIndex: 'cat_id',
      title: 'Danh Mục',
      className: 'nowrap',
      render: (_: string, record: TProductResponse): string => record?.cat_id?.title || '',
    },
    {
      key: 'color_id',
      dataIndex: 'color_id',
      title: 'Màu Sắc',
      className: 'nowrap',
      render: (_: string, record: TProductResponse): React.ReactElement => (
        <div className="Table-colors flex">
          {record?.color_id?.map((item) => (
            <CircleColor color={item.name as EProductColor} />
          ))}
        </div>
      ),
    },
    {
      key: 'config_id',
      dataIndex: 'config_id',
      title: 'Cấu Hình',
      render: (_: string, record: TProductResponse): React.ReactElement => (
        <div className="Table-tags flex">
          {record?.config_id?.map((item) => (
            <div className="Table-tag-item style-tag">{item.name}</div>
          ))}
        </div>
      ),
    },
    {
      key: 'price',
      dataIndex: 'price',
      title: 'Giá Sản Phẩm',
      className: 'nowrap',
      render: (value: string): string => `${formatNumberWithCommas(Number(value))}đ`,
    },
    {
      key: 'sale',
      dataIndex: 'sale',
      title: 'Giảm Giá',
      className: 'nowrap',
      render: (value: string): React.ReactElement => (
        <>
          {Number(value) > 0 ? (
            <div className="Table-tags flex">
              <div className="Table-tag-item style-tag">-{value}%</div>
            </div>
          ) : (
            <>{emptyTableData}</>
          )}
        </>
      ),
    },
    {
      key: 'stock',
      dataIndex: 'stock',
      title: 'Tồn Kho',
      className: 'nowrap',
    },
    {
      key: 'createdAt',
      dataIndex: 'createdAt',
      title: 'Ngày Tạo',
      className: 'nowrap',
      render: (value: string): string => formatISODateToDateTime(value),
    },
    {
      key: 'actions',
      dataIndex: 'actions',
      title: '',
      fixed: 'right',
      render: (_: string, record: TProductResponse): React.ReactElement => (
        <div style={{ width: 85 }} className="Table-actions flex items-center">
          <Button
            link={`${LayoutPaths.Admin}${Paths.UpdateProduct(record._id)}`}
            iconName={EIconName.edit}
            iconColor={EIconColor.white}
            className="secondary"
          />
          <Button
            iconName={EIconName.trash}
            iconColor={EIconColor.white}
            className="secondary"
            onClick={(): void => handleOpenDeleteProductModal(record._id)}
          />
        </div>
      ),
    },
  ];

  const handleOpenDeleteProductModal = (id: string): void => {
    setStateDeleteProductModal({
      visible: true,
      id,
    });
  };

  const handleCloseDeleteProductModal = (): void => {
    setStateDeleteProductModal({
      visible: false,
      id: '',
    });
  };

  const handleDeleteProduct = (): void => {
    dispatch(deleteProductAction.request(stateDeleteProductModal.id, handleDeleteProductSuccess));
  };

  const handleDeleteProductSuccess = (): void => {
    showNotification(ETypeNotification.success, 'Xoá Sản Phẩm Thành Công');
    handleCloseDeleteProductModal();
    getDataProducts();
  };

  const getDataProducts = (): void => {
    dispatch(getAllProductAction.request());
  };

  useEffect(() => {
    getDataProducts();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="ProductsManagement">
      <PageHeader title="Quản Lý Sản Phẩm" dataBreadcrumb={dataProductsManagementBreadcrumb}>
        <div className="ProductsManagement-cta flex">
          <Button
            title="Thêm Mới"
            link={`${LayoutPaths.Admin}${Paths.AddProduct}`}
            iconName={EIconName.plusCircle}
            iconColor={EIconColor.white}
            type="primary"
            reverse
          />
        </div>
      </PageHeader>

      <Table
        className="ProductsManagement-table"
        page={paramsRequest.page}
        pageSize={paramsRequest.pageSize}
        total={productsTotalState}
        rowKey="_id"
        dataSources={dataSources}
        columns={columns}
        loading={productLoading}
      />

      <ModalConfirm
        visible={stateDeleteProductModal.visible}
        title="Xoá Sản Phẩm"
        description="Bạn có chắc muốn xoá sản phẩm này. Dữ liệu đã xoá sẽ không thể khôi phục lại?"
        loading={deleteLoading}
        onClose={handleCloseDeleteProductModal}
        onConfirm={handleDeleteProduct}
      />
    </div>
  );
};

export default ProductsManagement;
