import React, { useEffect } from 'react';
import { Form } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'antd/lib/form/Form';

import { dataBreadcrumbCheckout } from 'pages/Checkout/Checkout.datas';
import Breadcrumb from 'components/Breadcrumb';
import Input from 'components/Input';
import TextArea from 'components/TextArea';
import Button from 'components/Button';
import Select from 'components/Select';
import { TRootState } from 'redux/reducers';
import { dataCityOptions, dataCountryOptions, dataTextColor, dataUnitShippingOptions } from 'common/static';
import { formatNumberWithCommas, getUrlImage, showNotification, validationRules } from 'utils/function';
import { EProductColor, ETypeNotification } from 'common/enums';
import { createOrderAction, uiAction } from 'redux/actions';
import { Paths } from 'pages/routers';

import './Checkout.scss';
import { navigate } from '@reach/router';
import { EOrderAction } from 'redux/actions/orders/constants';

export const Checkout: React.FC = () => {
  const dispatch = useDispatch();
  const [form] = useForm();
  const checkoutState = useSelector((state: TRootState) => state.ui.checkout);
  const checkoutLoading = useSelector((state: TRootState) => state.loading[EOrderAction.CREATE_ORDER]);

  const calculateDiscountPrice = (price: number, sale: number): number => {
    return price - price * (sale / 100);
  };

  const calculateTotalPrice = (): number => {
    return checkoutState
      .map((product) => calculateDiscountPrice(product.price, product.sale) * product.quantity)
      .reduce((a, b) => a + b, 0);
  };

  const handleSubmitCheckout = (values: any): void => {
    const body = {
      address: values.address,
      message: values.description || '',
      firstName: values.firstName,
      lastName: values.lastName,
      phoneNumber: values.phoneNumber,
      city: values.city.label,
      country: values.country.label,
      unitShipping: values.unitShipping.label,
      products: checkoutState.map((item) => ({
        _id: item._id,
        title: item.title,
        price: item.price,
        color_id: item.color_id,
        config_id: item.config_id,
        quatity: item.quantity,
        sale: item.sale,
        image: item.image,
      })),
    };
    dispatch(createOrderAction.request(body, handleSubmitCheckoutSuccess));
  };

  const handleSubmitCheckoutSuccess = (): void => {
    showNotification(ETypeNotification.success, 'Tạo Mới Đơn Hàng Thành Công');
    navigate(Paths.Home);
  };

  useEffect(() => {
    return (): void => {
      dispatch(uiAction.setCheckout([]));
    };
  }, [dispatch]);

  return (
    <div className="Checkout">
      <Breadcrumb data={dataBreadcrumbCheckout} />
      <div className="Checkout-main">
        <div className="container">
          <div className="Checkout-wrapper flex">
            <div className="Checkout-col">
              <h1 className="Checkout-title">Electon Store</h1>
              <h6 className="Checkout-subtitle">Thông Tin Vận Chuyển</h6>
              <Form
                form={form}
                className="Checkout-form flex flex-wrap justify-between"
                onFinish={handleSubmitCheckout}
              >
                <Form.Item className="Checkout-form-item half" name="firstName" rules={[validationRules.required()]}>
                  <Input placeholder="Họ" />
                </Form.Item>
                <Form.Item className="Checkout-form-item half" name="lastName" rules={[validationRules.required()]}>
                  <Input placeholder="Tên" />
                </Form.Item>
                <Form.Item className="Checkout-form-item" name="address" rules={[validationRules.required()]}>
                  <Input placeholder="Địa Chỉ" />
                </Form.Item>
                <Form.Item className="Checkout-form-item" name="phoneNumber" rules={[validationRules.required()]}>
                  <Input placeholder="Số Điện Thoại" />
                </Form.Item>
                <Form.Item className="Checkout-form-item" name="description">
                  <TextArea placeholder="Lời Nhắn" />
                </Form.Item>
                <Form.Item className="Checkout-form-item half" name="city" rules={[validationRules.required()]}>
                  <Select placeholder="Thành Phố" options={dataCityOptions} />
                </Form.Item>
                <Form.Item className="Checkout-form-item half" name="country" rules={[validationRules.required()]}>
                  <Select placeholder="Quốc Tịch" options={dataCountryOptions} />
                </Form.Item>
                <Form.Item className="Checkout-form-item" name="unitShipping" rules={[validationRules.required()]}>
                  <Select placeholder="Đơn Vị Vận Chuyển" options={dataUnitShippingOptions} />
                </Form.Item>
                <Form.Item className="Checkout-form-item">
                  <div className="Checkout-form-item-btns flex">
                    <Button size="large" type="primary" title="Đặt Hàng" htmlType="submit" loading={checkoutLoading} />
                    <Button size="large" className="secondary" title="Quay Lại Giỏ Hàng" link={Paths.Cart} />
                  </div>
                </Form.Item>
              </Form>
              <div className="Checkout-reserved">Bản Quyền Sở Hữu Electon Store</div>
            </div>
            <div className="Checkout-col">
              <div className="Checkout-products">
                {checkoutState.map((product) => (
                  <div className="Checkout-products-item flex items-center justify-between">
                    <div className="Checkout-products-item-info flex items-center">
                      <div className="Checkout-products-item-info-image">
                        <div className="Checkout-products-item-info-image-quantity">{product.quantity}</div>
                        <img src={getUrlImage(product.image)} alt="" />
                      </div>

                      <div className="Checkout-products-item-info-content">
                        <h5 className="Checkout-products-item-info-content-title flex items-center">
                          {product.title}
                          {Boolean(product.sale) && (
                            <div className="Checkout-products-item-info-content-title-sale">-{product.sale}%</div>
                          )}
                        </h5>
                        <p className="Checkout-products-item-info-content-des">
                          {product.config_id} / Màu {dataTextColor[product.color_id as EProductColor]}
                        </p>
                      </div>
                    </div>

                    <div className="Checkout-products-item-price">
                      {product.sale ? (
                        <>
                          <del>{formatNumberWithCommas(product.price)}đ</del>{' '}
                          {formatNumberWithCommas(calculateDiscountPrice(product.price, product.sale))}đ
                        </>
                      ) : (
                        <>{formatNumberWithCommas(product.price)}đ</>
                      )}
                    </div>
                  </div>
                ))}
              </div>

              <div className="Checkout-price-info">
                <div className="Checkout-price-info-item flex justify-between items-center">
                  <div className="Checkout-price-info-title">Tổng Phụ</div>
                  <div className="Checkout-price-info-des">{formatNumberWithCommas(calculateTotalPrice())}đ</div>
                </div>
                {/* <div className="Checkout-price-info-item flex justify-between items-center">
                  <div className="Checkout-price-info-title">Phí Vận Chuyển</div>
                  <div className="Checkout-price-info-des">79.000đ</div>
                </div> */}
                <div className="Checkout-price-info-item flex justify-between items-center">
                  <div className="Checkout-price-info-title">Voucher</div>
                  <div className="Checkout-price-info-des">0%</div>
                </div>
              </div>

              <div className="Checkout-price-info total">
                <div className="Checkout-price-info-item flex justify-between items-center">
                  <div className="Checkout-price-info-title">Tổng</div>
                  <div className="Checkout-price-info-des">{formatNumberWithCommas(calculateTotalPrice())}đ</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Checkout;
