import { Paths } from 'pages/routers';

export const dataBreadcrumbCheckout = [
  { key: 'home', link: Paths.Home, content: 'Trang Chủ' },
  { key: 'categories', link: Paths.Checkout, content: 'Thanh Toán' },
];
