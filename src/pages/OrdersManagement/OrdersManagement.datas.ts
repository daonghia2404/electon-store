import { LayoutPaths, Paths } from 'pages/routers';

export const dataOrdersManagementBreadcrumb = [
  { key: 'dashboard', link: `${LayoutPaths.Admin}${Paths.Home}`, content: 'Tổng Quan' },
  { key: 'products', link: `${LayoutPaths.Admin}${Paths.OrdersManagement}`, content: 'Đơn Hàng' },
];
