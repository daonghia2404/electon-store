import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import PageHeader from 'components/PageHeader';
import Table from 'components/Table';
import { dataOrdersManagementBreadcrumb } from 'pages/OrdersManagement/OrdersManagement.datas';
import { dataColorOrderStatus, dataOrderStatus, DEFAULT_PAGE, DEFAULT_PAGE_SIZE } from 'common/static';
import { formatISODateToDateTime, showNotification } from 'utils/function';
import { ETypeNotification } from 'common/enums';
import Button from 'components/Button';
import { EIconColor, EIconName } from 'components/Icon';
import { LayoutPaths, Paths } from 'pages/routers';
import { deleteOrderAction, getAllOrderAction } from 'redux/actions';
import { TRootState } from 'redux/reducers';
import { TPaginateParams } from 'common/types';
import ModalConfirm from 'containers/ModalConfirm';
import { EOrderAction } from 'redux/actions/orders/constants';
import { EOrderStatus } from 'services/orders/enums';
import { TOrderResponse } from 'services/orders/types';
import { Link } from '@reach/router';

import './OrdersManagement.scss';

export const OrdersManagement: React.FC = () => {
  const dispatch = useDispatch();

  const ordersState = useSelector((state: TRootState) => state.ordersState.orders);
  const ordersTotalState = useSelector((state: TRootState) => state.ordersState.total);
  const deleteLoading = useSelector((state: TRootState) => state.loading[EOrderAction.DELETE_ORDER]);
  const ordersLoading = useSelector((state: TRootState) => state.loading[EOrderAction.GET_ALL_ORDER]);

  const [paramsRequest] = useState<TPaginateParams>({
    page: DEFAULT_PAGE,
    pageSize: DEFAULT_PAGE_SIZE,
  });
  const [stateDeleteOrderModal, setStateDeleteOrderModal] = useState<{ visible: boolean; id: string }>({
    visible: false,
    id: '',
  });

  const dataSources = ordersState;

  const columns = [
    {
      key: '_id',
      dataIndex: '_id',
      title: 'Mã Đơn Hàng',
      className: 'nowrap',
      render: (value: string, record: TOrderResponse): React.ReactElement => (
        <Link className="Table-link" to={`${LayoutPaths.Admin}${Paths.UpdateOrder(record._id)}`}>
          {value}
        </Link>
      ),
    },
    {
      key: 'status',
      dataIndex: 'status',
      title: 'Trạng Thái',
      className: 'nowrap',
      render: (value: string): React.ReactElement => (
        <div className="Table-tags flex">
          <div className="Table-tag-item style-tag" style={{ background: dataColorOrderStatus[value as EOrderStatus] }}>
            {dataOrderStatus[value as EOrderStatus]}
          </div>
        </div>
      ),
    },
    {
      key: 'firstName',
      dataIndex: 'firstName',
      title: 'Họ',
      className: 'nowrap',
    },
    {
      key: 'lastName',
      dataIndex: 'lastName',
      title: 'Tên',
      className: 'nowrap',
    },
    {
      key: 'address',
      dataIndex: 'address',
      title: 'Địa Chỉ',
      className: 'nowrap',
    },
    {
      key: 'city',
      dataIndex: 'city',
      title: 'Thành Phố',
      className: 'nowrap',
    },
    {
      key: 'country',
      dataIndex: 'country',
      title: 'Quốc Tịch',
      className: 'nowrap',
    },
    {
      key: 'phoneNumber',
      dataIndex: 'phoneNumber',
      title: 'Số Điện Thoại',
      className: 'nowrap',
    },
    {
      key: 'createdAt',
      dataIndex: 'createdAt',
      title: 'Ngày Đặt',
      className: 'nowrap',
      render: (value: string): string => formatISODateToDateTime(value),
    },
    {
      key: 'actions',
      dataIndex: 'actions',
      title: '',
      fixed: 'right',
      render: (_: string, record: TOrderResponse): React.ReactElement => (
        <div style={{ width: 40 }} className="Table-actions flex items-center">
          <Button
            iconName={EIconName.trash}
            iconColor={EIconColor.white}
            className="secondary"
            onClick={(): void => handleOpenDeleteOrderModal(record._id)}
          />
        </div>
      ),
    },
  ];

  const handleOpenDeleteOrderModal = (id: string): void => {
    setStateDeleteOrderModal({
      visible: true,
      id,
    });
  };

  const handleCloseDeleteOrderModal = (): void => {
    setStateDeleteOrderModal({
      visible: false,
      id: '',
    });
  };

  const handleDeleteProduct = (): void => {
    dispatch(deleteOrderAction.request(stateDeleteOrderModal.id, handleDeleteOrderSuccess));
  };

  const handleDeleteOrderSuccess = (): void => {
    showNotification(ETypeNotification.success, 'Xoá Đơn Hàng Thành Công');
    handleCloseDeleteOrderModal();
    getDataOrders();
  };

  const getDataOrders = (): void => {
    dispatch(getAllOrderAction.request());
  };

  useEffect(() => {
    getDataOrders();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="OrdersManagement">
      <PageHeader title="Quản Lý Sản Phẩm" dataBreadcrumb={dataOrdersManagementBreadcrumb}>
        <div className="OrdersManagement-cta flex">
          <Button
            title="Thêm Mới"
            link={`${LayoutPaths.Admin}${Paths.AddProduct}`}
            iconName={EIconName.plusCircle}
            iconColor={EIconColor.white}
            type="primary"
            reverse
          />
        </div>
      </PageHeader>

      <Table
        className="OrdersManagement-table"
        page={paramsRequest.page}
        pageSize={paramsRequest.pageSize}
        total={ordersTotalState}
        rowKey="_id"
        dataSources={dataSources}
        columns={columns}
        loading={ordersLoading}
      />

      <ModalConfirm
        visible={stateDeleteOrderModal.visible}
        title="Xoá Đơn Hàng"
        description="Bạn có chắc muốn xoá sản phẩm này. Dữ liệu đã xoá sẽ không thể khôi phục lại?"
        loading={deleteLoading}
        onClose={handleCloseDeleteOrderModal}
        onConfirm={handleDeleteProduct}
      />
    </div>
  );
};

export default OrdersManagement;
