import React, { useEffect, useState } from 'react';

import PageHeader from 'components/PageHeader';
import { navigate, useParams } from '@reach/router';
import { LayoutPaths, Paths } from 'pages/routers';
import { useDispatch, useSelector } from 'react-redux';
import { TRootState } from 'redux/reducers';
import { EOrderAction } from 'redux/actions/orders/constants';
import Loading from 'components/Loading';
import { getOneOrderAction, updateOrderAction } from 'redux/actions';
import { formatISODateToDateTime, formatNumberWithCommas, getUrlImage, showNotification } from 'utils/function';
import { EProductColor, ETypeNotification } from 'common/enums';
import { dataColorOrderStatus, dataOrderStatus, dataTextColor } from 'common/static';
import DefaultProductImage from 'assets/images/image-product-default.png';

import './UpdateOrder.scss';
import Button from 'components/Button';
import { EOrderStatus } from 'services/orders/enums';
import ModalConfirm from 'containers/ModalConfirm';

export const UpdateOrder: React.FC = () => {
  const { orderId } = useParams();
  const dispatch = useDispatch();

  const [visibleModalConfirm, setVisibleModalConfirm] = useState<boolean>(false);

  const orderState = useSelector((state: TRootState) => state.ordersState.order);
  const orderLoading = useSelector((state: TRootState) => state.loading[EOrderAction.GET_ONE_ORDER]);
  const updateOrderLoading = useSelector((state: TRootState) => state.loading[EOrderAction.UPDATE_ORDER]);

  const calculateDiscountPrice = (price: number, sale: number): number => {
    return price - price * (sale / 100);
  };

  const dataUpdateOrderBreadcrumb = [
    { key: 'dashboard', link: `${LayoutPaths.Admin}${Paths.Home}`, content: 'Tổng Quan' },
    { key: 'orders', link: `${LayoutPaths.Admin}${Paths.OrdersManagement}`, content: 'Đơn Hàng' },
    { key: 'update-order', link: `${LayoutPaths.Admin}${Paths.UpdateOrder(orderId)}`, content: 'Thông Tin' },
  ];

  const calculateTotalPrice = (): number => {
    return orderState.products
      .map((product) => calculateDiscountPrice(product.price, product.sale) * product.quatity)
      .reduce((a, b) => a + b, 0);
  };

  const renderTitleBtnSubmit = (): string => {
    const currentStatus = orderState.status;
    const isPendingStep = currentStatus === EOrderStatus.PENDING;
    const isInprocessStep = currentStatus === EOrderStatus.INPROCESS;

    if (isPendingStep) return 'Tiến Hành Vận Chuyển';
    if (isInprocessStep) return 'Thanh Toán Thành Công';
    return '';
  };

  const handleUpdateStatusOrder = (): void => {
    const currentStatus = orderState.status;
    const isPendingStep = currentStatus === EOrderStatus.PENDING;
    const isInprocessStep = currentStatus === EOrderStatus.INPROCESS;

    if (isPendingStep) {
      dispatch(updateOrderAction.request(orderId, { status: EOrderStatus.INPROCESS }, handleUpdateStatusOrderSuccess));
    }
    if (isInprocessStep) {
      dispatch(updateOrderAction.request(orderId, { status: EOrderStatus.SUCCESS }, handleUpdateStatusOrderSuccess));
    }
  };

  const handleUpdateStatusOrderSuccess = (): void => {
    showNotification(ETypeNotification.success, 'Chuyển Trạng Thái Đơn Hàng Thành Công');
    navigate(`${LayoutPaths.Admin}${Paths.OrdersManagement}`);
  };

  const handleToggleModalConfirm = (): void => {
    setVisibleModalConfirm(!visibleModalConfirm);
  };

  useEffect(() => {
    dispatch(getOneOrderAction.request(orderId));
  }, [dispatch, orderId]);

  return (
    <div className="UpdateOrder">
      <ModalConfirm
        visible={visibleModalConfirm}
        title="Chuyển Trạng Thái"
        description="Bạn có chắc muốn chuyển trạng thái đơn hàng này? Trạng thái đã chuyển sẽ không thể khôi phục lại"
        loading={updateOrderLoading}
        onClose={handleToggleModalConfirm}
        onConfirm={handleUpdateStatusOrder}
      />

      <PageHeader title="Thông Tin Đơn Hàng" dataBreadcrumb={dataUpdateOrderBreadcrumb} />

      <div className="UpdateOrder-body">
        {orderLoading ? (
          <div className="UpdateOrder-loading flex items-center justify-center">
            <Loading />
          </div>
        ) : (
          <div className="UpdateOrder-main">
            <div className="UpdateOrder-info flex flex-wrap">
              <div className="UpdateOrder-content">
                <div className="UpdateOrder-content-title">Họ Tên:</div>
                <div className="UpdateOrder-content-description">
                  {orderState.lastName} {orderState.firstName}
                </div>
              </div>
              <div className="UpdateOrder-content">
                <div className="UpdateOrder-content-title">Số Điện Thoại:</div>
                <div className="UpdateOrder-content-description">{orderState.phoneNumber}</div>
              </div>
              <div className="UpdateOrder-content">
                <div className="UpdateOrder-content-title">Địa Chỉ:</div>
                <div className="UpdateOrder-content-description">{orderState.address}</div>
              </div>
              <div className="UpdateOrder-content">
                <div className="UpdateOrder-content-title">Thành Phố:</div>
                <div className="UpdateOrder-content-description">{orderState.city}</div>
              </div>
              <div className="UpdateOrder-content">
                <div className="UpdateOrder-content-title">Quốc Tịch:</div>
                <div className="UpdateOrder-content-description">{orderState.country}</div>
              </div>
              <div className="UpdateOrder-content">
                <div className="UpdateOrder-content-title">Đơn Vị Vận Chuyển:</div>
                <div className="UpdateOrder-content-description">{orderState.unitShipping}</div>
              </div>
              <div className="UpdateOrder-content">
                <div className="UpdateOrder-content-title">Ngày Tạo:</div>
                <div className="UpdateOrder-content-description">{formatISODateToDateTime(orderState.createdAt)}</div>
              </div>
              <div className="UpdateOrder-content">
                <div className="UpdateOrder-content-title">Trạng Thái:</div>
                <div className="UpdateOrder-content-description flex">
                  <div
                    className="Table-tag-item style-tag"
                    style={{ background: dataColorOrderStatus[orderState.status] }}
                  >
                    {dataOrderStatus[orderState.status]}
                  </div>
                </div>
              </div>
              <div className="UpdateOrder-content">
                <div className="UpdateOrder-content-title">Lời Nhắn:</div>
                <div className="UpdateOrder-content-description">{orderState.message}</div>
              </div>
            </div>

            <div className="UpdateOrder-products">
              {orderState.products.map((product) => (
                <div className="UpdateOrder-products-item flex items-center justify-between">
                  <div className="UpdateOrder-products-item-info flex items-center">
                    <div className="UpdateOrder-products-item-info-image">
                      <div className="UpdateOrder-products-item-info-image-quantity">{product.quatity}</div>
                      {product.image ? (
                        <img src={getUrlImage(product.image)} alt="" />
                      ) : (
                        <img src={DefaultProductImage} alt="" />
                      )}
                    </div>

                    <div className="UpdateOrder-products-item-info-content">
                      <h5 className="UpdateOrder-products-item-info-content-title flex items-center">
                        {product.title}
                        {Boolean(product.sale) && (
                          <div className="UpdateOrder-products-item-info-content-title-sale">-{product.sale}%</div>
                        )}
                      </h5>
                      <p className="UpdateOrder-products-item-info-content-des">
                        {product.config_id} / Màu {dataTextColor[product.color_id as EProductColor]}
                      </p>
                    </div>
                  </div>

                  <div className="UpdateOrder-products-item-price">
                    {product.sale ? (
                      <>
                        <del>{formatNumberWithCommas(product.price)}đ</del>{' '}
                        {formatNumberWithCommas(calculateDiscountPrice(product.price, product.sale))}đ
                      </>
                    ) : (
                      <>{formatNumberWithCommas(product.price)}đ</>
                    )}
                  </div>
                </div>
              ))}
            </div>

            <div className="UpdateOrder-total">
              <div className="UpdateOrder-price-info">
                <div className="UpdateOrder-price-info-item flex justify-between items-center">
                  <div className="UpdateOrder-price-info-title">Tổng Phụ</div>
                  <div className="UpdateOrder-price-info-des">{formatNumberWithCommas(calculateTotalPrice())}đ</div>
                </div>
                {/* <div className="UpdateOrder-price-info-item flex justify-between items-center">
                  <div className="UpdateOrder-price-info-title">Phí Vận Chuyển</div>
                  <div className="UpdateOrder-price-info-des">79.000đ</div>
                </div> */}
                <div className="UpdateOrder-price-info-item flex justify-between items-center">
                  <div className="UpdateOrder-price-info-title">Voucher</div>
                  <div className="UpdateOrder-price-info-des">0%</div>
                </div>
              </div>

              <div className="UpdateOrder-price-info total">
                <div className="UpdateOrder-price-info-item flex justify-between items-center">
                  <div className="UpdateOrder-price-info-title">Tổng</div>
                  <div className="UpdateOrder-price-info-des">{formatNumberWithCommas(calculateTotalPrice())}đ</div>
                </div>
              </div>
            </div>

            {orderState.status !== EOrderStatus.SUCCESS && (
              <div className="UpdateOrder-submit flex justify-end">
                <Button title={renderTitleBtnSubmit()} type="primary" size="large" onClick={handleToggleModalConfirm} />
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default UpdateOrder;
