import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { dataBreadcrumbCart } from 'pages/Cart/Cart.datas';
import Breadcrumb from 'components/Breadcrumb';
import InputNumber from 'components/InputNumber';
import Button from 'components/Button';
import Icon, { EIconColor, EIconName } from 'components/Icon';
import Checkbox from 'components/Checkbox';
import { TRootState } from 'redux/reducers';
import { uiAction } from 'redux/actions';
import { TCartProduct } from 'common/types';
import { formatNumberWithCommas, getUrlImage, renderTextColor } from 'utils/function';
import { Paths } from 'pages/routers';

import './Cart.scss';
import { navigate } from '@reach/router';

export const Cart: React.FC = () => {
  const dispatch = useDispatch();
  const cartState = useSelector((state: TRootState) => state.ui.cart);

  const [cartData, setCartData] = useState<Array<TCartProduct & { checked: boolean }>>([]);

  const isEmptyCart = cartState.length === 0;

  const calculateTotalPrice = (): number => {
    return cartData
      .filter((product) => product.checked)
      .map((product) => calculateDiscountPrice(product.price, product.sale) * product.quantity)
      .reduce((a, b) => a + b, 0);
  };

  const calculateDiscountPrice = (price: number, discount: number): number => {
    return price - price * (discount / 100);
  };

  const handleChangeQuantityCartProduct = (value: number, product: TCartProduct): void => {
    const newCartState = cartState.map((item) => {
      if (item.id === product._id) {
        return {
          ...item,
          quantity: value,
        };
      }

      return item;
    });

    const newCartData = cartData.map((item) => {
      if (item._id === product._id) {
        return {
          ...item,
          quantity: value,
        };
      }

      return item;
    });

    dispatch(uiAction.setCart(newCartState));
    setCartData(newCartData);
  };

  const handleRemoveProductCart = (product: TCartProduct & { cartProductId: string }): void => {
    const deletedCartState = cartState.filter((item) => item.cartProductId !== product.cartProductId);
    const deletedCartData = cartData.filter((item) => item.cartProductId !== product.cartProductId);

    dispatch(uiAction.setCart(deletedCartState));
    setCartData(deletedCartData);
  };

  const handleRemoveAllProductCart = (): void => {
    dispatch(uiAction.setCart([]));
    setCartData([]);
  };

  const handleChangeCheckboxProduct = (checked: boolean, product: TCartProduct & { checked: boolean }): void => {
    const data = cartData.map((item) => {
      if (item._id === product._id)
        return {
          ...item,
          checked,
        };

      return item;
    });
    setCartData(data);
  };

  const handleCheckAllProduct = (checked: boolean): void => {
    const data = cartData.map((item) => ({
      ...item,
      checked,
    }));

    setCartData(data);
  };

  const isAllCartChecked = (): boolean => {
    const isAllChecked = cartData.filter((product) => product.checked).length === cartState.length;
    return isAllChecked;
  };

  const isCheckedAtLeastOne = (): boolean => {
    const isChecked = cartData.filter((product) => product.checked).length > 0;
    return isChecked;
  };

  const handleCheckout = (): void => {
    dispatch(uiAction.setCheckout(cartData.filter((product) => product.checked)));
    navigate(Paths.Checkout);
  };

  useEffect(() => {
    const data = cartState.map((product) => ({ ...product, checked: true }));
    setCartData(data);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="Cart">
      <Breadcrumb data={dataBreadcrumbCart} />

      <div className="container">
        <div className="Cart-wrapper">
          {isEmptyCart ? (
            <div className="Cart-empty flex flex-col justify-center items-center">
              <Icon name={EIconName.shoppingCart} color={EIconColor.doveGray} />
              Không Có Sản Phẩm Trong Giỏ Hàng Của Bạn
              <Button className="secondary" title="Tiếp Tục Mua Sắm" link={`${Paths.Categories('id')}`} />
            </div>
          ) : (
            <>
              <div className="Cart-table">
                <table>
                  <thead>
                    <tr>
                      <td className="Cart-table-checkbox">
                        <Checkbox checked={isAllCartChecked()} onChange={handleCheckAllProduct} />
                      </td>
                      <td>Giỏ Hàng Của Tôi</td>
                      <td className="Cart-quantity">Số Lượng</td>
                      <td>Giá</td>
                      <td className="Cart-table-action" />
                    </tr>
                  </thead>
                  <tbody>
                    {cartData.map((product) => (
                      <tr>
                        <td className="Cart-table-checkbox">
                          <Checkbox
                            checked={product.checked}
                            onChange={(value): void => handleChangeCheckboxProduct(value, product)}
                          />
                        </td>
                        <td>
                          <div className="Cart-product flex items-center">
                            <div className="Cart-image">
                              <img src={getUrlImage(product.image)} alt="" />
                            </div>
                            <div className="Cart-info">
                              <h4 className="Cart-info-title flex items-center">
                                {product.title}
                                {Boolean(product.sale) && <div className="Cart-info-title-sale">-{product.sale}%</div>}
                              </h4>
                              {product.config_id && (
                                <p className="Cart-info-content">
                                  Cấu Hình: <span>{product.config_id}</span>
                                </p>
                              )}
                              <p className="Cart-info-content">
                                Màu Sắc: <span>{renderTextColor(product.color_id)}</span>
                              </p>
                              <p className="Cart-info-content">
                                Đơn Giá: <span>{formatNumberWithCommas(product.price)}đ</span>
                              </p>
                            </div>
                          </div>
                        </td>
                        <td className="Cart-quantity">
                          <div className="Cart-quantity-wrapper">
                            <InputNumber
                              min={1}
                              value={product.quantity}
                              onChange={(value): void => handleChangeQuantityCartProduct(value, product)}
                            />
                          </div>
                        </td>
                        <td>
                          <div className="Cart-price">
                            {product.sale ? (
                              <>
                                <del>{formatNumberWithCommas(product.price * product.quantity)}đ</del>
                                {formatNumberWithCommas(
                                  calculateDiscountPrice(product.price, product.sale) * product.quantity,
                                )}
                                đ
                              </>
                            ) : (
                              <>{formatNumberWithCommas(product.price * product.quantity)}đ</>
                            )}
                          </div>
                        </td>
                        <td className="Cart-table-action">
                          <div className="Cart-table-action-wrapper">
                            <Button
                              className="secondary"
                              iconName={EIconName.trash}
                              iconColor={EIconColor.white}
                              onClick={(): void => handleRemoveProductCart(product)}
                            />
                          </div>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
              <div className="Cart-table-total flex items-center justify-between">
                <div className="Cart-table-total-label">Tổng Cộng: </div>
                <div className="Cart-table-total-price">{formatNumberWithCommas(calculateTotalPrice())}đ</div>
              </div>
              <div className="Cart-table-footer flex justify-between">
                <div className="Cart-table-footer-col flex">
                  <Button className="secondary" title="Xoá Giỏ Hàng" onClick={handleRemoveAllProductCart} />
                  <Button className="secondary" title="Cập Nhật Giỏ Hàng" />
                </div>
                <div className="Cart-table-footer-col flex">
                  <Button type="primary" title="Tiếp Tục Mua Sắm" link={`${Paths.Categories('id')}`} />
                  <Button
                    type="primary"
                    title="Thanh Toán"
                    onClick={handleCheckout}
                    disabled={!isCheckedAtLeastOne()}
                  />
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Cart;
