import { Paths } from 'pages/routers';

export const dataBreadcrumbCart = [
  { key: 'home', link: Paths.Home, content: 'Trang Chủ' },
  { key: 'products', link: Paths.Cart, content: 'Giỏ Hàng' },
];
