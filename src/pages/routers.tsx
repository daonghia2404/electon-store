import React, { lazy, Suspense } from 'react';
import { Redirect, RouteComponentProps } from '@reach/router';

import helpers from 'services/helpers';
import Loading from 'components/Loading';

const retryLoadComponent = (fn: () => Promise<unknown>, retriesLeft = 5, interval = 1000): any =>
  new Promise((resolve, reject) => {
    fn()
      .then(resolve)
      .catch((error) => {
        setTimeout(() => {
          if (retriesLeft === 1) {
            reject(error);
            return;
          }

          retryLoadComponent(fn, retriesLeft - 1, interval).then(resolve, reject);
        }, interval);
      });
  });

const Home = lazy(() => retryLoadComponent(() => import('pages/Home')));
const Categories = lazy(() => retryLoadComponent(() => import('pages/Categories')));
const Product = lazy(() => retryLoadComponent(() => import('pages/Product')));
const Cart = lazy(() => retryLoadComponent(() => import('pages/Cart')));
const Checkout = lazy(() => retryLoadComponent(() => import('pages/Checkout')));
const AdminLogin = lazy(() => retryLoadComponent(() => import('pages/AdminLogin')));
const ProductsManagement = lazy(() => retryLoadComponent(() => import('pages/ProductsManagement')));
const AddProduct = lazy(() => retryLoadComponent(() => import('pages/AddProduct')));
const UpdateProduct = lazy(() => retryLoadComponent(() => import('pages/UpdateProduct')));
const OrdersManagement = lazy(() => retryLoadComponent(() => import('pages/OrdersManagement')));
const UpdateOrder = lazy(() => retryLoadComponent(() => import('pages/UpdateOrder')));

export const LayoutPaths = {
  Admin: '/admin',
  Auth: '/xac-thuc',
  Guest: '/',
};

export const ModulePaths = {
  Categories: '/danh-muc',
  Product: '/san-pham',
  ProductsManagement: '/quan-ly-san-pham',
  OrdersManagement: '/quan-ly-don-hang',
};

export const Paths = {
  Home: '/trang-chu',
  Categories: (categoryId?: string): string => `${ModulePaths.Categories}/${categoryId || ':categoryId'}`,
  Product: (productId?: string): string => `${ModulePaths.Product}/${productId || ':productId'}`,
  Cart: '/gio-hang',
  Checkout: '/thanh-toan',

  AdminLogin: '/dang-nhap',

  ProductsManagement: ModulePaths.ProductsManagement,
  AddProduct: `${ModulePaths.ProductsManagement}/tao-moi-san-pham`,
  UpdateProduct: (productId?: string): string => `${ModulePaths.ProductsManagement}/${productId || ':productId'}`,

  OrdersManagement: ModulePaths.OrdersManagement,
  UpdateOrder: (orderId?: string): string => `${ModulePaths.OrdersManagement}/${orderId || ':orderId'}`,

  Rest: '*',
};

export const Pages = {
  Home,
  Categories,
  Product,
  Cart,
  Checkout,
  AdminLogin,
  ProductsManagement,
  OrdersManagement,
  AddProduct,
  UpdateProduct,
  UpdateOrder,
};

interface IRouteProps extends RouteComponentProps {
  component: React.FC;
}

export const ProtectedRoute: React.FC<IRouteProps> = ({ component: Component, ...rest }) => {
  const loggedIn: string | any = helpers.getAccessToken();

  return loggedIn ? (
    <Suspense fallback={<Loading minHeight="calc(100vh - 82px)" />}>
      <Component {...rest} />
    </Suspense>
  ) : (
    <Redirect noThrow from={Paths.Rest} to={`${LayoutPaths.Auth}${Paths.AdminLogin}`} />
  );
};

export const PublicRoute: React.FC<IRouteProps> = ({ component: Component, ...rest }) => (
  <Suspense fallback={<Loading minHeight="calc(100vh - 146px)" />}>
    <Component {...rest} />
  </Suspense>
);
