import React from 'react';

import Banner from 'containers/Banner';
import SliderCategories from 'containers/SliderCategories';
import SaleOff from 'containers/SaleOff';
import Services from 'containers/Services';
import TabProducts from 'containers/TabProducts';
import SliderProducts from 'containers/SliderProducts';
import BannerSpecial from 'components/BannerSpecial';
import BlogEvent from 'containers/BlogEvent';
import Brands from 'containers/Brands';
import Subscribe from 'containers/Subscribe';

export const Home: React.FC = () => {
  return (
    <div className="Home">
      <Banner />
      <SliderCategories />
      <TabProducts />
      <SaleOff />
      <Services />
      <SliderProducts title="SẢN PHẨM ĐẶC BIỆT" />
      <BannerSpecial />
      <BlogEvent />
      <Brands />
      <Subscribe />
    </div>
  );
};

export default Home;
