import React, { useEffect, useState } from 'react';
import ReactImageMagnify from 'react-image-magnify';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';

import { Paths } from 'pages/routers';
import Breadcrumb from 'components/Breadcrumb';
import Icon, { EIconColor, EIconName } from 'components/Icon';
import PaymentAmericanExpress from 'assets/images/card-payment/image-card-american-express.png';
import PaymentDiscover from 'assets/images/card-payment/image-card-discover.png';
import PaymentMasterCard from 'assets/images/card-payment/image-card-master-card.png';
import PaymentPaypal from 'assets/images/card-payment/image-card-paypal.png';
import PaymentVisa from 'assets/images/card-payment/image-card-visa.png';
import Button from 'components/Button';
import { ETypeNotification } from 'common/enums';
import InputNumber from 'components/InputNumber';
import SliderProducts from 'containers/SliderProducts';
import Tabs from 'components/Tabs';
import { navigate, useParams } from '@reach/router';
import { formatNumberWithCommas, getUrlImage, showNotification } from 'utils/function';
import { TRootState } from 'redux/reducers';
import { getOneProductAction, uiAction } from 'redux/actions';

import './Product.scss';
import { EProductAction } from 'redux/actions/products/constants';
import Loading from 'components/Loading';
import { TSelectOption } from 'common/types';
import SelectConfig from 'containers/SelectConfig';
import SelectColor from 'containers/SelectColor';

export const Product: React.FC = () => {
  const { productId } = useParams();
  const dispatch = useDispatch();

  const productState = useSelector((state: TRootState) => state.productsState.product);
  const productLoading = useSelector((state: TRootState) => state.loading[EProductAction.GET_ONE_PRODUCT]);
  const cartState = useSelector((state: TRootState) => state.ui.cart);

  const [config, setConfig] = useState<TSelectOption | null>(null);
  const [color, setColor] = useState<TSelectOption | null>(null);
  const [quantity, setQuantity] = useState<number>(1);

  const dataBreadcrumbProduct = [
    { key: 'home', link: Paths.Home, content: 'Trang Chủ' },
    { key: 'products', link: Paths.Categories('id'), content: 'Sản Phẩm' },
    { key: 'productId', link: Paths.Product(productId), content: productState?.title },
  ];

  const reactImageMagnifyOptions = {
    smallImage: {
      alt: '',
      isFluidWidth: true,
      src: getUrlImage(productState.image),
    },
    largeImage: {
      src: getUrlImage(productState.image),
      width: 300 * 2,
      height: 350 * 2,
    },
    enlargedImageContainerDimensions: {
      width: '60%',
      height: '60%',
    },
  };

  const renderDescriptionProduct = (): React.ReactElement => {
    return <div className="Product-description style-content-editable">{productState.content}</div>;
  };

  const dataTabs = [
    {
      title: 'Mô Tả',
      key: 'description',
      content: renderDescriptionProduct(),
    },
    {
      title: 'Đánh Giá',
      key: 'review',
      content: renderDescriptionProduct(),
    },
    {
      title: 'Video',
      key: 'video',
      content: renderDescriptionProduct(),
    },
    {
      title: 'Bình Luận',
      key: 'comments',
      content: renderDescriptionProduct(),
    },
  ];

  const handleAddCart = (): void => {
    const isValidatedSuccess = config && color;
    if (isValidatedSuccess) {
      const product = {
        ...productState,
        cartProductId: uuidv4(),
        quantity,
        color_id: color?.label,
        config_id: config?.label,
      };

      dispatch(uiAction.setCart([...cartState, product]));
      showNotification(ETypeNotification.success, 'Thêm Vào Giỏ Hàng Thành Công');
    } else {
      showNotification(ETypeNotification.error, 'Vui Lòng Chọn Cấu Hình Và Màu Của Sản Phẩm');
    }
  };

  const handleCheckout = (): void => {
    const isValidatedSuccess = config && color;
    if (isValidatedSuccess) {
      const product = {
        ...productState,
        cartProductId: uuidv4(),
        quantity,
        color_id: color?.label,
        config_id: config?.label,
      };
      dispatch(uiAction.setCheckout([product]));
      navigate(Paths.Checkout);
    } else {
      showNotification(ETypeNotification.error, 'Vui Lòng Chọn Cấu Hình Và Màu Của Sản Phẩm');
    }
  };

  const handleChangeConfig = (value: TSelectOption): void => {
    setConfig(value);
  };

  const handleChangeColor = (value: TSelectOption): void => {
    setColor(value);
  };

  const handleChangeQuantity = (value: number): void => {
    setQuantity(value);
  };

  useEffect(() => {
    if (productId) dispatch(getOneProductAction.request(productId));
    else navigate(Paths.Home);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productId]);

  return (
    <div className="Product">
      <Breadcrumb data={dataBreadcrumbProduct} />
      {productLoading ? (
        <div className="Product-loading">
          <Loading />
        </div>
      ) : (
        <>
          <div className="container">
            <div className="Product-wrapper">
              <div className="Product-view flex items-start">
                <div className="Product-view-image">
                  <ReactImageMagnify {...reactImageMagnifyOptions} />
                </div>
                <div className="Product-view-info">
                  <h1 className="Product-view-info-title">{productState.title}</h1>
                  <div className="Product-view-info-review flex items-center">
                    {[...Array(5)].map(() => (
                      <div className="Product-view-info-review-star">
                        <Icon name={EIconName.star} color={EIconColor.buttercup} />
                      </div>
                    ))}
                    <div className="Product-view-info-review-write-review">Viết Đánh Giá</div>
                  </div>
                  <div className="Product-view-info-price flex items-center">
                    {productState.sale ? (
                      <>
                        <div className="Product-view-info-price-current">
                          {formatNumberWithCommas(productState.price - productState.price * (productState.sale / 100))}đ
                        </div>
                        <del className="Product-view-info-price-old">{formatNumberWithCommas(productState.price)}đ</del>
                        <div className="Product-view-info-price-sale">-{productState.sale}%</div>
                      </>
                    ) : (
                      <div className="Product-view-info-price-current">
                        {formatNumberWithCommas(productState.price)}đ
                      </div>
                    )}
                  </div>

                  <div className="Product-view-info-content">
                    <div className="Product-view-info-content-status flex items-center">
                      Tình Trạng:
                      {productState.stock > 0 ? (
                        <div
                          className="Product-view-info-content-status-label flex items-center"
                          style={{ color: EIconColor.eucalyptus }}
                        >
                          Còn Hàng <Icon name={EIconName.check} color={EIconColor.eucalyptus} />
                        </div>
                      ) : (
                        <div
                          className="Product-view-info-content-status-label flex items-center"
                          style={{ color: EIconColor.sangria }}
                        >
                          Hết Hàng <Icon name={EIconName.x} color={EIconColor.sangria} />
                        </div>
                      )}
                    </div>
                    <p>{productState.description}</p>
                  </div>

                  <div className="Product-view-info-options">
                    <div className="Product-view-info-options-config flex items-center flex-wrap">
                      <SelectConfig data={productState.config_id} value={config} onChange={handleChangeConfig} />
                    </div>

                    <div className="Product-view-info-options-config flex items-center flex-wrap">
                      <SelectColor data={productState.color_id} value={color} onChange={handleChangeColor} />
                    </div>

                    <div className="Product-view-info-options-config flex items-center flex-wrap">
                      <div className="Product-view-info-options-config-label">Số Lượng: </div>
                      <InputNumber min={1} max={20} value={quantity} onChange={handleChangeQuantity} />
                    </div>

                    <div className="Product-view-info-options-btns flex justify-between flex-wrap">
                      <Button
                        className="secondary"
                        title="Thêm Vào Giỏ Hàng"
                        iconName={EIconName.shoppingCart}
                        iconColor={EIconColor.white}
                        reverse
                        onClick={handleAddCart}
                      />
                      <Button
                        className="secondary"
                        title="Thêm Vào Mục Yêu Thích"
                        iconName={EIconName.heart}
                        iconColor={EIconColor.white}
                        reverse
                      />
                      <Button
                        className="Product-view-info-options-btns buy-now"
                        title="Mua Ngay"
                        type="primary"
                        onClick={handleCheckout}
                      />
                    </div>
                  </div>

                  <div className="Product-view-info-options">
                    <div className="Product-view-info-options-config flex items-center flex-wrap">
                      <div className="Product-view-info-options-config-label">Mã Sản Phẩm: </div>
                      <p className="Product-view-info-options-config-text">{productState.title}</p>
                    </div>

                    <div className="Product-view-info-options-config flex items-center flex-wrap">
                      <div className="Product-view-info-options-config-label">Hình Thức Thanh Toán: </div>
                      <div className="Product-payment-cards flex">
                        <div className="Product-payment-cards-item">
                          <img src={PaymentAmericanExpress} alt="" />
                        </div>
                        <div className="Product-payment-cards-item">
                          <img src={PaymentDiscover} alt="" />
                        </div>

                        <div className="Product-payment-cards-item">
                          <img src={PaymentMasterCard} alt="" />
                        </div>

                        <div className="Product-payment-cards-item">
                          <img src={PaymentPaypal} alt="" />
                        </div>

                        <div className="Product-payment-cards-item">
                          <img src={PaymentVisa} alt="" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="Product-detail">
                <Tabs dataTabs={dataTabs} />
              </div>
            </div>
          </div>

          <div className="Product-related">
            <SliderProducts title="SẢN PHẨM LIÊN QUAN" />
          </div>
        </>
      )}
    </div>
  );
};

export default Product;
