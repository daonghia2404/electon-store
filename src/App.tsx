import React from 'react';
import { Redirect, Router } from '@reach/router';

import { LayoutPaths, Pages, Paths, ProtectedRoute, PublicRoute } from 'pages/routers';
import GuestLayout from 'layouts/GuestLayout';
import AuthLayout from 'layouts/AuthLayout';
import AdminLayout from 'layouts/AdminLayout';

const App: React.FC = () => {
  return (
    <div className="App">
      <Router primary={false}>
        <GuestLayout path={LayoutPaths.Guest}>
          <PublicRoute path={Paths.Home} component={Pages.Home} />
          <PublicRoute path={Paths.Product()} component={Pages.Product} />
          <PublicRoute path={Paths.Categories()} component={Pages.Categories} />
          <PublicRoute path={Paths.Cart} component={Pages.Cart} />
          <PublicRoute path={Paths.Checkout} component={Pages.Checkout} />
          <Redirect noThrow from={Paths.Rest} to={Paths.Home} />
        </GuestLayout>

        <AuthLayout path={LayoutPaths.Auth}>
          <PublicRoute path={Paths.AdminLogin} component={Pages.AdminLogin} />
          <Redirect noThrow from={Paths.Rest} to={`${LayoutPaths.Auth}${Paths.AdminLogin}`} />
        </AuthLayout>

        <AdminLayout path={LayoutPaths.Admin}>
          <ProtectedRoute path={Paths.ProductsManagement} component={Pages.ProductsManagement} />
          <ProtectedRoute path={Paths.OrdersManagement} component={Pages.OrdersManagement} />
          <ProtectedRoute path={Paths.AddProduct} component={Pages.AddProduct} />
          <ProtectedRoute path={Paths.UpdateProduct()} component={Pages.UpdateProduct} />
          <ProtectedRoute path={Paths.UpdateOrder()} component={Pages.UpdateOrder} />

          <Redirect noThrow from={Paths.Rest} to={`${LayoutPaths.Admin}${Paths.ProductsManagement}`} />
        </AdminLayout>
      </Router>
    </div>
  );
};

export default App;
