import _ from 'lodash';
import { Rule } from 'antd/lib/form';
import { notification } from 'antd';
import moment from 'moment';

import { EProductColor, ETypeNotification } from 'common/enums';
import { dataTextColor } from 'common/static';
import env from 'env';
import DefaultProductImage from 'assets/images/image-product-default.png';
import ImageCategoryMacbook from 'assets/images/categories/image-categories-macbook.png';
import ImageCategoryMacPro from 'assets/images/categories/image-categories-mac-pro.png';
import ImageCategoryMacMini from 'assets/images/categories/image-categories-mac-mini.png';
import ImageCategoryIMac from 'assets/images/categories/image-categories-imac.png';
import ImageCategoryIPad from 'assets/images/categories/image-categories-ipad.png';
import ImageCategoryIPhone from 'assets/images/categories/image-categories-iphone.png';
import ImageCategoryWatch from 'assets/images/categories/image-categories-watch.png';
import ImageCategoryMusic from 'assets/images/categories/image-categories-music.png';

export const formatNumberWithCommas = (number: number): string => {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export const getQueryParam = (param: string): string | null => {
  const params = new URLSearchParams(window.location.search);
  return params.get(param);
};

export const showNotification = (type: ETypeNotification, description: string): void => {
  const options = {
    message: '',
    description,
    className: 'Notification',
  };
  switch (type) {
    case ETypeNotification.success:
      notification.success(options);
      break;
    case ETypeNotification.warning:
      notification.warning(options);
      break;
    case ETypeNotification.error:
      notification.error(options);
      break;
    case ETypeNotification.info:
      notification.info(options);
      break;
    default:
      notification.open(options);
  }
};

export const removeDuplicateInArray = (array: Array<any>, key: string): Array<any> => {
  return _.uniqBy(array, key);
};

export const renderTextColor = (color: EProductColor): string => {
  return dataTextColor[color];
};

export const validationRules = {
  required: (message?: string): Rule => ({ required: true, message: message || 'Đây là trường bắt buộc' }),
  minLength: (message?: string, length = 2): Rule => ({
    min: length,
    message: message || `Nhập ít nhất ${length} kí tự`,
  }),
  maxLength: (message?: string, length = 10): Rule => ({
    max: length,
    message: message || `Nhập nhiều nhất ${length} kí tự`,
  }),
  email: (message?: string): Rule => ({
    type: 'email',
    message: message || 'Nhập sai định dạng email',
  }),
};

export const getFormDataFileUpload = (file: File): FormData => {
  const formData = new FormData();
  formData.append('file', file);
  return formData;
};

export const validateImageTypeFile = (file: File): boolean => {
  const acceptType = ['image/png', 'image/jpeg', 'image/jpg'];
  const maxSize = 2;
  const fileType = file.type;
  const fileSize = file.size / 1024 / 1024;

  return fileSize < maxSize && acceptType.includes(fileType);
};

export const getNameFile = (url: string): string => {
  return url.substring(url.lastIndexOf('/') + 1);
};

export const formatISODateToDateTime = (date: Date | string): string => {
  return moment(date).format('HH:mm DD/MM/YYYY');
};

export const getUrlImage = (fileName: string): string => {
  return `${env.api.baseUrl.service}/file/${fileName}`;
};

export const renderImageCategories = (title: string): string => {
  switch (title) {
    case 'Macbook':
      return ImageCategoryMacbook;
    case 'iMac':
      return ImageCategoryIMac;
    case 'iPad':
      return ImageCategoryIPad;
    case 'iPhone':
      return ImageCategoryIPhone;
    case 'Watch':
      return ImageCategoryWatch;
    case 'Airpod':
      return ImageCategoryMusic;
    case 'Mac Mini':
      return ImageCategoryMacMini;
    case 'Mac Pro':
      return ImageCategoryMacPro;
    default:
      return DefaultProductImage;
  }
};
